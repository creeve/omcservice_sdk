//
//  NSString+omc_NetApi.h
//  OMCService
//
//  Created by GuochengLiu on 2017/6/5.
//  Copyright © 2017年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (omc_NetApi)

+ (NSString *)deviceUUIDHashMD5Length32;

- (NSString *)stringByHashingWithMD5UsingEncoding:(NSStringEncoding)encoding;

+ (NSString *)hmacWithPortalKey:(NSString *)portalKey timeStamp:(NSTimeInterval)timeStamp user:(NSString *)user nonce:(NSInteger)nonce;

+ (NSInteger)getRandomNumber:(NSInteger)from to:(NSInteger)to;

@end

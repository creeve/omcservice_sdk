//
//  UBAModels.h
//  IvideoApiClient
//
//  Created by dvt04 on 16/11/16.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonModels.h"
#import "OMCMJExtension.h"

@interface UBAModels : NSObject

@end

@interface OMCChannelUrlInfo : NSObject

@property (nonatomic, copy) NSString *channelId;
@property (nonatomic, copy) NSString *channelUrl;

@end

@interface OMCRecommendItem : NSObject

@property (nonatomic, copy) NSString *epgSeriesTypeID;    // epg系列节目类型id，点播时为空字符串
@property (nonatomic, copy) NSString *charges;            // 收费标识，0免费而，大于0收费
@property (nonatomic, copy) NSString *epgSeriesID;        // 直播节目所在系列节目Id，点播时为空字符串
@property (nonatomic, copy) NSString *epgEndTime;         // epg结束时间，格式yyyyMMddHHmmss
@property (nonatomic, copy) NSArray<OMCChannelUrlInfo *> *channelUrlArr;   // channelUrl，直播地址
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *imageUrlArr;    // imageUrl,[{“分辨率标识1”:”url1”} …]频道台标图片url列表/点播节目图片url列表
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *epgImageUrlArr; // epgImageUrl,[{“分辨率标识1”:”url1”} …]  epg海报图片url列表，点播时为空字符串
@property (nonatomic, copy) NSString *updatedDrama;       // 更新到第几集（点播为空）
@property (nonatomic, copy) NSString *playTimes;          // 播放次数
@property (nonatomic, copy) NSString *epgID;              // epgid，点播时为空字符串
@property (nonatomic, copy) NSString *type;               // 类型，1表示点播，2表示直播
@property (nonatomic, copy) NSString *recommendId;        // id，频道id/节目id
@property (nonatomic, copy) NSString *epgSeriesName;      // 直播节目所在系列节目id，点播时为空字符串
@property (nonatomic, copy) NSString *recommendDes;       // description，描述信息
@property (nonatomic, copy) NSString *name;               // 频道名称/节目名称
@property (nonatomic, copy) NSString *epgName;            // epg播放节目名称，点播时为空字符
@property (nonatomic, copy) NSString *epgSeriesTypeName;  // epg系列节目类型名称，点播时为空字符串
@property (nonatomic, copy) NSString *liveRealImg;        // 直播频道实时截图
@property (nonatomic, copy) NSString *epgStartTime;       // epg开始时间，格式yyyyMMddHHmmss
@property (nonatomic, copy) NSString *tendency;           // 升降趋势，0无变化，1上升，-1下降
@property (nonatomic, copy) NSString *nextProgramName;    // 下一个epg名称
@property (nonatomic, copy) NSString *nextProgramDes;     // 下一个epg描述
@property (nonatomic, copy) NSString *nextStartTime;      // 下一个epg开始时间
@property (nonatomic, copy) NSString *nextEndTime;        // 下一个epg结束时间

@end

@interface OMCVodRecommendItem : NSObject

@property (nonatomic, copy) NSString *recommendId;  // id
@property (nonatomic, copy) NSString *charges;      
@property (nonatomic, copy) NSString *recommendDes; // description
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *playTimes;
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *imageUrlArr;

@end

@interface OMCPlayBackTopItem : NSObject

@property (nonatomic, copy) NSString *channelId;     // id，频道id
@property (nonatomic, copy) NSString *name;          // 频道名称
@property (nonatomic, copy) NSString *tendency;      // 升降趋势，0无变化，1上升，-1下降
@property (nonatomic, copy) NSString *playTimes;     // 播放次数
@property (nonatomic, copy) NSString *epgID;         // epgId，回看节目ID
@property (nonatomic, copy) NSString *epgName;       // epg播放节目名称，点播时为空字符串
@property (nonatomic, copy) NSString *epgStartTime;  // epg开始时间，格式yyyyMMddHHmmss
@property (nonatomic, copy) NSString *epgEndTime;    // epg结束时间，格式yyyyMMddHHmmss
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *imageUrlArr;  // [{“分辨率标识1”:”url1”} …]频道台标图片url列表

@end

@interface OMCEpisodeInfo : NSObject

@property (nonatomic, copy) NSString *startTime;  // 节目开始时间
@property (nonatomic, copy) NSString *endTime;    // 节目结束时间
@property (nonatomic, copy) NSString *drama;      // 当前集数，当前epg是电视剧的第多少集
@property (nonatomic, copy) NSString *epgID;      // epgID
@property (nonatomic, copy) NSString *epgName;    // epg名称

@end

@interface OMCEpgSeriesDesInfo : NSObject

@property (nonatomic, copy) NSArray<OMCEpisodeInfo *> *epgListArr;
@property (nonatomic, copy) NSString *duration;     // 总集数
@property (nonatomic, copy) NSString *updateDrama;  // 更新到第几集

@end

@interface OMCLiveEpgSeriesDesInfo : NSObject

@property (nonatomic, copy) NSString *startTime;          // 节目开始时间
@property (nonatomic, copy) NSString *endTime;            // 节目结束时间
@property (nonatomic, copy) NSString *channelAddress;     // 频道地址
@property (nonatomic, copy) NSString *historyUrl;         // 回看播放地址（如果是正在或即将播放的节目则此处为空）
@property (nonatomic, copy) NSString *epgSeriesDirector;  // 导演信息
@property (nonatomic, copy) NSString *epgSeriesActors;    // 演员信息
@property (nonatomic, copy) NSString *epgID;              // epgID
@property (nonatomic, copy) NSString *epgName;            // epg名称
@property (nonatomic, copy) NSString *rank;
@property (nonatomic, copy) NSString *programDes;
@property (nonatomic, copy) NSString *onetID;
@property (nonatomic, copy) NSString *freq;
@property (nonatomic, copy) NSString *networkID;
@property (nonatomic, copy) NSString *tsID;
@property (nonatomic, copy) NSString *serviceID;          // 业务ID,直播切拉屏时使用此值，对应交互文档中的“tv_ProgramNumber”字段

@end

@interface OMCHotSearchInfo : NSObject

@property (nonatomic, copy) NSString *keyword;   // 搜索关键字
@property (nonatomic, copy) NSString *times;     // 搜索次数
@property (nonatomic, copy) NSString *tendency;  // 上升下降的趋势(1:上升，0：持平，-1:下降)

@end

@interface OMCSearchResultItem : NSObject

@property (nonatomic, copy) NSString *type;                                 // 类型，1点播，2直播
@property (nonatomic, copy) NSString *mediaId;                              // id，频道Id/节目Id
@property (nonatomic, copy) NSString *mediaName;                            // name，频道名称/节目名称
@property (nonatomic, copy) NSString *mediaDes;                             // description，描述信息
@property (nonatomic, copy) NSArray<OMCChannelUrlInfo *> *channelUrlArr;    // channelUrl，直播地址
@property (nonatomic, copy) NSString *liveRealImg;                          // 直播实时截图
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *imageUrlArr;     // imageUrl，[{“分辨率标识1”:”url1”} …]频道台标图片url列表/点播节目图片url列表
@property (nonatomic, copy) NSString *epgSeriesID;                          // 直播节目所在系列节目Id，点播时为空字符串
@property (nonatomic, copy) NSString *epgSeriesName;                        // 直播节目所在系列节目名称，点播时为空字符串
@property (nonatomic, copy) NSString *playTimes;                            // 播放次数
@property (nonatomic, copy) NSString *epgID;                                // epgID,点播时为空字符串
@property (nonatomic, copy) NSString *epgName;                              // epg播放节目名称，点播时为空字符串
@property (nonatomic, copy) NSString *epgStartTime;                         // epg开始时间，格式yyyyMMddHHmmss，点播时为空字符串
@property (nonatomic, copy) NSString *epgEndTime;                           // epg结束时间，格式yyyyMMddHHmmss，点播时为空字符串
@property (nonatomic, copy) NSString *epgSeriesTypeID;                      // 所在系列类型ID
@property (nonatomic, copy) NSString *epgSeriesTypeName;                    // 所在系列类型名称
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *epgImageUrlArr;  // epgImageUrl，[{“分辨率标识1”:”url1”} …]  epg海报图片url列表，点播时为空字符串
@property (nonatomic, copy) NSString *categoryType;                         // 分类类型（0频道，1节目）
@property (nonatomic, copy) NSString *charges;                              // 收费标识，0免费，大于0收费

@end

@interface OMCSearchResultInfo : NSObject

@property (nonatomic, copy) NSString *categoryName;  // 分类名称
@property (nonatomic, copy) NSArray<OMCSearchResultItem *> *mediaListArr;

@end

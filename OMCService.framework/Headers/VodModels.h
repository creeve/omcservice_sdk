//
//  VodModels.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/11/1.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonModels.h"
#import "OMCMJExtension.h"


@interface OMCWelcomeInfo: NSObject

@property (nonatomic, copy) NSString *resourceID;          // 节目ID，字符串类型
@property (nonatomic, copy) NSString *resourceName;        // 节目名称，字符串类型
@property (nonatomic, copy) NSString *desc;         // 描述, 字符串类型（广告类型时为http跳转地址）
@property (nonatomic, copy) NSString *imageUrl;          // 图片url（文字类型无广告）
@property (nonatomic, copy) NSString *sortInx;          // 排序字段，值小的排在前面，字符串类型
@property (nonatomic, copy) NSString *positionType;  // 位置类型，字符串类型(1-首页大横幅，2-文字，3-最新)

@property (nonatomic, copy) NSString *recommendType;  // 推荐类型（1-点播、2-栏目、3-直播 4-广告）
@end

@interface OMCVodColumn : NSObject

@property (nonatomic, copy) NSString *columnID;          // 栏目标识
@property (nonatomic, copy) NSString *columnName;        // 栏目名称
@property (nonatomic, copy) NSString *columnDes;         // 栏目描述
@property (nonatomic, copy) NSString *imageUrl;          // 栏目未选中图片地址
@property (nonatomic, copy) NSString *selectedImageUrl;  // 栏目选中后的图片地址

@end

@interface OMCVodCategory : NSObject

@property (nonatomic, copy) NSString *categoryID;    // 分类ID
@property (nonatomic, copy) NSString *categoryName;  // 分类名称

@end

@interface OMCProgramInfo : NSObject

@property (nonatomic, copy) NSString *programID;
@property (nonatomic, copy) NSString *assertID;
@property (nonatomic, copy) NSString *providerID;
@property (nonatomic, copy) NSString *programName;
@property (nonatomic, copy) NSString *programDes;
@property (nonatomic, copy) NSString *columnID;
@property (nonatomic, copy) NSString *columnName;
@property (nonatomic, copy) NSString *categoryID;
@property (nonatomic, copy) NSString *categoryName;
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *imageUrlArr;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *rank;
@property (nonatomic, copy) NSString *updateTime;
@property (nonatomic, copy) NSString *actors;
@property (nonatomic, copy) NSString *director;
@property (nonatomic, copy) NSString *hdFlag;
@property (nonatomic, copy) NSString *location;
@property (nonatomic, copy) NSString *programType;  // 节目类型标识（0-电影，1-电视剧，2-系列片）
@property (nonatomic, copy) NSString *peopleCount;  // 历史观看人数
@property (nonatomic, copy) NSString *charges;      // 收费标识,0表示免费,大于0表示收费
@property (nonatomic, copy) NSString *programLabels;
@property (nonatomic, copy) NSString *otherInfo;

@end

@interface OMCMovieUrlInfo : NSObject

@property (nonatomic, copy) NSString *movieID;
@property (nonatomic, copy) NSString *movieUrl;

@end

@interface OMCProgramItemInfo : NSObject

@property (nonatomic, copy) NSString *programItemID;
@property (nonatomic, copy) NSString *programID;
@property (nonatomic, copy) NSString *titleName;
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *posterUrlArr;
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *previewUrlArr; 
@property (nonatomic, copy) NSString *previewAssetID;
@property (nonatomic, copy) NSString *previewProviderID;
@property (nonatomic, copy) NSArray<OMCMovieUrlInfo *> *movieUrlArr;
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *downLoadUrlArr;
@property (nonatomic, copy) NSString *movieAssertID;
@property (nonatomic, copy) NSString *movieProviderID;
@property (nonatomic, copy) NSString *movieLength;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *episodeID;
@property (nonatomic, copy) NSString *episodeName;
@property (nonatomic, copy) NSString *updateTime;

@end

@interface OMCVodHistoryInfo : NSObject

@property (nonatomic, copy) NSString *programID;         // 节目ID
@property (nonatomic, copy) NSString *episodeID;         // 集数标识
@property (nonatomic, copy) NSString *programName;       // 节目名称
@property (nonatomic, copy) NSString *titleName;         // 用于显示的节目名称（每一集的名称）
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *imageUrlArr;  // [{分辨率标识1：imageUrl1},{分辨率标识2：imageUrl2},…]
@property (nonatomic, copy) NSString *lastPosition;      // 最后观看位置，内容为距1970年1月1日的毫秒数
@property (nonatomic, copy) NSString *localModifyTime;   // 本地最后修改时间（使用服务器时间，字符串类型，内容为距1970年1月1日的毫秒数），终端比较此项进行合并，服务器校验时间误差(要求localModifyTime < serverTime + 1min)
@property (nonatomic, copy) NSString *serverModifyTime;  // 服务器最后修改时间（字符串类型，内容为距1970年1月1日的毫秒数）
@property (nonatomic, copy) NSString *status;            // 记录状态，-1已删除，0正常
@property (nonatomic, copy) NSString *compeleted;        // 0播放未完成，1播放已完成
@property (nonatomic, copy) NSString *assertID;
@property (nonatomic, copy) NSString *providerID;

@end

@interface OMCCloudVodHistoryInfo : NSObject

@property (nonatomic, copy) NSString *lastPosition;  // 最后观看位置，内容为距1970年1月1日的毫秒数
@property (nonatomic, copy) NSString *createTime;    // 创建时间，年月日时分秒格式 如:201610090800
@property (nonatomic, copy) NSString *programID;     // 节目ID
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *imageUrlArr;  // [{分辨率标识1：imageUrl1},{分辨率标识2：imageUrl2},…]
@property (nonatomic, copy) NSString *updateTime;    // 更新时间，年月日时分秒格式 如:201610090800
@property (nonatomic, copy) NSString *titleName;     // 用于显示的节目名称（每一集的名称）
@property (nonatomic, copy) NSString *assertID;      // 来自于portal与终端交互接口文档3.2.12的movieAssertID
@property (nonatomic, copy) NSString *providerID;    // 来自于portal与终端交互接口文档3.2.12的movieProviderID
@property (nonatomic, copy) NSString *completed;     // 0播放未完成，1播放已完成
@property (nonatomic, copy) NSString *programName;   // 节目名称

@end

@interface OMCVodFavoriteInfo : NSObject

@property (nonatomic, copy) NSString *programID;        // 节目ID
@property (nonatomic, copy) NSString *programName;      // 节目名称
@property (nonatomic, copy) NSString *columnID;         // 栏目ID
@property (nonatomic, copy) NSString *columnName;       // 栏目名称
@property (nonatomic, copy) NSString *actors;           // 演员
@property (nonatomic, copy) NSString *rank;             // 评分
@property (nonatomic, copy) NSString *director;         // 导演
@property (nonatomic, copy) NSString *year;             // 上映时间
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *imageUrlArr; // [{分辨率标识1：imageUrl1}]
@property (nonatomic, copy) NSString *localModifyTime;  // 本地最后修改时间，毫秒
@property (nonatomic, copy) NSString *serverModifyTime; // 服务端最后修改时间，毫秒
@property (nonatomic, copy) NSString *status;           // 记录状态（-1已删除，0正常）

@end

@interface OMCCloudVodFavoriteInfo : NSObject

@property (nonatomic, copy) NSString *createTime;  // 创建时间，（字符串类型，年月日时分秒格式 如:201610090800）
@property (nonatomic, copy) NSString *programID;   // 节目ID
@property (nonatomic, copy) NSString *rank;        // 评分
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *imageUrlArr;  // [{分辨率标识1：imageUrl1}]
@property (nonatomic, copy) NSString *updateTime;  // 更新时间，（字符串类型，年月日时分秒格式 如:201610090800）
@property (nonatomic, copy) NSString *columnID;    // 栏目ID
@property (nonatomic, copy) NSString *columnName;  // 栏目名称
@property (nonatomic, copy) NSString *year;        // 上映时间
@property (nonatomic, copy) NSString *director;    // 导演
@property (nonatomic, copy) NSString *programName; // 节目名称

@end

/**
 搜索结果
 */
@interface OMCSearchResult : NSObject

@property (nonatomic, copy) NSString *columnSize;  // portal翻页自用字段（iOS暂不使用）
@property (nonatomic, copy) NSString *count;       // 记录总数
@property (nonatomic, copy) NSString *rootType;    // 返回结果的类型. (1-直播类型 废弃）2-点播类型
@property (nonatomic, copy) NSArray *arrResult;      // 根据rootType的不同，分别存储TSLiveInfo的array或者TSVodInfo的array
@property (nonatomic, copy) NSArray *arrCategory;    // 搜索结果的分类。依赖于search接口中的type值, type为0时，此参数存储搜索结果的分类信息即TSSearchCategory的array, type为1时此参数为nil

@end

/**
 搜索结果分类
 */
@interface OMCSearchCategory : NSObject

@property (nonatomic, copy) NSString *rootType; // (1-直播类型,废弃，search接口仅用于搜索点播节目) 2-点播类型
@property (nonatomic, copy) NSString *subId;      // 点播栏目ID(或者直播分类ID)
@property (nonatomic, copy) NSString *subName;    // 点播栏目名称（或者直播分类名称）
@property (nonatomic, copy) NSString *counts;     // 当前分类下的搜索记录个数

@end

/**
 关键字
 */
@interface OMCKeywordsInfo : NSObject

@property (nonatomic, copy) NSString *keyword;  // 搜索关键字
@property (nonatomic, copy) NSString *times;    // 搜索次数

@end

/**
 节目上下线状态
 */
@interface OMCProgramStatus : NSObject

@property (nonatomic, copy) NSString *programID;  // 频道ID
@property (nonatomic, copy) NSString *status;     // 上下线状态，0表示上线，1表示下线

@end

@interface OMCDanmakuProperty : NSObject

@property (nonatomic, copy) NSString *speed;  // 速度
@property (nonatomic, copy) NSString *color;  // 颜色
@property (nonatomic, copy) NSString *size;   // 字体
@property (nonatomic, copy) NSString *pos;    // 位置

@end

@interface OMCDanmakuItem : NSObject

@property (nonatomic, copy) NSString *assetId;
@property (nonatomic, copy) NSString *authorId;     // 发送者用户id 暂用deviceId
@property (nonatomic, copy) NSString *content;      // 内容
@property (nonatomic, copy) NSString *createTime;   // 创建时间，单位毫秒
@property (nonatomic, copy) NSString *danmakuId;    // 弹幕ID
@property (nonatomic, copy) NSString *deviceType;   // 设备类型
@property (nonatomic, copy) NSString *episodeId;
@property (nonatomic, copy) NSString *playTime;     // 发送时间相对于视频开始时间的时间戳 单位毫秒
@property (nonatomic, strong) OMCDanmakuProperty *property;
@property (nonatomic, copy) NSString *providerId;   //
@property (nonatomic, copy) NSString *status;       // 1已审核 2未审核
@property (nonatomic, copy) NSString *type;         // 弹幕类型，1顶部滚动，2底部固定

@end

@interface OMCDanmakuModel : NSObject

@property (nonatomic, copy) NSString *filtered;  // 是否已过滤，1已审核，0未审核
@property (nonatomic, copy) NSString *count;     // 数量
@property (nonatomic, copy) NSArray<OMCDanmakuItem *> *danmakuListArr;

@end

@interface OMCYearInfo : NSObject

@property (nonatomic, copy) NSString *yearID;
@property (nonatomic, copy) NSString *yearName;

@end

@interface OMCGenresInfo : NSObject

@property (nonatomic, copy) NSString *genreID;
@property (nonatomic, copy) NSString *genreName;

@end

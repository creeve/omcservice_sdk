//
//  NSObject+OMCMJCoding.h
//  OMCMJExtension
//
//  Created by mj on 14-1-15.
//  Copyright (c) 2014年 小码哥. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCMJExtensionConst.h"

/**
 *  Codeing协议
 */
@protocol OMCMJCoding <NSObject>
@optional
/**
 *  这个数组中的属性名才会进行归档
 */
+ (NSArray *)omcmj_allowedCodingPropertyNames;
/**
 *  这个数组中的属性名将会被忽略：不进行归档
 */
+ (NSArray *)omcmj_ignoredCodingPropertyNames;
@end

@interface NSObject (OMCMJCoding) <OMCMJCoding>
/**
 *  解码（从文件中解析对象）
 */
- (void)omcmj_decode:(NSCoder *)decoder;
/**
 *  编码（将对象写入文件中）
 */
- (void)omcmj_encode:(NSCoder *)encoder;
@end

/**
 归档的实现
 */
#define OMCMJCodingImplementation \
- (id)initWithCoder:(NSCoder *)decoder \
{ \
if (self = [super init]) { \
[self omcmj_decode:decoder]; \
} \
return self; \
} \
\
- (void)encodeWithCoder:(NSCoder *)encoder \
{ \
[self omcmj_encode:encoder]; \
}

#define OMCMJExtensionCodingImplementation OMCMJCodingImplementation

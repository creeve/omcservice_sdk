//
//  LiveModels.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/11/1.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonModels.h"
#import "OMCMJExtension.h"

@interface OMCChannelInfo : NSObject

@property (nonatomic, copy) NSString *programNumber;
@property (nonatomic, copy) NSString *charges;
@property (nonatomic, copy) NSString *channelName;
@property (nonatomic, copy) NSString *modulation;
@property (nonatomic, copy) NSString *networkID;
@property (nonatomic, copy) NSString *hdFlag;
@property (nonatomic, copy) NSString *channelID;
@property (nonatomic, copy) NSString *freq;
@property (nonatomic, copy) NSString *audioPID;
@property (nonatomic, copy) NSString *channelDes;
@property (nonatomic, copy) NSString *symbolRate;
@property (nonatomic, copy) NSString *videoPID;
@property (nonatomic, copy) NSString *rank;
@property (nonatomic, copy) NSString *tsID;
@property (nonatomic, assign) NSInteger serviceID;
@property (nonatomic, copy) NSString *liveRealImg;
@property (nonatomic, copy) NSString *onetID;

@property (nonatomic, copy) NSArray<OMCResolutionMedia *> *channelUrl;
@property (nonatomic, copy) NSArray<OMCResolutionMedia *> *timeShiftUrl;
@property (nonatomic, copy) NSArray<NSNumber *> *categoryID;
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *imageUrl;

@end

@interface OMCLiveCategory : NSObject

@property (nonatomic, copy) NSString *categoryID;
@property (nonatomic, copy) NSString *categoryName;
@property (nonatomic, copy) NSString *imageUrl;

@end


@interface OMCCurrentEPG : NSObject

@property (nonatomic, copy) NSString *channelID;
@property (nonatomic, copy) NSString *liveRealImg;

@property (nonatomic, copy) NSString *currentProgramName;
@property (nonatomic, copy) NSString *currentProgramDes;
@property (nonatomic, copy) NSString *currentStartTime;
@property (nonatomic, copy) NSString *currentEndTime;

@property (nonatomic, copy) NSString *nextProgramName;
@property (nonatomic, copy) NSString *nextProgramDes;
@property (nonatomic, copy) NSString *nextStartTime;
@property (nonatomic, copy) NSString *nextEndTime;

@end


@interface OMCChannelEPG : NSObject

@property (nonatomic, copy) NSString *epgID;
@property (nonatomic, copy) NSString *epgName;
@property (nonatomic, copy) NSString *epgDes;

@property (nonatomic, copy) NSString *epgSeriesTypeID;
@property (nonatomic, copy) NSString *epgSeriesID;
@property (nonatomic, copy) NSString *epgSeriesTypeName;
@property (nonatomic, copy) NSString *channelName;
@property (nonatomic, copy) NSString *networkID;

@property (nonatomic, copy) NSString *freq;
@property (nonatomic, copy) NSString *startTime;
@property (nonatomic, copy) NSString *endTime;
@property (nonatomic, copy) NSString *rank;
@property (nonatomic, copy) NSString *tsID;
@property (nonatomic, copy) NSString *serviceID;

@property (nonatomic, copy) NSArray<OMCResolutionMedia *> *historyUrl;
@property (nonatomic, copy) NSArray<OMCResolutionImage *> *epgImageUrl;


@end


@interface OMCChannelStatus : NSObject

@property (nonatomic, copy) NSString *channelID;
@property (nonatomic, copy) NSString *status;

@end

@interface OMCWelcomeChannelInfo : NSObject

@property (nonatomic, copy) NSString *channelID;
@property (nonatomic, copy) NSString *channelName;
@property (nonatomic, copy) NSString *index;
@property (nonatomic, copy) NSString *imageUrl;

@end

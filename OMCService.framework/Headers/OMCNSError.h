//
//  OMCNSError.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/10/26.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString * const IvideoNSErrorDomain;

extern NSString * const IvideoNSErrorDescriptionKey;


typedef NS_ENUM(NSInteger, ApiClientErrorCode) {
    ApiClientErrorCode_ResponseError = -10001,
    ApiClientErrorCode_HttpStatusCodeError = -10002,
    ApiClientErrorCode_jsonParseError = -10003,
    ApiClientErrorCode_ResponseJsonNotDic = -10004,
};

typedef NS_ENUM(NSInteger, ApiBaseManagerErrorCode) {
    ApiBaseManagerErrorCode_ResponseDicIsEmpty = -20001,
    ApiBaseManagerErrorCode_ResponseStatusError = -20002,
    ApiBaseManagerErrorCode_ResponseDataValueNull = -20003,
    ApiBaseManagerErrorCode_ResponseDataValueNotDictionary = -20004,
    ApiBaseManagerErrorCode_ServerInfoNotIncludeValidaAddress = -20005,
};

typedef NS_ENUM(NSInteger, ApiManagerErrorCode) {
    ApiManagerErrorCode_RootValueIsEmpty = -30001,
    ApiManagerErrorCode_RootValueNotArray = -30002,
};


@interface OMCNSError : NSObject


@end


#ifndef __OMCMJExtensionConst__H__
#define __OMCMJExtensionConst__H__

#import <Foundation/Foundation.h>

// 过期
#define OMCMJExtensionDeprecated(instead) NS_DEPRECATED(2_0, 2_0, 2_0, 2_0, instead)

// 构建错误
#define OMCMJExtensionBuildError(clazz, msg) \
NSError *error = [NSError errorWithDomain:msg code:250 userInfo:nil]; \
[clazz setMj_error:error];

// 日志输出
#ifdef DEBUG
#define OMCMJExtensionLog(...) NSLog(__VA_ARGS__)
#else
#define OMCMJExtensionLog(...)
#endif

/**
 * 断言
 * @param condition   条件
 * @param returnValue 返回值
 */
#define OMCMJExtensionAssertError(condition, returnValue, clazz, msg) \
[clazz setMj_error:nil]; \
if ((condition) == NO) { \
    OMCMJExtensionBuildError(clazz, msg); \
    return returnValue;\
}

#define OMCMJExtensionAssert2(condition, returnValue) \
if ((condition) == NO) return returnValue;

/**
 * 断言
 * @param condition   条件
 */
#define OMCMJExtensionAssert(condition) OMCMJExtensionAssert2(condition, )

/**
 * 断言
 * @param param         参数
 * @param returnValue   返回值
 */
#define OMCMJExtensionAssertParamNotNil2(param, returnValue) \
OMCMJExtensionAssert2((param) != nil, returnValue)

/**
 * 断言
 * @param param   参数
 */
#define OMCMJExtensionAssertParamNotNil(param) OMCMJExtensionAssertParamNotNil2(param, )

/**
 * 打印所有的属性
 */
#define OMCMJLogAllIvars \
-(NSString *)description \
{ \
    return [self omcmj_keyValues].description; \
}
#define OMCMJExtensionLogAllProperties OMCMJLogAllIvars

/**
 *  类型（属性类型）
 */
extern NSString *const OMCMJPropertyTypeInt;
extern NSString *const OMCMJPropertyTypeShort;
extern NSString *const OMCMJPropertyTypeFloat;
extern NSString *const OMCMJPropertyTypeDouble;
extern NSString *const OMCMJPropertyTypeLong;
extern NSString *const OMCMJPropertyTypeLongLong;
extern NSString *const OMCMJPropertyTypeChar;
extern NSString *const OMCMJPropertyTypeBOOL1;
extern NSString *const OMCMJPropertyTypeBOOL2;
extern NSString *const OMCMJPropertyTypePointer;

extern NSString *const OMCMJPropertyTypeIvar;
extern NSString *const OMCMJPropertyTypeMethod;
extern NSString *const OMCMJPropertyTypeBlock;
extern NSString *const OMCMJPropertyTypeClass;
extern NSString *const OMCMJPropertyTypeSEL;
extern NSString *const OMCMJPropertyTypeId;

#endif

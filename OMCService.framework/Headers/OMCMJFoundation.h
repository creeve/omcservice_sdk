//
//  OMCMJFoundation.h
//  OMCMJExtensionExample
//
//  Created by OMCMJ Lee on 14/7/16.
//  Copyright (c) 2014年 小码哥. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OMCMJFoundation : NSObject
+ (BOOL)isClassFromFoundation:(Class)c;
@end

//
//  OMCLiveApiManager.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/11/1.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCApiBaseManager.h"
#import "OMCApiManagerUtil.h"

#import "LiveModels.h"

extern NSString * const OMCLiveChannelCategoryAll;

typedef NS_ENUM(NSUInteger, OMCLiveSortType) {
    OMCLiveSortTypeName = 0,
    OMCLiveSortTypeScore = 1,
    OMCLiveSortTypeSTB = 2,
};


@interface OMCLiveApiManager : NSObject

@property (nonatomic, copy) NSArray <OMCChannelInfo *> *arrayCachedChannel;

+ (instancetype)sharedManager;


- (NSURLSessionTask *)request_LiveCategoryList_WithSuccess:(void (^)(NSURLSessionDataTask *task, NSArray <OMCLiveCategory *>* arrayData))success
                                                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionTask *)request_ChannelList_WithCategoryId:(NSString *)categoryId
                                                   start:(NSInteger)start
                                                     end:(NSInteger)end
                                             channelName:(NSString *)channelName
                                             programName:(NSString *)programName
                                                sortType:(OMCLiveSortType)sortType
                                                 success:(void (^)(NSURLSessionDataTask *task, NSArray <OMCChannelInfo *>* arrayData))success
                                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionTask *)request_AllChannel_WithCached:(BOOL)cached
                                            success:(void (^)(NSURLSessionDataTask *task, NSArray <OMCChannelInfo *>* arrayData))success
                                            failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionTask *)request_CurrentEPG_WithStart:(NSInteger)start
                                               end:(NSInteger)end
                                          sortType:(OMCLiveSortType)sortType
                                        categoryID:(NSString *)categoryID
                                         channelID:(NSString *)channelID
                                        resolution:(NSString *)resulution
                                           success:(void (^)(NSURLSessionDataTask *task, NSArray <OMCCurrentEPG *>* arrayData))success
                                           failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionTask *)request_EPGInfoList_WithChanneID:(NSString *)channelID
                                             startTime:(NSString *)startTime
                                               endTime:(NSString *)endTime
                                               success:(void (^)(NSURLSessionDataTask *task, NSArray <OMCChannelEPG *>* arrayData))success
                                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


- (NSURLSessionTask *)request_ChannelStatus_WithArrayChannelID:(NSArray <NSString *>*)arrayChannelID success:(void (^)(NSURLSessionDataTask *task, NSArray <OMCChannelStatus *>* arrayData))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


- (NSURLSessionTask *)request_ChannelRecommend_WithSuccess:(void (^)(NSURLSessionDataTask *task, NSArray <OMCWelcomeChannelInfo *>* arrayData))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;


@end

//
//  OMCService.h
//  OMCService
//
//  Created by GuochengLiu on 2017/5/18.
//  Copyright © 2017年 sumavision. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for OMCService.
FOUNDATION_EXPORT double OMCServiceVersionNumber;

//! Project version string for OMCService.
FOUNDATION_EXPORT const unsigned char OMCServiceVersionString[];

#import <OMCService/OMCApiBaseManager+BaseUrl.h>
#import <OMCService/OMCCommonApiManager.h>
#import <OMCService/OMCLiveApiManager.h>
#import <OMCService/OMCVodApiManager.h>
#import <OMCService/OMCPayApiManager.h>
#import <OMCService/OMCUBAApiManager.h>
#import <OMCService/OMCUserApiManager.h>
#import <OMCService/OMCAuthApiManager.h>
#import <OMCService/OMCWelcomeApiManager.h>
#import <OMCService/OMCAdvertiseApiManager.h>
#import <OMCService/OMCMJFoundation.h>




//
//  NSURLSessionTask+Api.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/11/14.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLSessionTask (Api)
/**
 将response返回的原始data数据作为sessionTask的附加字段，方便有需要的时候取用（主要为接口测试程序呈现response data使用）
 */
- (void)setIvideoApiResponseData:(NSData *)responseData;

/**
 获取sessionTask上附着的data数据
 */
- (NSData *)getIvideoApiResponseData;

/**
 获取sessionTask上附着的data数据，并转换为NSString
 */
- (NSString *)getIvideoApiResponseDataString;

@end

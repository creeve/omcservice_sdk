//
//  OMCApiClient.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/10/24.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCNSError.h"
#import "NSURLSessionTask+Api.h"

typedef NS_ENUM(NSInteger, NetworkMethod) {
    Get = 0,
    Post,
    Put,
    Delete,
};

@interface OMCApiClient : NSObject

//填充完整URL，返回json解析后的对象
//针对网络错误处理，例如404等，其余返回
//维护task队列；提供单个task的函数；提供取消所有task的函数
//增加运行时校验所有参数是否为空

+ (OMCApiClient *)sharedApiClient;

@property (readonly, nonatomic, strong) NSURLSession *session;


- (NSURLSessionDataTask *)request_JsonDataWith_URL:(NSURL *)fullURL
                                    withMethodType:(NetworkMethod)method
                                       bodyJsonStr:(NSString *)bodyJsonStr
                                           success:(void (^)(NSURLSessionDataTask *sessionTask, NSDictionary *responseDic))success
                                           failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;


- (void)cancelSessionTask:(NSURLSessionDataTask *)cancelTask;

- (NSURLSessionDataTask *)validateServerWith_GetURL:(NSURL *)getURL
                                  completionHandler:(void (^)(NSURLSessionDataTask *sessionTask, NSDictionary *dataDic, NSError *error))completionHandler;

@end

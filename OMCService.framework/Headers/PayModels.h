//
//  PayModels.h
//  IvideoApiClient
//
//  Created by dvt04 on 16/11/22.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PayModels : NSObject

@end

@interface OMCAccountBalanceInfo : NSObject

@property (nonatomic, copy) NSString *accountID;    // 账户ID
@property (nonatomic, copy) NSString *accountName;  // 账户名称
@property (nonatomic, copy) NSString *balance;      // 余额信息

@end

@interface OMCGoodsInfo : NSObject

@property (nonatomic, copy) NSString *goodsID;      // 商品ID
@property (nonatomic, copy) NSString *goodsName;    // 商品名称
@property (nonatomic, copy) NSString *goodsPrices;  // 商品价格
@property (nonatomic, copy) NSString *goodsDes;     // 商品描述

@end

@interface OMCOrderInfo : NSObject

@property (nonatomic, copy) NSString *orderID;      // 订单编号
@property (nonatomic, copy) NSString *totalPrice;   // 订单金额
@property (nonatomic, copy) NSString *orderDate;    // 订单生成时间，yyyy-MM-ddThh:mm:ss
@property (nonatomic, copy) NSString *orderStatus;  // 订单状态，1未支付，2已支付，3等待支付，4订单废除
@property (nonatomic, copy) NSString *goodsName;    // 商品名称
@property (nonatomic, copy) NSString *goodsAmount;  // 商品总数
@property (nonatomic, copy) NSString *orderDes;     // 订单描述
@property (nonatomic, copy) NSString *balance;      // 余额信息

@end

@interface OMCPayBalanceInfo : NSObject

@property (nonatomic, copy) NSString *orderID;  // 订单号
@property (nonatomic, copy) NSString *result;   // 支付结果,0成功，1失败
@property (nonatomic, copy) NSString *reason;   // 原因

@end


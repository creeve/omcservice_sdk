//
//  OMCCommonApiManager.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/10/29.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCApiBaseManager.h"
#import "OMCApiManagerUtil.h"
#import "CommonModels.h"

@interface OMCCommonApiManager : NSObject

@property (nonatomic, assign) NSTimeInterval timeIntervalBetweenServer;

+ (OMCCommonApiManager *)sharedManager;

/**
 * 意见反馈
 *
 * @param email         邮件地址
 * @param phone         联系方式
 * @param suggestion    意见（1024字节以内，最多512汉字）
 * @param mode          终端型号
 * @param appVersion    客户端版本
 * @param sdkVersion    终端系统版本
 * @param manufacturer  制造商
 *
 * @return YES,标识反馈成功
 */
- (NSURLSessionDataTask *)request_FeedBack_WithEmail:(NSString *)email
                                               phone:(NSString *)phone
                                          suggestion:(NSString *)suggestion
                                                mode:(NSString *)mode
                                          appVersion:(NSString *)appVersion
                                          sdkVersion:(NSString *)sdkVersion
                                        manufacturer:(NSString *)manufacturer
                                             success:(void (^)(NSURLSessionDataTask *sessionTask, BOOL result))success
                                             failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取配置信息
 *
 * @param what  配置信息类型
 *
 * @return NSDictionary
 */
- (NSURLSessionDataTask *)request_ConfigInfo_WithWhat:(NSString *)what
                                          success:(void (^)(NSURLSessionDataTask *sessionTask, NSDictionary *dicConfigs))success
                                          failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取地区信息
 *
 * @param locationID  地区ID
 *
 * @return OMCLocationInfo类型的数组
 */
- (NSURLSessionDataTask *)request_LocationInfo_WithLocationID:(NSString *)locationID
                                                      success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray <OMCLocationInfo *> *arrLocations))success
                                                      failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取服务器当前时间
 *
 * @param dateFormat  返回的时间格式（默认为yyyyMMddHHmmss）
 *
 * @return NSString
 */
- (NSURLSessionDataTask *)request_ServerTime_WithDateFormat:(NSString *)dateFormat
                                                    success:(void (^)(NSURLSessionDataTask *sessionTask, NSString *serverTime))success
                                                    failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

- (NSURLSessionDataTask *)request_TimeIntervalBetweenServer_WithSuccess:(void (^)(NSURLSessionDataTask *sessionTask, NSTimeInterval timeInterval))success failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取portalServerIP
 *
 * @return NSDictionary
 */
- (NSURLSessionDataTask *)request_portalServerIP_WithSuccess:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                       NSDictionary *portalServerInfo))success
                                                     failure:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                       NSError *error))failure;

/**
 * 获取分辨率支持信息
 *
 * @return OMCResolutionInfo类型的数组
 */
- (NSURLSessionDataTask *)request_Resolution_WithSuccess:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                   NSArray <OMCResolutionInfo *> *arrResolution))success
                                                 failure:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                   NSError *error))failure;

/**
 * 获取开机欢迎图片
 *
 * @return OMCBootPictureInfo类型的数组
 */
- (NSURLSessionDataTask *)request_BootPicture_WithSuccess:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                    NSArray <OMCBootPictureInfo *> *arrBootPictures))success
                                                  failure:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                    NSError *error))failure;

/**
 * 获取广告业务列表
 * 
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_Advertisement_WithSuccess:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                      NSArray *arrAds))success
                                                    failure:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                      NSError *error))failure;

/**
 * 点赞和踩  目前此接口已经废弃，改用ptl_ipvp_cmn_cmn018接口。
 *
 * @param operateType  操作类型（1.赞 2.踩 3.获取评分）
 * @param programID    节目ID
 * @param channelID    频道ID
 *
 * @return NSDictionary
 */
- (NSURLSessionDataTask *)request_LikeOrNot_WithOperateType:(NSString *)operateType
                                                  programID:(NSString *)programID
                                                  channelID:(NSString *)channelID
                                                    success:(void (^)(NSURLSessionDataTask *sessionTask, NSDictionary *dicLikeOrNot))success
                                                    failure:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                      NSError *error))failure;

/**
 * 获取portalKey
 *
 * @return NSString
 */
- (NSURLSessionDataTask *)request_PortalKey_WithSuccess:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                  NSString *portalKey))success
                                                failure:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                  NSError *error))failure;

/**
 * 获取配置文件信息  目前废弃，曾经在福建用过，但福建现在使用ptl_ipvp_cmn_cmn017替代
 *
 * @param keys  获取配置文件信息对应的key，字符串类型，可以为空，支持多个，多个以英文逗号“,”连接
 *
 * @return NSDictionary
 */
- (NSURLSessionDataTask *)request_ConfigInfoByKeys_WithKeys:(NSString *)keys
                                                    success:(void (^)(NSURLSessionDataTask *sessionTask, NSDictionary *dicConfigs))success
                                                    failure:(void (^)(NSURLSessionDataTask *sessionTask,
                                                        NSError *error))failure;

/**
 * 获取配置信息（V1）
 *
 * @return NSDictionary
 */
- (NSURLSessionDataTask *)request_Config_V1_WithConfigInfo:(NSDictionary *)dicConfig
                                                    success:(void (^)(NSURLSessionDataTask *sessionTask, NSDictionary *dicConfigs))success
                                                    failure:(void (^)(NSURLSessionDataTask *sessionTask,
                                                        NSError *error))failure;

/**
 * 点赞和踩（V1）
 *
 * @param operateType    操作类型（1.点赞 2.踩 3.获取评分和是否能够继续点赞的标识）
 * @param programID      节目ID
 * @param channelID      频道ID
 * @param programItemID  剧集ID，该值为“-1”时，表示是针对programID这个剧集整体的点赞或踩、获取评分的操作
 *
 * @return OMCLikeOrNotInfo类型的数组
 */
- (NSURLSessionDataTask *)request_LikeOrNot_V1_WithOperateType:(NSString *)operateType
                                                     programID:(NSString *)programID
                                                     channelID:(NSString *)channelID
                                                 programItemID:(NSString *)programItemID
                                                       success:(void (^)(NSURLSessionDataTask *sessionTask, OMCLikeOrNotInfo *likeOrNotInfo))success
                                                       failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取评论信息列表
 *
 * @param programID      // 直播或点播的ID
 * @param programType    // 1.直播 2.点播
 * @param programItemID  // 剧集ID,该值为“-1”时，表示是针对programID这个剧集整体的评论操作
 * @param releaseTime    // 该参数为查询评论的时间条件，可以是当前时间，也可以是指定评论的发布时间
 * @param count          // 要求前端返回的评论数，从当前时间（或指定评论的发布时间）开始计算向前获取指定的评论数，当该值为
 0时，只返回一定时间内活跃的评论人数
 * @param commentID      // 评论ID（该参数为查询评论的条件，替换之前的releaseTime查询条件）
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_CommentList_WithProgramID:(NSString *)programID
                                                programType:(NSString *)programType
                                              programItemID:(NSString *)programItemID
                                                releaseTime:(NSString *)releaseTime
                                                      count:(NSInteger)count
                                                  commentID:(NSString *)commentID
                                                    success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCCommentInfo *> *commentsArr, NSString *commentsCount, NSString *peopleCount))success
                                                    failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 添加评论信息
 *
 * @param programID      // 节目ID
 * @param programItemID  // 剧集ID，该值为“-1”时，表示是针对programID这个剧集整体的评论操作
 * @param programType    // 节目类型（1：直播 2：点播）
 * @param headImageUrl   // 用户头像url，可以为空
 * @param content        // 评论内容
 *
 * @return BOOL
 */
- (NSURLSessionDataTask *)request_AppendComment_WithProgramID:(NSString *)programID
                                                programItemID:(NSString *)programItemID
                                                  programType:(NSString *)programType
                                                 headImageUrl:(NSString *)headImageUrl
                                                      content:(NSString *)content
                                                      success:(void (^)(NSURLSessionDataTask *sessionTask, BOOL result))success
                                                      failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

@end

//
//  NSError+Api.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/10/29.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCNSError.h"

@interface NSError (Api)

+ (NSError *)errorWithApiCode:(NSInteger)code;
+ (NSError *)errorWithApiCode:(NSInteger)code errDescription:(NSString *)errorDescription;
- (NSString *)ivideoDescription;

@end

//
//  OMCVodApiManager.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/11/1.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCApiBaseManager.h"
#import "OMCApiManagerUtil.h"
#import "VodModels.h"

@interface OMCVodApiManager : NSObject

+ (OMCVodApiManager *)sharedManager;

/**
 * 获取点播热门搜索
 *
 * @param count  要返回的列表个数，不足返回实际列表
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_VodHotSearchInfo_WithCount:(NSInteger)count
                                                  success:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                    NSArray<OMCKeywordsInfo *> *arr))success
                                                  failure:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                    NSError *error))failure;
/**
 * 获取点播搜索
 *
 * @param keyword     关键字
 * @param start       列表开始位置
 * @param end         列表结束为止
 * @param subID       点播栏目ID
 * @param columnSize  翻页自用字段(当要对搜索结果进行翻页时，将从portal获取的响应数据里的同名参数值传回)
 * @param type        搜索类型，默认为0（0-返回搜索结果列表以及搜索结果分类信息；1-返回搜索结果列表信息，不返回搜索结果分类信息；）
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_VodSearch_WithKeyword:(NSString *)keyword
                                                  start:(NSInteger)start
                                                    end:(NSInteger)end
                                                  subID:(NSString *)subID
                                             columnSize:(NSString *)columnSize
                                                   type:(NSInteger)type
                                                success:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                  OMCSearchResult *searchResult))success
                                                failure:(void (^)(NSURLSessionDataTask *sessionTask,
                                                                  NSError *error))failure;

/**
 * 按节目名称精确搜索
 *
 * @param programName      节目名称
 * @param startTime        节目开始时间
 * @param tvFreq           频点
 * @param tvSymbolRate     符号率
 * @param tvModulation     调制方式
 * @param tvProgramNumber  业务ID
 * @param tvVideoPID       视频ID
 * @param tvAudioPID       音频ID
 * @param start            列表开始位置
 * @param end              列表结束位置
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_findProgramByName_WithProgramName:(NSString *)programName
                                                          startTime:(NSString *)startTime
                                                             tvFreq:(NSString *)tvFreq
                                                      tvSymbolRate:(NSString *)tvSymbolRate
                                                       tvModulation:(NSString *)tvModulation
                                                    tvProgramNumber:(NSString *)tvProgramNumber
                                                         tvVideoPID:(NSString *)tvVideoPID
                                                         tvAudioPID:(NSString *)tvAudioPID
                                                              start:(NSInteger)start
                                                                end:(NSInteger)end
                                                          success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCProgramInfo *> *arr))success
                                                          failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;


/**
 * 获取点播首页推荐列表
 *
 * @param counts  获取节目排行的数量（默认取每个栏目的排名前10的节目）
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_VodWelcomeList_WithCounts:(NSInteger)counts
                                                    success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray *arr))success
                                                    failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取首页推荐列表及节目相关信息  // 已在OMCWelcomeApiManager中实现
 *
 * @param counts  获取节目偏航的数量（默认取每个栏目排名前10的节目）
 *
 * @return NSArray
 */
//- (NSURLSessionDataTask *)request_VodWelcomeListInfos_WithCounts:(NSInteger)counts success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray *arr))success failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取首页推荐分类列表  // 已在OMCWelcomeApiManager中实现
 *
 * @param counts    获取节目排行的数量
 * @param columnID  栏目ID（空串表示获取全部的栏目，例如：电影、电视剧等）栏目的个数，可以由Portal系统配置文件配置。默认为5个栏目。该值来自于3.2.8 获取栏目列表中的返回值columnID）
 *
 * @return NSArray
 */
//- (NSURLSessionDataTask *)request_VodWelcomeListByHot_WithCounts:(NSInteger)counts columnID:(NSString *)columnID success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray *arr))success failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取栏目列表
 *
 * @return ProgramInfo类型的数组
 */
- (NSURLSessionDataTask *)request_ColumnInfoList_WithSuccess:(void (^)(NSURLSessionDataTask *sessionTask,
                                                         NSArray<OMCVodColumn *> *arr))success
                                                     failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取栏目下分类列表
 *
 * @param columnID  栏目标示
 *
 * @return OMCVodCategory类型的数组
 */
- (NSURLSessionDataTask *)request_CategoryInfoList_WithColumnID:(NSString *)columnID
                                                        success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCVodCategory *> *arr))success
                                                        failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 终端获取筛选条件列表
 *
 * @param columnID  栏目ID
 *
 * @return NSArray, NSArray, NSArray, NSArray
 */
- (NSURLSessionDataTask *)request_FilterList_WithColumnID:(NSString *)columnID
                                                  success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCVodCategory *> *categoryArr, NSArray<OMCGenresInfo *> *genresArr, NSArray<OMCLocationInfo *> *locationArr, NSArray<OMCYearInfo *> *yearArr))success
                                                  failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取点播节目列表
 *
 * @param columnID     栏目标识
 * @param categoryID   分类标识
 * @param start        列表开始
 * @param end          列表结束
 * @param programName  节目名称
 * @param queryValue   搜索关键字（匹配节目名称、导演等）
 * @param sortType     排序时间（0-上架时间；1-评分；2-节目上映时间，3-点击次数，4-首页推荐节目）类型4仅供俄罗斯使用
 * @param year         年份
 * @param location     地区（1-内地，2-港台，3-日韩，4-美国，5-法国,0-全部）
 *
 * @return ProgramInfo类型的数组
 */
- (NSURLSessionDataTask *)request_ProgramInfoList_WithColumnID:(NSString *)columnID
                                                    categoryID:(NSString *)categoryID
                                                         start:(NSInteger)start
                                                           end:(NSInteger)end
                                                   programName:(NSString *)programName
                                                    queryValue:(NSString *)queryValue
                                                      sortType:(NSInteger)sortType
                                                          year:(NSString *)year
                                                      location:(NSString *)location
                                                       success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCProgramInfo *> *arr))success failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取点播节目包
 *
 * @param programID   节目ID（为空表示不作为查询条件）
 * @param assetID     资产ID（为空表示不作为查询条件）
 * @param providerID  提供商ID（为空表示不作为查询条件）
 * @param episodeID   集数标识（为空表示不作为查询条件）
 *
 * @return OMCProgramItemInfo类型数组
 */
- (NSURLSessionDataTask *)request_ProgramInfoItem_WithProgramID:(NSString *)programID
                                                        assetID:(NSString *)assetID
                                                     providerID:(NSString *)providerID
                                                      episodeID:(NSString *)episodeID
                                                        success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCProgramItemInfo *> *arr))success failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取点播节目信息
 *
 * @param programID   节目ID（为空表示不作为查询条件）
 * @param assetID     资产ID（为空表示不作为查询条件）
 * @param providerID  提供商ID（为空表示不作为查询条件）
 *
 * @return OMCProgramItemInfo
 */
- (NSURLSessionDataTask *)request_ProgramInfo_WithProgramID:(NSString *)programID
                                                    assetID:(NSString *)assetID
                                                 providerID:(NSString *)providerID
                                                    success:(void (^)(NSURLSessionDataTask *sessionTask, OMCProgramInfo *programItemInfo))success
                                                    failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取点播节目数量  // 未调用
 *
 * @param columnID    栏目ID
 * @param categoryID  分类ID
 * @param year        年份
 * @param location    地区 1.内地 2.港台 3.日韩 4.美国 5.法国 0.全部
 *
 * @return NSString
 */
- (NSURLSessionDataTask *)request_ProgramCount_WithColumnID:(NSString *)columnID
                                                 categoryID:(NSString *)categoryID
                                                       year:(NSString *)year
                                                   location:(NSString *)location
                                                    success:(void (^)(NSURLSessionDataTask *sessionTask, NSString *count))success
                                                    failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取所有标签信息  // 未开发
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_AllProgramLabel_WithSuccess:(void (^)(NSURLSessionDataTask *sessionTask, NSArray *arr))success failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 根据标签获取点播节目列表  // 未开发
 *
 * @param columnID    栏目标识
 * @param categoryID  分类标识
 * @param start       列表开始
 * @param end         列表结束
 * @param labelName   标签名称
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_ProgramInfosByLabel_WithColumnID:(NSString *)columnID
                                                          categoryID:(NSString *)categoryID
                                                             start:(NSInteger)start
                                                               end:(NSInteger)end
                                                         labelName:(NSString *)labelName
                                                           success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray *arr))success
                                                           failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 查询点播播放记录
 *
 * @param lastQueryTime  serverModifyTime最新值（内容为距1970年1月1日的毫秒数，传0表示全量查询；全量更新不返回已删除数据）
 * @param start          查询的起始位置（从0开始）
 * @param end            查询的结束位置（包括该位置）
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_QueryHistory_WithLastQueryTime:(NSString *)lastQueryTime
                                                           start:(NSInteger)start
                                                             end:(NSInteger)end
                                                         success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray *arr))success
                                                         failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 更新点播播放记录
 *
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_UpdateHistory_WithVodHistoryInfos:(NSArray *)arrVodHistoryInfos
                                             success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray *arr))success
                                             failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 删除点播播放记录
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_DeleteHistory_WithVodHistoryInfos:(NSArray *)arrVodHistoryInfos
                                                            success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray *arr))success
                                                            failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 查询点播收藏记录
 *
 * @param lastQueryTime  上次查询时间（内容为距1970年1月1日的毫秒数，传0表示全量查询）
 * @param start          查询的起始位置
 * @param end            查询的结束位置
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_QueryVodFavorite_WithLastQueryTime:(NSString *)lastQueryTime
                                                               start:(NSInteger)start
                                                                 end:(NSInteger)end
                                                             success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCVodFavoriteInfo *> *arr))success
                                                             failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 更新点播收藏记录
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_UpdateVodFavorite_WithVodFavoriteInfos:(NSArray *)arrVodFavoriteInfos
                                                            success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray *arr))success
                                                            failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 删除点播收藏记录
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_DeleteVodFavorite_WithVodFavoriteInfos:(NSArray *)arrFavoriteInfos
                                                            success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray *arr))success
                                                            failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取点播语音关键字列表  // 未开发
 *
 * @param versions  版本号（整形，前端的语音关键字文件有修改，version加1）
 *
 * @return NSDictionary
 */
- (NSURLSessionDataTask *)request_VodVoiceKeyWords_WithVersions:(NSString *)versions
                                                        success:(void (^)(NSURLSessionDataTask *sessionTask, NSDictionary *dic))success
                                                        failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取点播上下线状态
 *
 * @param programID  点播节目ID，可以为空，支持多个programID，多个programID以英文逗号“,”连接
 * @param columnID   点播一级栏目ID，可以为空
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_ProgramStatus_WithProgramID:(NSString *)programID
                                                     columnID:(NSString *)columnID
                                                      success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCProgramStatus *> *arr))success
                                                      failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取弹幕信息
 *
 * @param assetId     资产ID
 * @param providerId  供应商ID
 * @param episodeId   集数
 * @param startTime   获取弹幕的开始时间
 * @param endTime     获取弹幕的结束时间
 *
 * @return OMCDanmakuModel
 */
- (NSURLSessionDataTask *)request_Danmaku_WithAssetID:(NSString *)assetId
                                           providerId:(NSString *)providerId
                                            episodeId:(NSString *)episodeId
                                            startTime:(NSTimeInterval)startTime
                                              endTime:(NSTimeInterval)endTime
                                              success:(void (^)(NSURLSessionDataTask *sessionTask, OMCDanmakuModel *danmakuModel))success
                                              failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 发送弹幕信息
 *
 * @param assetId     资产ID
 * @param providerId  供应商ID
 * @param episodeId   集数
 * @param pos         位置
 * @param size        字体
 * @param color       颜色
 * @param speed       速度
 * @param content     内容
 * @param type        弹幕类型
 * @param playTime    发送时间（距离节目开始的秒数）
 *
 * @return BOOL类型的result
 */
- (NSURLSessionDataTask *)request_SendDanmaku_WithAssetId:(NSString *)assetId
                                               providerId:(NSString *)providerId
                                                episodeId:(NSString *)episodeId
                                                      pos:(NSString *)pos
                                                     size:(NSString *)size
                                                    color:(NSString *)color
                                                    speed:(NSString *)speed
                                                  content:(NSString *)content
                                                     type:(NSString *)type
                                                 playTime:(NSString *)playTime
                                                  success:(void (^)(NSURLSessionDataTask *sessionTask, BOOL result))success
                                                  failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取预告片和片花信息
 *
 * @param programID   节目ID
 * @param assetID     资产ID（为空表示不作为查询条件）
 * @param providerID  提供商ID（为空表示不作为查询条件）
 * @param episodeID   集数标识（为空表示不作为查询条件）
 *
 * @return NSArray, NSArray  一个存放预告片， 一个存放片花
 */
- (NSURLSessionDataTask *)request_ProgramPrevue_WithProgramID:(NSString *)programID
                                                      assetID:(NSString *)assetID
                                                   providerID:(NSString *)providerID
                                                    episodeID:(NSString *)episodeID
                                                      success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCProgramItemInfo *> *prevuesArr, NSArray<OMCProgramItemInfo *> *videoClipsArr))success
                                                      failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 查询点播播放记录（新）
 *
 * @param start  查询的起始位置（从0开始）
 * @param end    查询的结束位置（包括该位置）
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_QueryVodPlayHistory_WithStart:(NSInteger)start
                                                            end:(NSInteger)end
                                                        success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCCloudVodHistoryInfo *> *arr))success
                                                        failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 更新点播播放记录（新）
 *
 * @return BOOL类型的result
 */
- (NSURLSessionDataTask *)request_UpdateVodPlayHistory_WithVodHistoryInfo:(OMCVodHistoryInfo *)vodHistoryInfo
                                                            success:(void (^)(NSURLSessionDataTask *sessionTask, BOOL result))success
                                                            failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 删除点播播放记录（新）
 *
 * @return BOOL类型的result
 */
- (NSURLSessionDataTask *)request_DeleteVodPlayHistory_WithVodHistoryInfo:(OMCVodHistoryInfo *)vodHistoryInfo
                                                            success:(void (^)(NSURLSessionDataTask *sessionTask, BOOL result))success
                                                            failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 查询点播收藏记录（新）
 *
 * @param start  查询的起始位置
 * @param end    查询的结束位置
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_QueryVodFavoriteHistory_WithStart:(NSInteger)start
                                                                end:(NSInteger)end
                                                            success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCCloudVodFavoriteInfo *> *arr))success
                                                            failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 保存点播收藏记录（新）
 *
 * @return BOOL类型的result
 */
- (NSURLSessionDataTask *)request_UpdateVodFavoriteHistory_WithVodFavoriteInfo:(OMCVodFavoriteInfo *)
                                                                       vodFavoriteInfo
                                                                       success:(void (^)(NSURLSessionDataTask *sessionTask, BOOL result))success
                                                                       failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 删除点播收藏记录（新）
 *
 * @return BOOL类型的result
 */
- (NSURLSessionDataTask *)request_DeleteVodFavoriteHistory_WithVodFavoriteInfo:(OMCVodFavoriteInfo *)
                                                                       vodFavoriteInfo
                                                                       success:(void (^)(NSURLSessionDataTask *sessionTask, BOOL result))success
                                                                       failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取点播搜索（按照节目命名匹配）
 *
 * @param keyword     关键字
 * @param start       列表开始位置
 * @param end         列表结束位置
 * @param subID       点播栏目ID
 * @param columnSize  portal翻页自用字段(当要对搜索结果进行翻页时，将从portal获取的响应数据里的同名参数值传回)
 * @param type        搜索类型，默认为0（0-返回搜索结果列表以及搜索结果分类信息；1-返回搜索结果列表信息，不返回搜索结果分类信息；）
 *
 * @return NSArray, NSArray
 */
- (NSURLSessionDataTask *)request_VodSearchByProgramName_WithKeyword:(NSString *)keyword
                                                               start:(NSInteger)start
                                                                 end:(NSInteger)end
                                                               subID:(NSString *)subID
                                                          columnSize:(NSString *)columnSize
                                                                type:(NSInteger)type
                                                             success:(void (^)(NSURLSessionDataTask *sessionTask, NSString *columnSize, NSString *count, NSArray<OMCProgramInfo *> *programsArr, NSArray<OMCSearchCategory *> * categoryArr))success
                                                             failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 查询点播收藏记录是否存在
 *
 * @param programID  节目ID
 *
 * @return BOOL类型的result
 */
- (NSURLSessionDataTask *)request_VodFavoriteExists_WithProgramID:(NSString *)programID
                                                          success:(void (^)(NSURLSessionDataTask *sessionTask, BOOL result))success
                                                          failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 根据programID查询点播播放记录信息
 *
 * @param programID  节目ID
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_VodPlayHistory_WithProgramID:(NSString *)programID
                                                       success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCCloudVodHistoryInfo *> *arr))success
                                                       failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;


@end

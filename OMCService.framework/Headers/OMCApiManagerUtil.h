//
//  OMCApiManagerUtil.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/10/29.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSError+Api.h"
#import "OMCMJExtension.h"

@interface OMCApiManagerUtil : NSObject

+ (void)parseDataDictionaryWithRootKey:(NSString *)rootKey
                      targetModelClass:(Class)modelClass
                       sessionDataTask:(NSURLSessionDataTask *)sessionTask
                        dataDictionary:(NSDictionary *)dataDic
                               success:(void (^)(NSURLSessionDataTask *task, NSArray *modelArray))ApiSuccessBlock
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *errorParse))ApiFailureBlock;

/**
 * 给对象的属性设置默认值
 */
+ (void)checkEntity:(id)object;

@end

//
//  NSString+OMCMJExtension.h
//  OMCMJExtensionExample
//
//  Created by OMCMJ Lee on 15/6/7.
//  Copyright (c) 2015年 小码哥. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCMJExtensionConst.h"

@interface NSString (OMCMJExtension)
/**
 *  驼峰转下划线（loveYou -> love_you）
 */
- (NSString *)omcmj_underlineFromCamel;
/**
 *  下划线转驼峰（love_you -> loveYou）
 */
- (NSString *)omcmj_camelFromUnderline;
/**
 * 首字母变大写
 */
- (NSString *)omcmj_firstCharUpper;
/**
 * 首字母变小写
 */
- (NSString *)omcmj_firstCharLower;

- (BOOL)omcmj_isPureInt;

- (NSURL *)omcmj_url;
@end

@interface NSString (OMCMJExtensionDeprecated_v_2_5_16)
- (NSString *)underlineFromCamel OMCMJExtensionDeprecated("请在方法名前面加上omcmj_前缀，使用omcmj_***");
- (NSString *)camelFromUnderline OMCMJExtensionDeprecated("请在方法名前面加上omcmj_前缀，使用omcmj_***");
- (NSString *)firstCharUpper OMCMJExtensionDeprecated("请在方法名前面加上omcmj_前缀，使用omcmj_***");
- (NSString *)firstCharLower OMCMJExtensionDeprecated("请在方法名前面加上omcmj_前缀，使用omcmj_***");
- (BOOL)isPureInt OMCMJExtensionDeprecated("请在方法名前面加上omcmj_前缀，使用omcmj_***");
- (NSURL *)url OMCMJExtensionDeprecated("请在方法名前面加上omcmj_前缀，使用omcmj_***");
@end

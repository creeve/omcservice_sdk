//
//  OMCApiBaseManager.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/10/24.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCApiClient.h"
#import "NSString+omc_NetApi.h"

#define OMCTask NSURLSessionDataTask

extern NSString * const kOMCPrefKeyLocation;
extern NSString * const kOMCPrefKeyToken;

@interface BaseUrlInfo : NSObject

@property(nonatomic, strong) NSArray <NSString *> *baseUrlsList;

@end
@interface ServerInfo : NSObject

@property(nonatomic, strong) NSArray <BaseUrlInfo *> *baseUrlInfoList;

@end

@interface OMCApiBaseManager : NSObject

//拼接完整请求地址，baseURL + Api path + base params + api params
//允许切换baseURL
//处理反馈数据中的公共参数结果，例如错误码等

+ (OMCApiBaseManager *)sharedManager;

@property (readonly, nonatomic, assign) BOOL isValidatingPortalAddress;

@property (nonatomic, assign) BOOL enableNetworkLog;

@property (nonatomic, copy) NSString *omc_pUser;
@property (nonatomic, copy) NSString *omc_pToken;
@property (nonatomic, copy) NSString *omc_pLocation;
@property (nonatomic, copy) NSString *omc_pVersion;
@property (nonatomic, copy) NSString *omc_pServerAddress;
@property (nonatomic, copy) NSString *omc_pSerialNumber;

@property (nonatomic, copy) NSString *omc_apiKey;
@property (nonatomic, copy) NSString *omc_appId;
@property (nonatomic, copy) NSString *omc_portalKey;

@property (nonatomic, copy) NSString *omc_pCityName;
@property (nonatomic, copy) NSString *omc_pCountyName;


+ (void)setAppId:(NSString *)appId
          appKey:(NSString *)appKey
      completion:(void (^)(BOOL success, NSError *error))completion;

+ (void)enableNetworkLog:(BOOL)enable;

+ (void)setServerAddress:(NSString *)serverAddress;

- (NSURLSessionDataTask *)request_ModelDataWith_GetPath:(NSString *)apiPath
                                             withParams:(NSDictionary *)paramsDic
                                                success:(void (^)(NSURLSessionDataTask *sessionTask, NSDictionary *dataDic))success
                                                failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

- (NSURLSessionDataTask *)request_ModelDataWith_PostPath:(NSString *)apiPath
                                              withParams:(NSDictionary *)paramsDic
                                          withBodyParams:(id)bodyParamsObj
                                                 success:(void (^)(NSURLSessionDataTask *sessionTask, NSDictionary *dataDic))success
                                                 failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 将符合格式要求的plist文件转换为ServerInfo对象
 */
- (ServerInfo *)ApiServerInfoWithPlistFile:(NSString *)plistName;

- (NSDictionary *)dictionaryByAppendingPublicParameterWithParameter:(NSDictionary *)parameter;

- (NSDictionary *)dictionaryByAppendingAuthPublicParameterWithParameter:(NSDictionary *)parameter;


/**
 将符合格式要求的json字符串转换为ServerInfo对象
 
 json格式如下：
 {
     "servers": [
         {
            "baseUrls": ["http://www.demo_111.com/test","http://www.demo_222.com/test"]
         },
         {
            "baseUrls": ["http://www.demo_333.com/test","http://www.demo_444.com/test"]
         }
     ]
 }
 */

- (ServerInfo *)ApiServerInfoWithJsonString:(NSString *)jsonStr;


/**
 *  校验ServerInfo中包含的baseUrl是否有效，启动后将会按照校验规则逐一校验baseUrl，并将拦截所有在校验过程中用户发起的网络请求，待校验完成后，使用新地址完成之前被拦截的所有用户请求，如果需要使用主备地址功能，可在App启动后其他网络请求之前，优先调用此函数
 *  
 *  校验规则：
 *  
 *   {
        "servers": [
            {
                "baseUrls": ["http://www.demo_111.com/test","http://www.demo_222.com/test"]
            },
            {
                "baseUrls": ["http://www.demo_333.com/test","http://www.demo_444.com/test"]
            }
         ]
     }
 *  
 *  ServerInfo使用如上格式的plist文件或json String转换而来；
 *  1. server item将按顺序逐个校验，前一个明确不可用后继续验证下一个；
 *  2. baseUrl item将并发验证，即使用baseUrl array中的地址同时发送请求，最快返回结果的将被认为有效；
 
 *  以上面地址举例，则优先同时向demo_111和demo_222地址发送请求；如果demo_222优先返回成功结果，则将baseUrl置为http://www.demo_222.com/test，不再进行后续测试；如果demo_111和demo_222请求均返回失败，则进入下一server item重复步骤1，即再次同时向demo_333和demo_444地址发送请求，采用二者中最快反馈正确结果的地址作为baseUrl
 *  
 *  返回参数：
 *      successBaseUrlStr: 如果成功找到通过验证的baseUrl，则返回；
 *      error: 如果未找到serverInfo中符合要求的baseUrl，则返回error，errorCode = -20005
 */

- (void)validatePortalAddressWithServerInfo:(ServerInfo *)serverInfo
                          completionHandler:(void (^)(NSString *successBaseUrlStr, NSError *error))completionHandler;

- (NSError *)validateResponseObj:(NSDictionary *)responseDic;

@end


//
//  WelcomeModels.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/10/29.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCMJExtension.h"
#import "CommonModels.h"

@interface OMCHorizontalWelcome : NSObject

@property (nonatomic, copy) NSString *programID;
@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *programName;
@property (nonatomic, copy) NSString *programDes;
@property (nonatomic, copy) NSString *actors;
@property (nonatomic, copy) NSString *director;
@property (nonatomic, copy) NSString *programTime;
@property (nonatomic, copy) NSString *provider;
@property (nonatomic, copy) NSString *categoryID;
@property (nonatomic, copy) NSString *columnID;
@property (nonatomic, copy) NSString *columnName;
@property (nonatomic, copy) NSString *categoryName;

@end


@interface OMCWelcomeProgram : NSObject

@property (nonatomic, copy) NSString *programID;
@property (nonatomic, copy) NSString *programName;
@property (nonatomic, copy) NSString *descriptionProgram;
@property (nonatomic, strong) NSArray <OMCResolutionImage *> *resolutionImageArr;
@property (nonatomic, copy) NSString *showType;
@property (nonatomic, copy) NSString *updateTime;

@end

@interface OMCVerticalWelcome : NSObject

@property (nonatomic, copy) NSString *columnID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *selectedImageUrl;
@property (nonatomic, copy) NSString *sortinx;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *descriptionWelcome;
@property (nonatomic, strong) NSArray <OMCWelcomeProgram *> *OMCWelcomeProgramsArr;

@end

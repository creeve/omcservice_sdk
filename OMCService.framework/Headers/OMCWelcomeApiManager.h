//
//  OMCWelcomeApiManager.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/10/29.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCApiBaseManager.h"
#import "OMCApiManagerUtil.h"
#import "WelcomeModels.h"

@interface OMCWelcomeApiManager : NSObject

+ (OMCWelcomeApiManager *)sharedManager;

- (NSURLSessionDataTask *)request_HorizontalWelcome_WithCounts:(NSInteger)counts
                                                       success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray <OMCHorizontalWelcome *> *horiWelcomeList))success
                                                       failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

- (NSURLSessionDataTask *)request_VerticalWelcome_WithCounts:(NSInteger)counts
                                                    columnID:(NSString *)columnID
                                                     success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray <OMCVerticalWelcome *> *vertWelcomeList))success
                                                     failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

@end

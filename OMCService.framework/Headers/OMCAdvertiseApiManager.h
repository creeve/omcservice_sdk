//
//  OMCAdvertiseApiManager.h
//  IvideoApiClient
//
//  Created by dvt04 on 16/11/17.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCApiBaseManager.h"
#import "OMCApiManagerUtil.h"
#import "CommonModels.h"

@interface OMCAdvertiseApiManager : NSObject

+ (OMCAdvertiseApiManager *)sharedManager;

- (NSURLSessionDataTask *)request_Advertisement_WithSuccess:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCAdvertisement *> *arr))success failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

@end

//
//  OMCUBAApiManager.h
//  IvideoApiClient
//
//  Created by dvt04 on 16/11/16.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCApiBaseManager.h"
#import "OMCApiManagerUtil.h"
#import "UBAModels.h"
#import "VodModels.h"

@interface OMCUBAApiManager : NSObject

+ (OMCUBAApiManager *)sharedManager;

/**
 * 获取栏目推荐节目列表
 *
 * @param start            获取节目排行的起始index
 * @param numberOfResults  每页显示条数
 * @param recommendTypeID  一级推荐栏目id（为空则默认全部）
 * @param type             0全部，1点播，2直播
 * @param resolution       分辨率（要获取的直播截图的分辨率,可以为空）
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_RecommendList_WithStart:(NSInteger)start
                                          numberOfResults:(NSInteger)numberOfResults
                                          recommendTypeID:(NSString *)recommendTypeID
                                                     type:(NSInteger)type
                                               resolution:(NSString *)resolution
                                                  success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCRecommendItem *> *arr))success
                                                  failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取点播节目排行列表
 *
 * @param start            获取点播节目排行起始index
 * @param numberOfResults  每页显示条数
 * @param columnID         栏目Id（空串表示获取全部的栏目，例如：电影、电视剧等）栏目的个数，可以由Portal系统配置文件配置。默认为5                                       
                           个栏目
 * @param secondaryTypeID  二级分类Id，为空标示获取所有
 * @param sortType         排行类型id，例如：1-精选（从UBA调用离线排行），2-最新（调用Portal内部）,3-全部（调用Portal内部，人工
                           排序方式进行显示）
 * @param location         地区（填日韩、欧美等对应的ID，非系统的地区参数，为空默认全部）
 * @param timeRange        时间范围，例如“2014-2015”
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_VodRankList_WithStart:(NSInteger)start
                                        numberOfResults:(NSInteger)numberOfResults
                                               columnID:(NSString *)columnID
                                        secondaryTypeID:(NSString *)secondaryTypeID
                                               sortType:(NSString *)sortType
                                               location:(NSString *)location
                                              timeRange:(NSString *)timeRange
                                                success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCVodRecommendItem *> *arr))success
                                                failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取指定节目相关推荐节目信息
 *
 * @param start            分页起始位置
 * @param numberOfResults  获取相关推荐节目的数量
 * @param epgID            epgId
 * @param epgSeriesID      直播epg系列的Id
 * @param programID        点播节目Id
 * @param type             类型：1.点播 2.直播 3.全部
 * @param resolution       分辨率（要获取的直播截图的分辨率，可以为空）
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_RelevantRecommendList_WithStart:(NSInteger)start
                                                  numberOfResults:(NSInteger)numberOfResults
                                                            epgID:(NSString *)epgID
                                                      epgSeriesID:(NSString *)epgSeriesID
                                                        programID:(NSString *)programID
                                                             type:(NSInteger)type
                                                       resolution:(NSString *)resolution
                                                          success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCRecommendItem *> *arr))success
                                                          failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 根据演员或导演获取节目接口
 *
 * @param name             演员导演名（以“，”分割）
 * @param assertID         epgID或者是点播节目ID
 * @param start            分页起始位置
 * @param numberOfResults  获取的数量
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_ProgramByArtist_WithName:(NSString *)name
                                                  assertID:(NSString *)assertID
                                                     start:(NSInteger)start
                                           numberOfResults:(NSInteger)numberOfResults
                                                   success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCRecommendItem *> *arr))success
                                                   failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 根据epg名称获取点播节目接口
 *
 * @param epgName          节目名称
 * @param start            分页的起始位置
 * @param numberOfResults  获取的数量
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_ProgramByEpg_WithEpgName:(NSString *)epgName
                                                     start:(NSInteger)start
                                           numberOfResults:(NSInteger)numberOfResults
                                                   success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCProgramInfo *> *arr))success
                                                   failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取直播系列节目详细信息
 *
 * @param epgID      直播epg的Id
 * @param channelID  直播频道ID
 *
 * @return OMCEpgSeriesDesInfo
 */
- (NSURLSessionDataTask *)request_LiveSeriesInfoList_WithEpgID:(NSString *)epgID
                                                     channelID:(NSString *)channelID
                                                       success:(void (^)(NSURLSessionDataTask *sessionTask, OMCEpgSeriesDesInfo *epgSeriesDesInfo))success
                                                       failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取直播节目详细信息
 *
 * @param epgID  直播epg的Id
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_LiveEpgSeriesDesInfo_WithEpgID:(NSString *)epgID
                                                         success:(void (^)(NSURLSessionDataTask *sessionTask, OMCLiveEpgSeriesDesInfo *liveEpgSeriesDesInfo))success
                                                         failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取频道实时排行接口
 *
 * @param resolution  分辨率（要获取的直播截图的分辨率，为空则返回默认分辨率）// 此参数暂不启用
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_LiveChannelTop_WithResolution:(NSString *)resolution
                                                        success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCRecommendItem *> *arr))success
                                                        failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取回看实时排行接口
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_ReseedTop_WithSuccess:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCPlayBackTopItem *> *arr))success failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取看了又看点播节目接口
 *
 * @param start            分页起始位置
 * @param numberOfResults  获取相关推荐节目的数量
 * @param programID        点播节目Id
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_LookRelevantRecommendList_WithStart:(NSInteger)start
                                                      numberOfResults:(NSInteger)numberOfResults
                                                            programID:(NSString *)programID
                                                              success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCRecommendItem *> *arr))success
                                                              failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 热搜词汇排行
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_HotSearchInfo_WithSuccess:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCHotSearchInfo *> *arr))success failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 首字母搜索联想词
 *
 * @param firstLetter  首字母
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_FirstWordSearch_WithFirstLetter:(NSString *)firstLetter
                                                          success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray *arr))success
                                                          failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 混合搜索
 *
 * @param keyword       关键字
 * @param start         列表开始位置
 * @param resultSize    返回个数（默认20）
 * @param categoryType  分类类型（0.频道，1.节目，空.全部）
 * @param channelId     频道Id（为空不作为查询条件）
 * @param categoryName  类型名称，（为空则返回全部分类，同时返回类型列表）
 * @param rootType     （1.点播，2.直播，空.全部）
 * @param resolution    分辨率（要获取的直播截图的分辨率，为空则返回默认的分辨率）
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_SearchByMix_WithKeyword:(NSString *)keyword
                                                    start:(NSInteger)start
                                               resultSize:(NSInteger)resultSize
                                             categoryType:(NSString *)categoryType
                                                channelId:(NSString *)channelId
                                             categoryName:(NSString *)categoryName
                                                 rootType:(NSString *)rootType
                                               resolution:(NSString *)resolution
                                                  success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCSearchResultInfo *> *arr))success
                                                  failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

@end

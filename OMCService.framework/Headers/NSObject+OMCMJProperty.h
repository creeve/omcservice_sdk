//
//  NSObject+OMCMJProperty.h
//  OMCMJExtensionExample
//
//  Created by OMCMJ Lee on 15/4/17.
//  Copyright (c) 2015年 小码哥. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCMJExtensionConst.h"

@class OMCMJProperty;

/**
 *  遍历成员变量用的block
 *
 *  @param property 成员的包装对象
 *  @param stop   YES代表停止遍历，NO代表继续遍历
 */
typedef void (^OMCMJPropertiesEnumeration)(OMCMJProperty *property, BOOL *stop);

/** 将属性名换为其他key去字典中取值 */
typedef NSDictionary * (^OMCMJReplacedKeyFromPropertyName)();
typedef id (^OMCMJReplacedKeyFromPropertyName121)(NSString *propertyName);
/** 数组中需要转换的模型类 */
typedef NSDictionary * (^OMCMJObjectClassInArray)();
/** 用于过滤字典中的值 */
typedef id (^OMCMJNewValueFromOldValue)(id object, id oldValue, OMCMJProperty *property);

/**
 * 成员属性相关的扩展
 */
@interface NSObject (OMCMJProperty)
#pragma mark - 遍历
/**
 *  遍历所有的成员
 */
+ (void)omcmj_enumerateProperties:(OMCMJPropertiesEnumeration)enumeration;

#pragma mark - 新值配置
/**
 *  用于过滤字典中的值
 *
 *  @param newValueFormOldValue 用于过滤字典中的值
 */
+ (void)omcmj_setupNewValueFromOldValue:(OMCMJNewValueFromOldValue)newValueFormOldValue;
+ (id)omcmj_getNewValueFromObject:(__unsafe_unretained id)object oldValue:(__unsafe_unretained id)oldValue property:(__unsafe_unretained OMCMJProperty *)property;

#pragma mark - key配置
/**
 *  将属性名换为其他key去字典中取值
 *
 *  @param replacedKeyFromPropertyName 将属性名换为其他key去字典中取值
 */
+ (void)omcmj_setupReplacedKeyFromPropertyName:(OMCMJReplacedKeyFromPropertyName)replacedKeyFromPropertyName;
/**
 *  将属性名换为其他key去字典中取值
 *
 *  @param replacedKeyFromPropertyName121 将属性名换为其他key去字典中取值
 */
+ (void)omcmj_setupReplacedKeyFromPropertyName121:(OMCMJReplacedKeyFromPropertyName121)replacedKeyFromPropertyName121;

#pragma mark - array model class配置
/**
 *  数组中需要转换的模型类
 *
 *  @param objectClassInArray          数组中需要转换的模型类
 */
+ (void)omcmj_setupObjectClassInArray:(OMCMJObjectClassInArray)objectClassInArray;
@end

@interface NSObject (OMCMJPropertyDeprecated_v_2_5_16)
+ (void)enumerateProperties:(OMCMJPropertiesEnumeration)enumeration OMCMJExtensionDeprecated("请在方法名前面加上omcmj_前缀，使用omcmj_***");
+ (void)setupNewValueFromOldValue:(OMCMJNewValueFromOldValue)newValueFormOldValue OMCMJExtensionDeprecated("请在方法名前面加上omcmj_前缀，使用omcmj_***");
+ (id)getNewValueFromObject:(__unsafe_unretained id)object oldValue:(__unsafe_unretained id)oldValue property:(__unsafe_unretained OMCMJProperty *)property OMCMJExtensionDeprecated("请在方法名前面加上omcmj_前缀，使用omcmj_***");
+ (void)setupReplacedKeyFromPropertyName:(OMCMJReplacedKeyFromPropertyName)replacedKeyFromPropertyName OMCMJExtensionDeprecated("请在方法名前面加上omcmj_前缀，使用omcmj_***");
+ (void)setupReplacedKeyFromPropertyName121:(OMCMJReplacedKeyFromPropertyName121)replacedKeyFromPropertyName121 OMCMJExtensionDeprecated("请在方法名前面加上omcmj_前缀，使用omcmj_***");
+ (void)setupObjectClassInArray:(OMCMJObjectClassInArray)objectClassInArray OMCMJExtensionDeprecated("请在方法名前面加上omcmj_前缀，使用omcmj_***");
@end

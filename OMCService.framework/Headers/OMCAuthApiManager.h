//
//  OMCAuthApiManager.h
//  OMCService
//
//  Created by GuochengLiu on 2017/9/25.
//  Copyright © 2017年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCApiBaseManager.h"

@class OMCAuthResultModel;

typedef void(^OMCAuthCompletion)(OMCAuthResultModel *resultModel, NSError *error);

// 鉴权节目类型
typedef NS_ENUM(NSUInteger, OMCAuthProgramType) {
    OMCAuthProgramTypeNone = 0,
    OMCAuthProgramTypeLive = 1,
    OMCAuthProgramTypeTimeshift = 2,
    OMCAuthProgramTypePlayback = 3,
    OMCAuthProgramTypeVod = 4,
};

// 鉴权的ErrorCode
typedef NS_ENUM(NSUInteger, OMCAuthErrorCode) {
    OMCAuthErrorCodeUnknown = -1,
    OMCAuthErrorCodeOK = 0,
    OMCAuthErrorCodeNeedLogin = 662,
    OMCAuthErrorCodeAuthFailed = 671,
    OMCAuthErrorCodeNeedBuy = 1011,
    OMCAuthErrorCodeReloginOtherDevice,
    OMCAuthErrorCodeReloginOtherArea = 9982,
    OMCAuthErrorCodeExceedLimitCanPlayFreeProgram = 702,
    OMCAuthErrorCodeExceedLimit = 693,
    OMCAuthErrorCodePasswordError = 669,
    OMCAuthErrorCodeUserUnactivated = 668,
    OMCAuthErrorCodeNeedBindCACard = 9980,
    OMCAuthErrorCodeUserCancelled = 9984,
    OMCAuthErrorCodeNeedExpiredAuthorization = 9945,
    OMCAuthErrorCodeUnBindCACard = 9946,
};

// 鉴权的ErrorReason
typedef NS_ENUM(NSUInteger, OMCAuthErrorReasonCode) {
    OMCAuthErrorReasonCodeOK = 0,
    OMCAuthErrorReasonCodeNeedLogin = 1,
    OMCAuthErrorReasonCodeNeedRelogin = 2,
    OMCAuthErrorReasonCodeNeedBuy = 3,
    OMCAuthErrorReasonCodeExpiredNeedRebuy = 4,
    OMCAuthErrorReasonCodeUseupNeedRebuy = 5,
    OMCAuthErrorReasonCodeUnknown = 6,
};

typedef NS_OPTIONS(NSUInteger, OMCACLMask) {
    OMCACLMaskNone = 0,
    OMCACLMaskLive = 1 << 0,
    OMCACLMaskTimeshift = 1 << 1,
    OMCACLMaskPlayback = 1 << 2,
    OMCACLMaskVod = 1 << 3,
};

@interface OMCAuthResultModel : NSObject

@property (nonatomic, assign) OMCAuthProgramType programType;   //节目类型（直播，时移，回看，点播）
@property (nonatomic, assign) BOOL hasRight;    // 是否有权限
@property (nonatomic, copy) NSString *originalMediaURL; // 原始媒资地址
@property (nonatomic, copy) NSString *finalMediaURL;    // 拼接后的媒资地址，有权限时可以用于播放
@property (nonatomic, copy) NSString *authResultURL;    // 鉴权接口返回的鉴权结果URL
@property (nonatomic, copy) NSError *authError;         // 鉴权失败时生成的错误
@property (nonatomic, copy) NSString *acl;              // 鉴权结果中的ACL值
@property (nonatomic, copy) NSString *errorCode;        // 鉴权结果中的错误码
@property (nonatomic, copy) NSString *errorReason;      // 鉴权结果中的错误原因

@end


typedef NS_ENUM(NSUInteger, OMCAuthType) {
    OMCAuthTypeDefault = 0, //普通鉴权，不需要传入secondAuthid
    OMCAuthTypeMixed = 1,   //混合鉴权，需要传入secondAuthid，常用于获取机顶盒的权限参数后作为secondAuthid传入
};

@interface OMCAuthApiManager : NSObject

+ (OMCAuthApiManager *)sharedManager;



/**
 鉴权接口（aut002-V2）

 @param authType 鉴权类型 0：正常鉴权 1：混合鉴权(只针对移动终端，如果为混合鉴权，必须传secondAuthid)(不可为空)
 @param secondAuthid 机顶盒ID（该值已实际运营中的机顶盒用户名为准，一般会为CA卡号或设备ID）；（可以为空）
 @param pid 点播节目ID号，当点击点播时，有该参数，该点播节目ID在AAA_V2版本时，传入的为assertid，AAA需要根据assertid查找对应的节目进行权限查询。
 @param cid 直播频道ID号，当点击直播、时移、回看时有该参数。
 @param epgID 暂不使用，留作扩展。
 @param drmToken 暂不使用留作扩展
 @param mediaURL 媒资URL，类似http://xxx.com/cctv.m3u8
 @param programType 节目类型（直播、时移、回看、点播）
 @param completion 鉴权结果回调
 @return NSURLSessionDataTask
 */
- (OMCTask *)authWithAuthType:(OMCAuthType)authType
                 secondAuthid:(NSString *)secondAuthid
                          pid:(NSString *)pid
                          cid:(NSString *)cid
                        epgID:(NSString *)epgID
                     drmToken:(NSString *)drmToken
                     mediaURL:(NSString *)mediaURL
                  programType:(OMCAuthProgramType)programType
                   completion:(OMCAuthCompletion)completion;


/**
 转换鉴权返回URL为鉴权结果

 @param authResult 鉴权返回URL串
 @param originalURL 视频的原始地址
 @param programType 鉴权节目类型
 @return 鉴权结果Model
 */
- (OMCAuthResultModel *)authResultModelWithURL:(NSString *)authResult
                                   originalURL:(NSString *)originalURL
                                   programType:(OMCAuthProgramType)programType;

+ (NSString *)errorMessageWithCode:(OMCAuthErrorCode)errorCode;

+ (NSString *)errorReasonWithCode:(OMCAuthErrorReasonCode)reasonCode;

+ (BOOL)checkRightWithACL:(NSInteger)acl programType:(OMCAuthProgramType)programType;

@end

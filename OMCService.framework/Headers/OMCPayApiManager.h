//
//  OMCPayApiManager.h
//  IvideoApiClient
//
//  Created by dvt04 on 16/11/18.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCApiBaseManager.h"
#import "OMCApiManagerUtil.h"
#import "PayModels.h"

@interface OMCPayApiManager : NSObject

+ (OMCPayApiManager *)sharedManager;

/**
 * 账户余额查询
 *
 * @param userName  账号（机顶盒号）
 *
 * @return OMCAccountBalanceInfo
 */
- (NSURLSessionDataTask *)request_AccountBalance_WithUserName:(NSString *)userName
                                                      success:(void (^)(NSURLSessionDataTask *sessionTask, OMCAccountBalanceInfo *accountBalanceInfo))success
                                                      failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 请求商品列表
 *
 * @param businessID   业务编号
 * @param productType  产品类型，0表示协议期片源，1表示按次点播片源
 * @param type         1.点播 2.直播
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_GoodsList_WithBusinessID:(NSString *)businessID
                                               productType:(NSInteger)productType
                                                      type:(NSInteger)type
                                                   success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCGoodsInfo *> *arr))success
                                                   failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取订单列表接口
 *
 * @param userName     账号（机顶盒号）
 * @param startTime    起始时间yyyyMMddhhmmss
 * @param endTime      结束时间yyyyMMddhhmmss
 * @param orderStatus  订单状态,1.待付款 2.已完成 3.已取消 4.全部状态
 *
 * @return NSArray
 */
- (NSURLSessionDataTask *)request_OrderList_WithUserName:(NSString *)userName
                                               startTime:(NSString *)startTime
                                                 endTime:(NSString *)endTime
                                             orderStatus:(NSInteger)orderStatus                                                 success:(void (^)(NSURLSessionDataTask *sessionTask, NSArray<OMCOrderInfo *> *arr))success
                                                 failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 支付余额
 *
 * @param password  密码（需要校验密码，此处手机终端需要手动输入一次用户密码，机顶盒用户则默认传递机顶盒号）
 * @param orderID   订单号
 *
 * @return OMCPayBalanceInfo
 */
- (NSURLSessionDataTask *)request_PayBalance_WithPassword:(NSString *)password
                                                  orderID:(NSString *)orderID
                                                  success:(void (^)(NSURLSessionDataTask *sessionTask, OMCPayBalanceInfo *payBalanceInfo))success
                                                  failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 请求订单
 *
 * @param goodsID   // 商品ID
 * @param userName  // 账号（或机顶盒号）
 * @param count     // 购买数量
 *
 * @return OMCOrderInfo
 */
- (NSURLSessionDataTask *)request_OrderFromBoss_WithGoodsID:(NSString *)goodsID
                                                   userName:(NSString *)userName
                                                      count:(NSInteger)count
                                                    success:(void (^)(NSURLSessionDataTask *sessionTask, OMCOrderInfo *orderInfo))success
                                                    failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;


@end

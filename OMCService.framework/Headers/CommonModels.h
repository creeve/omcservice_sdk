//
//  CommonModels.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/10/30.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OMCResolutionImage : NSObject

@property (nonatomic, copy) NSString *resolutionID;
@property (nonatomic, copy) NSString *imageUrl;

@end

@interface OMCResolutionMedia : NSObject

@property (nonatomic, copy) NSString *resolutionID;
@property (nonatomic, copy) NSString *mediaUrl;

@end

@interface OMCUpdateInfo : NSObject

@property (nonatomic, copy) NSString *appName;        // 程序名
@property (nonatomic, copy) NSString *appType;        // 类型
@property (nonatomic, copy) NSString *versionName;    // 版本名
@property (nonatomic, copy) NSString *versionCode;    // 版本号
@property (nonatomic, copy) NSString *appUrl;         // 程序下载地址
@property (nonatomic, copy) NSString *flag;           // 1-标识客户端必须要更新才能使用

@end

@interface OMCLocationInfo : NSObject

@property (nonatomic, copy) NSString *locationName; // 地区名称
@property (nonatomic, copy) NSString *locationID;   // 地区ID

@end

@interface OMCResolutionInfo : NSObject

@property (nonatomic, copy) NSString *resolutionID;     // 分辨率ID
@property (nonatomic, copy) NSString *bitrate;          // 码率
@property (nonatomic, copy) NSString *type;             // 海报类型，1表示横版，2表示竖版
@property (nonatomic, copy) NSString *resolutionDes;    // 分辨率描述

@end

@interface OMCBootPictureInfo : NSObject

@property (nonatomic, copy) NSString *adID;                 // ID号
@property (nonatomic, copy) NSString *bootPicDescription;   // 图片描述
@property (nonatomic, copy) NSString *duration;             // 显示时长
@property (nonatomic, copy) NSString *endDate;              // 结束日期，yyyyMMddHHmmss
@property (nonatomic, copy) NSString *imageUrl;             // 图片地址
@property (nonatomic, copy) NSString *locationid;           // 地区ID
@property (nonatomic, copy) NSString *picType;              // 图片类型(1：静态图；2：gif；3：视频)
@property (nonatomic, copy) NSString *resolution;           // 图片分辨率
@property (nonatomic, copy) NSString *startDate;            // 起始日期，只有在有效期内的图片才会被显示，多个有效时，随机显示
@property (nonatomic, copy) NSString *targetUrl;            // 跳转地址
@property (nonatomic, copy) NSString *version;              // 版本号

@end

@interface OMCLikeOrNotInfo : NSObject

@property (nonatomic, copy) NSString *isContinue;   // 标识是否可以继续点赞或踩，1：已赞 2：已踩 3：未赞未踩，可以继续
@property (nonatomic, copy) NSString *score;        // 评分，-1为没有相应信息
@property (nonatomic, copy) NSString *notLike;      // 踩的数量
@property (nonatomic, copy) NSString *viewNum;      // 浏览次数
@property (nonatomic, copy) NSString *like;         // 赞的数量

@end

@interface OMCAdvertisement : NSObject

@property (nonatomic, copy) NSString *adID;               // 广告节目ID
@property (nonatomic, copy) NSString *adTitle;            // 广告标题
@property (nonatomic, copy) NSString *adType;             // 广告类型(1：首页广告、2：片头广告、3：暂停广告，4：换台广告、5：音量调节广告)
@property (nonatomic, assign) NSInteger adIndex;          // 显示位置(type=1时有效，从0开始)
@property (nonatomic, copy) NSString *contentType;        // 内容类型(1：图片、2：视频)
@property (nonatomic, copy) NSString *adDescription;      // 广告文字描述
@property (nonatomic, copy) NSString *showUrl;            // 图片地址
@property (nonatomic, copy) NSString *targetUrl;          // 广告跳转地址
@property (nonatomic, assign) NSTimeInterval adDuration;  // 显示时长，单位为秒
@property (nonatomic, assign) BOOL canSkipByGuest;        // 表示该广告是否可被普通用户跳过
@property (nonatomic, assign) BOOL canSkipByMember;       // 表示该广告是否可被会员跳过

@end


@interface OMCCommentInfo : NSObject

@property (nonatomic, copy) NSString *commentID;     // 评论ID
@property (nonatomic, copy) NSString *userName;      // 用户名
@property (nonatomic, copy) NSString *headImageUrl;  // 用户头像
@property (nonatomic, copy) NSString *content;       // 评论信息
@property (nonatomic, copy) NSString *releaseTime;   // 评论发布时间

@end


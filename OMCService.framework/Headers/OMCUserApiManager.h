//
//  OMCUserApiManager.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/11/1.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMCApiBaseManager.h"
#import "OMCApiManagerUtil.h"
#import "UserModels.h"

@interface OMCUserApiManager : NSObject

+ (OMCUserApiManager *)sharedManager;

/**
 * 登录接口
 *
 * @param user       用户名
 * @param password   密码
 * @param protectedType  密码保护方法（传code值），空或者“0”为不加密，”1“：MD5，“2”：SHA-1，后续待扩展
 * @param drmInfo    DRM agent返回给终端的信息
 *
 * @return NSString
 */
- (NSURLSessionDataTask *)request_Login_WithUser:(NSString *)user
                                        password:(NSString *)password
                                       protected:(NSString *)protectedType
                                         drmInfo:(NSString *)drmInfo
                                         success:(void (^)(NSURLSessionDataTask *sessionTask, NSString *AAAToken, NSString *DRMToken))success
                                         failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 注册接口
 *
 * @param user           用户名
 * @param phoneNumber    手机号
 * @param email          电子邮件
 * @param password       密码
 * @param protectedType  密码保护方法（传code值），空或者“0”为不加密，“1”：MD5，“2”：SHA-1，后续待扩展
 * @param cardNumber     ca卡号
 * @param cardPassword   ca密码
 * @param accountID      账户ID
 * @param token          手机验证码，有效时间60s
 *
 * @return NSString
 */
- (NSURLSessionDataTask *)request_Regist_WithUser:(NSString *)user
                                      phoneNumber:(NSString *)phoneNumber
                                            email:(NSString *)email
                                         password:(NSString *)password
                                        protected:(NSString *)protectedType
                                       cardNumber:(NSString *)cardNumber
                                     cardPassword:(NSString *)cardPassword
                                        accountID:(NSString *)accountID
                                            token:(NSString *)token
                                          success:(void (^)(NSURLSessionDataTask *sessionTask, BOOL result))success
                                          failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 退出登录接口
 *
 * @return BOOL                                                                                                                                                                                                                                                      
 */
- (NSURLSessionDataTask *)request_Logout_WithSuccess:(void (^)(NSURLSessionDataTask *sessionTask, BOOL result))success failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 修改密码接口
 *
 * @param user         用户名
 * @param oldPassword  旧密码
 * @param newPassword  新密码
 * @param phoneNumber  手机号
 *
 * @return BOOL
 */
- (NSURLSessionDataTask *)request_ModifyPassword_WithUser:(NSString *)user
                                              oldPassword:(NSString *)oldPassword
                                              newPassword:(NSString *)newPassword
                                              phoneNumber:(NSString *)phoneNumber
                                                  success:(void (^)(NSURLSessionDataTask *sessionTask, BOOL result))success
                                                  failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 获取验证码接口
 *
 * @param phoneNumber  手机号
 * @param type         1：注册，2：更换手机号验证旧手机，3更换手机号验证新手机，4：密码丢失后设置密码
 *
 * @return NSString
 */
- (NSURLSessionDataTask *)request_GetToken_WithPhoneNumber:(NSString *)phoneNumber
                                                      type:(NSInteger)type
                                                   success:(void (^)(NSURLSessionDataTask *sessionTask, NSString *timeout))success
                                                   failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 校验验证码接口
 *
 * @param phoneNumber  手机号
 * @param token        验证码
 * @param type         1：注册，2：更换手机号验证旧手机，3更换手机号验证新手机，4：密码丢失后设置密码
 *
 * @return BOOL
 */
- (NSURLSessionDataTask *)request_CheckToken_WithPhoneNumber:(NSString *)phoneNumber
                                                       token:(NSString *)token
                                                        type:(NSInteger)type
                                                     success:(void (^)(NSURLSessionDataTask *sessionTask, BOOL result))success
                                                     failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 重置密码接口
 *
 * @param phoneNumber  手机号
 * @param token        验证码
 * @param protectedType     密码保护方法（传code值），空或者“0”为不加密，“1”：MD5，“2”：SHA-1，后续待扩展
 * @param newPassword  新密码
 *
 * @return BOOL
 */
- (NSURLSessionDataTask *)request_ResetPassword_WithPhoneNumber:(NSString *)phoneNumber
                                                          token:(NSString *)token
                                                      protected:(NSString *)protectedType
                                                    newPassword:(NSString *)newPassword
                                                        success:(void (^)(NSURLSessionDataTask *sessionTask, BOOL result))success
                                                        failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

// TODO: 鉴权接口暂时写到此处
/**
 * 终端鉴权接口
 *
 * @param programID     点播节目ID，点播鉴权无需传channelID
 * @param DRMtoken      DRMtoken，传入空值
 * @param channelID     直播频道ID，直播鉴权无需传programID
 * @param epgID         epg的ID
 * @param authType      鉴权类型 0：正常鉴权 1：混合鉴权(机顶盒与其他设备混合鉴权)(不可为空)
 * @param secondAuthid  混合鉴权中，第二种鉴权的标识,如果混合鉴权类型为0，当前值可为空；否则该值必填；（可以为空）
 *
 * @return NSString
 */
- (NSURLSessionDataTask *)request_Auth_WithprogramID:(NSString *)programID
                                            DRMtoken:(NSString *)DRMtoken
                                           channelID:(NSString *)channelID
                                               epgID:(NSString *)epgID
                                            authType:(NSString *)authType
                                        secondAuthid:(NSString *)secondAuthid
                                             success:(void (^)(NSURLSessionDataTask *sessionTask, NSString *authResult))success
                                             failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;

/**
 * 终端鉴权接口（v1）
 *
 * @param DRMtoken      暂不使用留作扩展
 * @param epgID         epg的ID，暂不使用，留作扩展
 * @param authType      鉴权类型 0：正常鉴权 1：混合鉴权(只针对移动终端，如果为混合鉴权，必须传secondAuthid)(不可为空)
 * @param secondAuthid  机顶盒ID（该值已实际运营中的机顶盒用户名为准，一般会为CA卡号或设备ID）；（可以为空）
 * @param token         表示用户登录token。AAA_V2版本中可简化为t。例如t=aabbcc
 * @param pid           点播节目ID号，当点击点播时，有该参数，该点播节目ID在AAA_V2版本时，传入的为assertid，AAA需要根据       
                        assertid查找对应的节目进行权限查询。AAA_V2版本中可简化为pid。例如pid=aabbcc
 * @param cid           直播频道ID号，当点击直播、时移、回看时有该参数。AAA_V2版本中可简化为cid。例如cid=aabbcc
 * @param userID        表示用户ID号。AAA_V2版本中可简化为u。例如u=aabbcc
 * @param pType         表示终端平台标识（1-android手机平台，2-androidPad，3-iPhone，4-iPad，8-机顶盒平台）。AAA_V2版本中可
                        简化为p。例如p=1
 * @param locationID    表示地区ID。AAA_V2版本中可简化为l。例如l=aabbcc
 * @param deviceID      表示设备ID号。AAA_V2版本中可简化为did。例如d=aabbcc
 * @param name          表示当v参数存在并等于2时，该参数为客户端访问的媒资名称，此参数由终端截取媒资播放地址获得，例如：一个媒资的地址为：http://59.48.222.244:8061/live/shuhuapindao_800.m3u8，则个该参数为终端截取播放地址后的shuhuapindao_800
 * @param version       表示AAA鉴权版本号，字符串型。例如，v=2
 *
 * @return NSString
 */
- (NSURLSessionDataTask *)request_Auth_V1_WithDRMtoken:(NSString *)DRMtoken
                                                 epgID:(NSString *)epgID
                                              authType:(NSString *)authType
                                          secondAuthid:(NSString *)secondAuthid
                                                 token:(NSString *)token
                                                   pid:(NSString *)pid
                                                   cid:(NSString *)cid
                                                userID:(NSString *)userID
                                          platformType:(NSInteger)pType
                                            locationID:(NSString *)locationID
                                              deviceID:(NSString *)deviceID
                                                  name:(NSString *)name
                                               version:(NSString *)version
                                               success:(void (^)(NSURLSessionDataTask *sessionTask, NSString *authResult))success failure:(void (^)(NSURLSessionDataTask *sessionTask, NSError *error))failure;


@end

//
//  OMCApiBaseManager+BaseUrl.h
//  IvideoApiClient
//
//  Created by dvt04 on 2016/11/7.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import "OMCApiBaseManager.h"

extern NSString *const DefaultBaseURLStr;

@interface OMCApiBaseManager (BaseUrl)

/*
 获取当前正在使用的Portal BaseUrl
 */
+ (NSString *)ApiBaseURLStr;

/*
 更换默认使用的Portal BaseUrl，并固化在NSUserDefaults
 */
+ (void)changeApiBaseURLStrTo:(NSString *)baseURLStr;

@end

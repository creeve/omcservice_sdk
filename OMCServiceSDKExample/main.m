//
//  main.m
//  OMCServiceSDKExample
//
//  Created by GuochengLiu on 2017/6/12.
//  Copyright © 2017年 sumavision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

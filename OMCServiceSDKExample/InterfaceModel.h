//
//  Interfaces.h
//  IvideoApiClient
//
//  Created by fudo on 16/11/18.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InterfaceModel : NSObject

@property (nonatomic, copy) NSString *errorData; // 错误数据
@property (nonatomic, copy) NSString *responseData; // 响应数据
@property (nonatomic, strong) NSArray *responseArr; // 响应数组, 用于生成picker
@property (nonatomic, strong) NSMutableDictionary *paramsDic;// 参数字典

@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *interfaceDes;
@property (nonatomic, copy) NSString *requestURL;
@property (nonatomic, copy) NSString *path;
@property (nonatomic, copy) NSString *rely;
@property (nonatomic, strong) NSArray *paramsArr; // 初始化参数控件用
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, assign) SEL sendMethod;
@property (nonatomic, assign) BOOL relied;

@property (nonatomic, assign) NSInteger segmentIndex;
@property (nonatomic, assign) NSInteger pickerSelectedRow;

- (instancetype)initWithDic: (NSDictionary *)dic;
+ (instancetype)interfaceWithDic: (NSDictionary *)dic;

@end

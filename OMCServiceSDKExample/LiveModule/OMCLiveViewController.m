//
//  OMCLiveViewController.m
//  OMCServiceDemoApp
//
//  Created by GuochengLiu on 2017/9/25.
//  Copyright © 2017年 sumavision. All rights reserved.
//

#import "OMCLiveViewController.h"

#import <OMCService/OMCService.h>

@interface OMCLiveViewController ()

@property (weak, nonatomic) IBOutlet UIButton *getLocationButton;
@property (weak, nonatomic) IBOutlet UILabel *getLocationLabel;
@property (weak, nonatomic) IBOutlet UIButton *getLiveCategoryButton;
@property (weak, nonatomic) IBOutlet UILabel *getLiveCategoryLabel;
@property (weak, nonatomic) IBOutlet UIButton *getChannelButton;
@property (weak, nonatomic) IBOutlet UILabel *getChannelLabel;
@property (weak, nonatomic) IBOutlet UIButton *authButton;
@property (weak, nonatomic) IBOutlet UILabel *authLabel;

@end

@implementation OMCLiveViewController
{
    OMCLocationInfo *_selectedLocation;
    
    OMCLiveCategory *_selectedLiveCategory;
    
    OMCChannelInfo *_selectedChannel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"直播实现";
    self.getLiveCategoryButton.enabled = NO;
    self.getChannelButton.enabled = NO;
    self.authButton.enabled = NO;
}

- (IBAction)onHitGetLocationButton:(UIButton *)sender {
    
    [[OMCCommonApiManager sharedManager] request_LocationInfo_WithLocationID:nil success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCLocationInfo *> *arrLocations) {
        NSLog(@"地区列表 %@", arrLocations);
        
        _selectedLocation = arrLocations[0];
        [OMCApiBaseManager sharedManager].omc_pLocation = _selectedLocation.locationID;
        
        self.getLocationLabel.text = [NSString stringWithFormat:@"选择了地区 id:%@, name:%@", _selectedLocation.locationID, _selectedLocation.locationName];
        self.getLiveCategoryButton.enabled = YES;
        
    } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
        NSLog(@"Get location error %@", error.localizedDescription);
    }];
}

- (IBAction)onHitGetLiveCategoryButton:(UIButton *)sender {
    [[OMCLiveApiManager sharedManager] request_LiveCategoryList_WithSuccess:^(NSURLSessionDataTask *task, NSArray<OMCLiveCategory *> *arrayData) {
        NSLog(@"直播分类列表 %@", arrayData);
        _selectedLiveCategory = arrayData[0];
        self.getLiveCategoryLabel.text = [NSString stringWithFormat:@"选择了直播分类 id:%@, name:%@", _selectedLiveCategory.categoryID, _selectedLiveCategory.categoryName];
        
        self.getChannelButton.enabled = YES;
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Get live Category error %@", error.localizedDescription);
    }];
}

- (IBAction)onHitGetChannelButton:(UIButton *)sender {
    [[OMCLiveApiManager sharedManager] request_ChannelList_WithCategoryId:_selectedLiveCategory.categoryID start:0 end:16 channelName:nil programName:nil sortType:OMCLiveSortTypeSTB success:^(NSURLSessionDataTask *task, NSArray<OMCChannelInfo *> *arrayData) {
        NSLog(@"直播频道列表 %@", arrayData);
        _selectedChannel = arrayData[0];
        self.getChannelLabel.text = [NSString stringWithFormat:@"选择了直播频道 id:%@, name:%@", _selectedChannel.channelID, _selectedChannel.channelName];
        self.authButton.enabled = YES;
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Get live channel error %@", error.localizedDescription);
    }];
}

- (IBAction)onHitAuthButton:(UIButton *)sender {
    
    OMCResolutionMedia *media = _selectedChannel.channelUrl[0];
    
    [[OMCAuthApiManager sharedManager] authWithAuthType:OMCAuthTypeDefault secondAuthid:nil pid:nil cid:_selectedChannel.channelID epgID:nil drmToken:nil mediaURL:media.mediaUrl programType:OMCAuthProgramTypeLive completion:^(OMCAuthResultModel *resultModel, NSError *error) {
        
        
        // 可以尝试切换其他直播分类，直播频道来验证其他鉴权情况
        
        if (resultModel.hasRight == YES) {
            self.authLabel.text = [NSString stringWithFormat:@"鉴权通过，播放地址 %@",
                                   resultModel.finalMediaURL];
        } else {
            self.authLabel.text = [NSString stringWithFormat:@"鉴权没通过: [%@]%@",
                                   resultModel.errorCode, resultModel.authError.localizedDescription];
        }
        
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

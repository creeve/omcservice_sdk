//
//  InterfaceMethods.m
//  IvideoApiClient
//
//  Created by fudo on 16/11/22.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import "InterfaceMethods.h"
#import "define.h"
#import <OMCService/OMCService.h>

@implementation InterfaceMethods

+ (instancetype)sharedMethod
{
    static InterfaceMethods *__sharedMethod = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedMethod = [[InterfaceMethods alloc] init];
    });
    
    return __sharedMethod;
}

- (NSIndexPath *)prepareForSection: (NSInteger)section Row: (NSInteger)row
{
    InterfaceModel *model;
    
    switch (section) {
        case baseData:
            model = commonArr[row];
            break;
        case vod:
            model = vodArr[row];
            break;
        case live:
            model = liveArr[row];
            break;
        case user:
            model = userArr[row];
            break;
        case uba:
            model = ubaArr[row];
            break;
        case pay:
            model = payArr[row];
            break;
        default:
            break;
    }
    
    model.status = 1;
    model.pickerSelectedRow = 0;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    [self.delegate updateCellAtIndexPath:indexPath];
    
    return indexPath;
}

- (void)successWithSession: (NSURLSessionDataTask *)sessionTask indexPath: (NSIndexPath *)indexPath
{
    InterfaceModel *model;
    
    switch (indexPath.section)
    {
        case baseData:
            model = commonArr[indexPath.row];
            break;
        case vod:
            model = vodArr[indexPath.row];
            break;
        case live:
            model = liveArr[indexPath.row];
            break;
        case user:
            model = userArr[indexPath.row];
            break;
        case uba:
            model = ubaArr[indexPath.row];
            break;
        case pay:
            model = payArr[indexPath.row];
            break;
        default:
            break;
    }
    NSString *url = [sessionTask.currentRequest.URL absoluteString];
    model.requestURL = url;
    model.segmentIndex = 1;
    model.status = 3;
    model.errorData = @"";
    model.responseData = [[sessionTask getIvideoApiResponseDataString] stringByReplacingOccurrencesOfString:@"," withString:@",\n"];
    model.responseData = [model.responseData stringByReplacingOccurrencesOfString:@"{" withString:@"{\n"];
    model.responseData = [model.responseData stringByReplacingOccurrencesOfString:@"}" withString:@"\n}"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate updateCellAtIndexPath:indexPath];
    });
    
    NSLog(@"%@成功，%@", model.interfaceDes, model.responseData);
}

- (void)failureWithSession: (NSURLSessionDataTask *)sessionTask error: (NSError *)error indexPath: (NSIndexPath *)indexPath
{
    InterfaceModel *model;
    
    switch (indexPath.section)
    {
        case baseData:
            model = commonArr[indexPath.row];
            break;
        case vod:
            model = vodArr[indexPath.row];
            break;
        case live:
            model = liveArr[indexPath.row];
            break;
        case user:
            model = userArr[indexPath.row];
            break;
        case uba:
            model = ubaArr[indexPath.row];
            break;
        case pay:
            model = payArr[indexPath.row];
            break;
        default:
            break;
    }
    NSString *url = [sessionTask.currentRequest.URL absoluteString];
    model.requestURL = url;
    model.segmentIndex = 2;
    model.status = 2;
    model.errorData = [NSString stringWithFormat:@"%@", error];
    model.responseData = @"";
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate updateCellAtIndexPath:indexPath];
    });
    
    NSLog(@"%@失败, %@", model.interfaceDes, error);
}

#pragma mark - 基础数据接口

// 1.3
- (void)feedback {
    
    NSIndexPath *indexPath         = [self prepareForSection:baseData Row:feedback];
    InterfaceModel *model          = commonArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    NSURLSessionDataTask *task =
    [[OMCCommonApiManager sharedManager] request_FeedBack_WithEmail:paramsDic[@"email"]
                                                              phone:paramsDic[@"phone"]
                                                         suggestion:paramsDic[@"suggestion"]
                                                               mode:paramsDic[@"mode"]
                                                         appVersion:paramsDic[@"appVersion"]
                                                         sdkVersion:paramsDic[@"sdkVersion"]
                                                       manufacturer:paramsDic[@"manufacturer"]
                                                            success:^(NSURLSessionDataTask *sessionTask, BOOL result) {
                                                                [self successWithSession:sessionTask indexPath:indexPath];
                                                            }
                                                            failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                            }];
    NSLog(@"%@", task);
}

// 1.4
- (void)configInfo
{
    NSIndexPath *indexPath         = [self prepareForSection:baseData Row:configInfo];
    InterfaceModel *model          = commonArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;

    NSURLSessionDataTask *task =
    [[OMCCommonApiManager sharedManager] request_ConfigInfo_WithWhat:paramsDic[@"what"]
                                                             success:^(NSURLSessionDataTask *sessionTask, NSDictionary *dictResult) {
                                                                 [self successWithSession:sessionTask indexPath:indexPath];
                                                             }
                                                             failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                 [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                             }];
    NSLog(@"%@", task);
}

// 1.6
- (void)locationInfo
{
    NSIndexPath *indexPath         = [self prepareForSection:baseData Row:locationInfo];
    InterfaceModel *model          = commonArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    NSURLSessionDataTask *task =
    [[OMCCommonApiManager sharedManager] request_LocationInfo_WithLocationID:paramsDic[@"locationID"]
                                                                     success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCLocationInfo *> *arrLocations)
                                                                     {
                                                                         model.responseArr = arrLocations;
                                                                         // 设置默认locationID
                                                                         if (arrLocations.count > 0)
                                                                         {
                                                                             commonLocationID = arrLocations[0].locationID;
                                                                         }
                                                                         
                                                                         [self successWithSession:sessionTask indexPath:indexPath];
                                                                         
                                                                     }
                                                                     failure:^(NSURLSessionDataTask *sessionTask, NSError *error)
                                                                     {
                                                                         [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                     }];
    NSLog(@"%@", task);
}

// 1.7
- (void)serverTime
{
    NSIndexPath *indexPath         = [self prepareForSection:baseData Row:serverTime];
    InterfaceModel *model          = commonArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    NSURLSessionDataTask *task =
    [[OMCCommonApiManager sharedManager] request_ServerTime_WithDateFormat:paramsDic[@"dateFormat"]
                                                                   success:^(NSURLSessionDataTask *sessionTask, NSString *serverTime) {
                                                                       [self successWithSession:sessionTask indexPath:indexPath];
                                                                   }
                                                                   failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                       [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                   }];
    
    NSLog(@"%@", task);
}

// 1.8
- (void)portalServerIP
{
    NSIndexPath *indexPath = [self prepareForSection:baseData Row:portalServerIP];
    
    NSURLSessionDataTask *task = [[OMCCommonApiManager sharedManager] request_portalServerIP_WithSuccess:^(NSURLSessionDataTask *sessionTask, NSDictionary *portalServerInfo) {
        [self successWithSession:sessionTask indexPath:indexPath];
    } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
        [self failureWithSession:sessionTask error:error indexPath:indexPath];
    }];
    NSLog(@"%@", task);
}

// 1.9
- (void)resolution
{
    NSIndexPath *indexPath = [self prepareForSection:baseData Row:resolution];
    
    NSURLSessionDataTask *task = [[OMCCommonApiManager sharedManager] request_Resolution_WithSuccess:^(NSURLSessionDataTask *sessionTask, NSArray<OMCResolutionInfo *> *arrResolution) {
        [self successWithSession:sessionTask indexPath:indexPath];
    } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
        [self failureWithSession:sessionTask error:error indexPath:indexPath];
    }];
    NSLog(@"%@", task);
}

// 1.10

// 1.11
- (void)bootPicture
{
    NSIndexPath *indexPath = [self prepareForSection:baseData Row:bootPicture];
    
    NSURLSessionDataTask *task = [[OMCCommonApiManager sharedManager] request_BootPicture_WithSuccess:^(NSURLSessionDataTask *sessionTask, NSArray <OMCBootPictureInfo *> *arrBootPictures) {
        [self successWithSession:sessionTask indexPath:indexPath];
    } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
        [self failureWithSession:sessionTask error:error indexPath:indexPath];
    }];
    NSLog(@"%@", task);
}

// 1.13
- (void)advertisement
{
    NSIndexPath *indexPath = [self prepareForSection:baseData Row:advertisement];
    
    NSURLSessionDataTask *task = [[OMCCommonApiManager sharedManager] request_Advertisement_WithSuccess:^(NSURLSessionDataTask *sessionTask, NSArray *arrAds) {
        [self successWithSession:sessionTask indexPath:indexPath];
    } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
        [self failureWithSession:sessionTask error:error indexPath:indexPath];
    }];
    NSLog(@"%@", task);
}

// 1.14, 已废弃
bool isPerforming_LikeOrNot;
- (void)likeOrNot {
    return;
#if 0
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_LikeOrNot)
    {
        isPerforming_LikeOrNot = true;
        [self programInfoList]; // 2.11
        return;
    }
    
    isPerforming_LikeOrNot = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:baseData Row:likeOrNot];
    InterfaceModel      *model     = commonArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if ([paramsDic[@"operateType"] isEqualToString:@""])
    {
        paramsDic[@"operateType"] = @"1"; // 为空时，给默认值1
    }
    if (![commonProgramID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"programID"] = commonProgramID; // commonProgramID不为初始值时，才传给参数
    }
    if (![commonChannelID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"channelID"] = commonChannelID;
    }
    
    NSURLSessionDataTask *task =
    [[OMCCommonApiManager sharedManager] request_LikeOrNot_WithOperateType:paramsDic[@"operateType"]
                                                                 programID:paramsDic[@"programID"]
                                                                 channelID:paramsDic[@"channelID"]
                                                                   success:^(NSURLSessionDataTask *sessionTask, NSDictionary *dicLikeOrNot) {
                                                                       [self successWithSession:sessionTask indexPath:indexPath];
                                                                   }
                                                                   failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                       [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                   }];
    NSLog(@"%@", task);
#endif
}

// 1.15
- (void)portalKey
{
    NSIndexPath *indexPath = [self prepareForSection:baseData Row:portalKey];
    
    NSURLSessionDataTask *task = [[OMCCommonApiManager sharedManager] request_PortalKey_WithSuccess:^(NSURLSessionDataTask *sessionTask, NSString *portalKey) {
        [self successWithSession:sessionTask indexPath:indexPath];
    } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
        [self failureWithSession:sessionTask error:error indexPath:indexPath];
    }];
    NSLog(@"%@", task);
}

// 1.16
- (void)configInfoByKeys
{
    NSIndexPath         *indexPath = [self prepareForSection:baseData Row:configInfoByKeys];
    InterfaceModel      *model     = commonArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;

    NSURLSessionDataTask *task =
    [[OMCCommonApiManager sharedManager] request_ConfigInfoByKeys_WithKeys:paramsDic[@"keys"]
                                                                   success:^(NSURLSessionDataTask *sessionTask, NSDictionary *dicConfigs) {
                                                                       [self successWithSession:sessionTask indexPath:indexPath];
                                                                   }
                                                                   failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                       [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                   }];
    NSLog(@"%@", task);
}

// 1.17
- (void)configV1
{
    NSIndexPath         *indexPath = [self prepareForSection:baseData Row:configV1];
    InterfaceModel      *model     = commonArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:0];

    [dic setObject:paramsDic[@"filename"] forKey:paramsDic[@"filename"]];

    NSURLSessionDataTask *task =
    [[OMCCommonApiManager sharedManager] request_Config_V1_WithConfigInfo:dic
                                                                  success:^(NSURLSessionDataTask *sessionTask, NSDictionary *dicConfigs) {
                                                                      [self successWithSession:sessionTask indexPath:indexPath];
                                                                  }
                                                                  failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                      [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                  }];
    NSLog(@"%@", task);
}

// 1.18
static bool isPerforming_LikeOrNotV1;
- (void)likeOrNotV1
{
    // 加上判断isPerforming_LikeOrNotV1状态，防止依赖接口执行完之后，依赖参数仍为空，出现死循环
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_LikeOrNotV1)
    {
        isPerforming_LikeOrNotV1 = true;
        [self programInfoList]; // 2.11
        return;
    }
    
    isPerforming_LikeOrNotV1 = false;
    
    NSIndexPath *indexPath = [self prepareForSection:baseData Row:likeOrNotV1];
    InterfaceModel *model = commonArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if ([paramsDic[@"operateType"] isEqualToString:@""])
    {
        paramsDic[@"operateType"] = @"3";
    }
    if (![commonProgramID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"programID"] = commonProgramID;
    }
    if (![commonChannelID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"channelID"] = commonChannelID;
    }
    
    
    NSURLSessionDataTask *task =
    [[OMCCommonApiManager sharedManager] request_LikeOrNot_V1_WithOperateType:paramsDic[@"operateType"]
                                                                    programID:paramsDic[@"programID"]
                                                                    channelID:paramsDic[@"channelID"]
                                                                programItemID:paramsDic[@"programItemID"]
                                                                      success:^(NSURLSessionDataTask *sessionTask, OMCLikeOrNotInfo *likeOrNotInfo) {
                                                                          [self successWithSession:sessionTask indexPath:indexPath];
                                                                      }
                                                                      failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                          [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                      }];
    NSLog(@"%@", task);
}

#pragma mark - 点播接口

// 2.1
- (void)vodHotSearchInfo
{
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:vodHotSearchInfo];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if ([paramsDic[@"count"] isEqualToString:@""])
    {
        paramsDic[@"count"] = @"5";
    }
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_VodHotSearchInfo_WithCount:[paramsDic[@"count"] integerValue]
                                                                 success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCKeywordsInfo *> *arr) {
                                                                     [self successWithSession:sessionTask indexPath:indexPath];
                                                                 }
                                                                 failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                     [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                 }];
    NSLog(@"%@", task);
}

// 2.2
- (void)vodSearch
{
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:vodSearch];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"end"] isEqualToString:@""])
    {
        paramsDic[@"end"] = @"5";
    }
    if ([paramsDic[@"type"] isEqualToString:@""])
    {
        paramsDic[@"type"] = @"0";
    }
    if (![commonSubID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"subID"] = commonSubID;
    }
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_VodSearch_WithKeyword:paramsDic[@"keyword"]
                                                              start:[paramsDic[@"start"] integerValue]
                                                                end:[paramsDic[@"end"] integerValue]
                                                              subID:paramsDic[@"subID"]
                                                         columnSize:paramsDic[@"columnSize"]
                                                               type:[paramsDic[@"type"] integerValue]
                                                            success:^(NSURLSessionDataTask *sessionTask, OMCSearchResult *searchResult) {
                                                                model.responseArr = searchResult.arrCategory;
                                                                
                                                                if (searchResult.arrCategory.count > 0)
                                                                {
                                                                    OMCSearchCategory *category = searchResult.arrCategory[0];
                                                                    commonSubID = category.subId;
                                                                }
                                                                
                                                                [self successWithSession:sessionTask indexPath:indexPath];
                                                                
                                                                // 2.34
                                                                if (isPerforming_VodSearchByProgramName)
                                                                {
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        [self vodSearchByProgramName];
                                                                    });
                                                                }
                                                            }
                                                            failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                
                                                                // 2.34
                                                                isPerforming_VodSearchByProgramName = false;
                                                            }];
    NSLog(@"%@", task);
}

// 2.3
- (void)findProgramByName
{
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:findProgramByName];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"end"] isEqualToString:@""])
    {
        paramsDic[@"end"] = @"5";
    }
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_findProgramByName_WithProgramName:paramsDic[@"programName"]
                                                                      startTime:paramsDic[@"startTime"]
                                                                         tvFreq:paramsDic[@"tvFreq"]
                                                                   tvSymbolRate:paramsDic[@"tvSymbolRate"]
                                                                   tvModulation:paramsDic[@"tvModulation"]
                                                                tvProgramNumber:paramsDic[@"tvProgramNumber"]
                                                                     tvVideoPID:paramsDic[@"tvVideoPID"]
                                                                     tvAudioPID:paramsDic[@"tvAudioPID"]
                                                                          start:[paramsDic[@"start"] integerValue]
                                                                            end:[paramsDic[@"end"] integerValue]
                                                                        success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCProgramInfo *> *arr)
                                                                        {
                                                                            [self successWithSession:sessionTask indexPath:indexPath];
                                                                        }
                                                                        failure:^(NSURLSessionDataTask *sessionTask, NSError *error)
                                                                        {
                                                                            [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                        }];
    NSLog(@"%@", task);
}

// 2.5
- (void)vodWelcomeList
{
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:vodWelcomeList];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if ([paramsDic[@"counts"] isEqualToString:@""])
    {
        paramsDic[@"counts"] = @"5";
    }
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_VodWelcomeList_WithCounts:[paramsDic[@"counts"] integerValue]
                                                                success:^(NSURLSessionDataTask *sessionTask, NSArray *arr) {
                                                                    [self successWithSession:sessionTask indexPath:indexPath];
                                                                }
                                                                failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                    [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                }];
    NSLog(@"%@", task);
}

// 2.6
- (void)horizontalWelcome
{
    NSIndexPath        *indexPath  = [self prepareForSection:vod Row:horizontalWelcome];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if ([paramsDic[@"counts"] isEqualToString:@""])
    {
        paramsDic[@"counts"] = @"5";
    }
    
    NSURLSessionDataTask *task =
    [[OMCWelcomeApiManager sharedManager] request_HorizontalWelcome_WithCounts:[paramsDic[@"counts"] integerValue]
                                                                       success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCHorizontalWelcome *> *horiWelcomeList) {
                                                                           [self successWithSession:sessionTask indexPath:indexPath];
                                                                       }
                                                                       failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                           [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                       }];
    NSLog(@"%@", task);
}

// 2.7
static bool isPerforming_VerticalWelcome;
- (void)verticalWelcome
{
    if ([commonColumnID isEqualToString:INIT_DEFAULT] && !isPerforming_VerticalWelcome)
    {
        isPerforming_VerticalWelcome = true;
        [self columnInfoList];// 依赖2.8
        return;
    }
    
    isPerforming_VerticalWelcome = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:verticalWelcome];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if ([paramsDic[@"counts"] isEqualToString:@""])
    {
        paramsDic[@"counts"] = @"5";
    }
    if (![commonColumnID isEqualToString:@""])
    {
        paramsDic[@"columnID"] = commonColumnID;
    }
    
    NSURLSessionDataTask *task =
    [[OMCWelcomeApiManager sharedManager] request_VerticalWelcome_WithCounts:[paramsDic[@"counts"] integerValue]
                                                                    columnID:paramsDic[@"columnID"]
                                                                     success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCHorizontalWelcome *> *horiWelcomeList) {
                                                                         [self successWithSession:sessionTask indexPath:indexPath];
                                                                     }
                                                                     failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                         [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                     }];
    NSLog(@"%@", task);
}


// 2.8
- (void)columnInfoList
{
    NSIndexPath    *indexPath = [self prepareForSection:vod Row:columnInfoList];
    InterfaceModel *model     = vodArr[indexPath.row];
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_ColumnInfoList_WithSuccess:^(NSURLSessionDataTask *sessionTask, NSArray<OMCVodColumn *> *arr)
    {
        model.responseArr = arr;
        if (arr.count > 0)
        {
            commonColumnID = arr[0].columnID;
        }
        
        [self successWithSession:sessionTask indexPath:indexPath];
        
        // 成功, 2.7
        if (isPerforming_VerticalWelcome)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self verticalWelcome];
            });
        }
        // 成功, 2.9
        if (isPerforming_categoryInfoList)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self categoryInfoList];
            });
        }
        // 成功, 2.10
        if (isPerforming_FilterList)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self filterList];
            });
        }
        // 成功, 5.2
        if (isPerforming_VodRankList)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self vodRankList];
            });
        }
        
    }
    failure:^(NSURLSessionDataTask *sessionTask, NSError *error)
    {
        [self failureWithSession:sessionTask error:error indexPath:indexPath];
        
        // 失败, 2.7
        isPerforming_categoryInfoList = false;
        // 失败, 2.9
        isPerforming_categoryInfoList = false;
        // 失败, 2.10
        isPerforming_FilterList = false;
        // 失败, 5.2
        isPerforming_VodRankList = false;
    }];
    
    NSLog(@"%@", task);
}

// 2.9
bool isPerforming_categoryInfoList;
- (void)categoryInfoList
{
    if ([commonColumnID isEqualToString:INIT_DEFAULT] && !isPerforming_categoryInfoList)
    {
        isPerforming_categoryInfoList = true;
        [self columnInfoList]; // 依赖2.8
        return;
    }
    
    isPerforming_categoryInfoList = false;

    NSIndexPath         *indexPath = [self prepareForSection:vod Row:categoryInfoList];
    InterfaceModel      *model = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if (![commonColumnID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"columnID"] = commonColumnID;
    }
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_CategoryInfoList_WithColumnID:paramsDic[@"columnID"]
                                                                    success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCVodCategory *> *arr)
                                                                    {
                                                                        model.responseArr = arr;
                                                                        if (arr.count > 0)
                                                                        {
                                                                            commonCategoryID = arr[0].categoryID;
                                                                        }
                                                                        
                                                                        [self successWithSession:sessionTask indexPath:indexPath];
                                                                        
                                                                        // 2.11
                                                                        if (isPerforming_ProgramInfoList)
                                                                        {
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                [self programInfoList];
                                                                            });
                                                                        }
                                                                        // 2.14
                                                                        if (isPerforming_ProgramCount)
                                                                        {
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                [self programCount];
                                                                            });
                                                                        }
                                                                        // 2.16
                                                                        if (isPerforming_ProgramInfosByLabel)
                                                                        {
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                [self programInfosByLabel];
                                                                            });
                                                                        }
                                                                        
                                                                    }
                                                                    failure:^(NSURLSessionDataTask *sessionTask, NSError *error)
                                                                    {
                                                                        [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                        
                                                                        // 2.11
                                                                        isPerforming_ProgramInfoList = false;
                                                                        // 2.14
                                                                        isPerforming_ProgramCount = false;
                                                                        // 2.16
                                                                        isPerforming_ProgramInfosByLabel = false;
                                                                    }];
    NSLog(@"%@", task);
}

// 2.10
static bool isPerforming_FilterList;
- (void)filterList
{
    if ([commonColumnID isEqualToString:INIT_DEFAULT] && !isPerforming_FilterList)
    {
        isPerforming_FilterList = true;
        [self columnInfoList]; // 依赖2.8
        return;
    }
    
    isPerforming_FilterList = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:filterList];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if (![commonColumnID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"columnID"] = commonColumnID;
    }
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_FilterList_WithColumnID:paramsDic[@"columnID"]
                                                              success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCVodCategory *> *categoryArr, NSArray<OMCGenresInfo *> *genresArr, NSArray<OMCLocationInfo *> *locationArr, NSArray<OMCYearInfo *> *yearArr) {
                                                                  [self successWithSession:sessionTask indexPath:indexPath];
                                                              }
                                                              failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                  [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                              }];
    NSLog(@"%@", task);
}

// 2.11 点播节目列表
static bool isPerforming_ProgramInfoList;
- (void)programInfoList
{
    
    if ([commonCategoryID isEqualToString:INIT_DEFAULT] && !isPerforming_ProgramInfoList) {
        isPerforming_ProgramInfoList = true;
        [self categoryInfoList]; // 依赖2.9
        return;
    }
    
    isPerforming_ProgramInfoList = false;
    
    NSIndexPath *indexPath = [self prepareForSection:vod Row:programInfoList];
    InterfaceModel *model = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;

    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"end"] isEqualToString:@""])
    {
        paramsDic[@"end"] = @"5";
    }
    if ([paramsDic[@"sortType"] isEqualToString:@""])
    {
        paramsDic[@"sortType"] = @"0";
    }
    if ([paramsDic[@"location"] isEqualToString:@""])
    {
        paramsDic[@"location"] = @"0";
    }
    
    if (![commonColumnID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"columnID"] = commonColumnID;
    }
    if (![commonCategoryID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"categoryID"] = commonCategoryID;
    }

    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_ProgramInfoList_WithColumnID:paramsDic[@"columnID"]
                                                                categoryID:paramsDic[@"categoryID"]
                                                                     start:[paramsDic[@"start"] integerValue]
                                                                       end:[paramsDic[@"end"] integerValue]
                                                               programName:paramsDic[@"programName"]
                                                                queryValue:paramsDic[@"queryValue"]
                                                                  sortType:[paramsDic[@"sortType"] integerValue]
                                                                      year:paramsDic[@"year"]
                                                                  location:paramsDic[@"location"]
                                                                   success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCProgramInfo *> *arr) {
                                                                       model.responseArr = arr;
                                                                       
                                                                       if (arr.count > 0) {
                                                                           commonProgramID  = arr[0].programID;
                                                                           commonAssertID   = arr[0].assertID;
                                                                           commonProviderID = arr[0].providerID;
                                                                       }
                                                                       
                                                                       [self successWithSession:sessionTask indexPath:indexPath];
                                                                       
                                                                       // 1.14
                                                                       if (isPerforming_LikeOrNot)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self likeOrNot];
                                                                           });
                                                                       }
                                                                       // 1.18
                                                                       if (isPerforming_LikeOrNotV1)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self likeOrNotV1];
                                                                           });
                                                                       }
                                                                       // 2.12
                                                                       if (isPerforming_ProgramInfoItem)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self programInfoItem];
                                                                           });
                                                                       }
                                                                       // 2.13
                                                                       if (isPerforming_ProgramInfo)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self programInfo];
                                                                           });
                                                                       }
                                                                       // 2.18
                                                                       if (isPerforming_UpdateHistory)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self updateHistory];
                                                                           });
                                                                       }
                                                                       // 2.19
                                                                       if (isPerforming_DeleteHistory)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self deleteHistory];
                                                                           });
                                                                       }
                                                                       // 2.21
                                                                       if (isPerforming_UpdateVodFavorite)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self updateVodFavorite];
                                                                           });
                                                                       }
                                                                       // 2.22
                                                                       if (isPerforming_DeleteVodFavorite)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self deleteVodFavorite];
                                                                           });
                                                                       }
                                                                       // 2.24
                                                                       if (isPerforming_ProgramStatus)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self programStatus];
                                                                           });
                                                                       }
                                                                       // 2.25
                                                                       if (isPerforming_Danmaku)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self danmaku];
                                                                           });
                                                                       }
                                                                       // 2.26
                                                                       if (isPerforming_SendDanmaku)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self sendDanmaku];
                                                                           });
                                                                       }
                                                                       // 2.27
                                                                       if (isPerforming_ProgramPrevue)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self programPrevue];
                                                                           });
                                                                       }
                                                                       // 2.29
                                                                       if (isPerforming_UpdateVodPlayHistory)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self updateVodPlayHistory];
                                                                           });
                                                                       }
                                                                       // 2.30
                                                                       if (isPerforming_DeleteVodPlayHistory)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self deleteVodPlayHistory];
                                                                           });
                                                                       }
                                                                       // 2.32
                                                                       if (isPerforming_UpdateVodFavoriteHistory)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self updateVodFavoriteHistory];
                                                                           });
                                                                       }
                                                                       // 2.33
                                                                       if (isPerforming_DeleteVodFavoriteHistory)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self deleteVodFavoriteHistory];
                                                                           });
                                                                       }
                                                                       // 2.35
                                                                       if (isPerforming_VodFavoriteExists)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self vodFavoriteExists];
                                                                           });
                                                                       }
                                                                       // 2.36
                                                                       if (isPerforming_VodPlayHistory)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self vodPlayHistory];
                                                                           });
                                                                       }
                                                                       // 4.8
                                                                       if (isPerforming_Auth)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self auth];
                                                                           });
                                                                       }
                                                                       // 4.9
                                                                       if (isPerforming_AuthV1)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self authV1];
                                                                           });
                                                                       }
                                                                       // 5.3
                                                                       if (isPerforming_RelevantRecommendList)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self relevantRecommendList];
                                                                           });
                                                                       }
                                                                       // 5.4
                                                                       if (isPerforming_ProgramByArtist)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self programByArtist];
                                                                           });
                                                                       }
                                                                       // 5.10
                                                                       if (isPerforming_LookRelevantRecommendList)
                                                                       {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [self lookRelevantRecommendList];
                                                                           });
                                                                       }
                                                                   }
                                                                   failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                       [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                       
                                                                       // 1.14
                                                                       isPerforming_LikeOrNot = false;
                                                                       // 1.18
                                                                       isPerforming_LikeOrNotV1 = false;
                                                                       // 2.12
                                                                       isPerforming_ProgramInfoItem = false;
                                                                       // 2.13
                                                                       isPerforming_ProgramInfo = false;
                                                                       // 2.18
                                                                       isPerforming_UpdateHistory = false;
                                                                       // 2.19
                                                                       isPerforming_DeleteHistory = false;
                                                                       // 2.21
                                                                       isPerforming_UpdateVodFavorite = false;
                                                                       // 2.22
                                                                       isPerforming_DeleteVodFavorite = false;
                                                                       // 2.24
                                                                       isPerforming_ProgramStatus = false;
                                                                       // 2.25
                                                                       isPerforming_Danmaku = false;
                                                                       // 2.26
                                                                       isPerforming_SendDanmaku = false;
                                                                       // 2.27
                                                                       isPerforming_ProgramPrevue = false;
                                                                       // 2.29
                                                                       isPerforming_UpdateVodPlayHistory = false;
                                                                       // 2.30
                                                                       isPerforming_DeleteVodPlayHistory = false;
                                                                       // 2.32
                                                                       isPerforming_UpdateVodFavoriteHistory = false;
                                                                       // 2.33
                                                                       isPerforming_DeleteVodFavoriteHistory = false;
                                                                       // 2.35
                                                                       isPerforming_VodFavoriteExists = false;
                                                                       // 2.36
                                                                       isPerforming_VodPlayHistory = false;
                                                                       // 4.8
                                                                       isPerforming_Auth = false;
                                                                       // 4.9
                                                                       isPerforming_AuthV1 = false;
                                                                       // 5.3
                                                                       isPerforming_RelevantRecommendList = false;
                                                                       // 5.4
                                                                       isPerforming_ProgramByArtist = false;
                                                                       // 5.10
                                                                       isPerforming_LookRelevantRecommendList = false;
                                                                   }];
    NSLog(@"%@", task);
}

// 2.12
static bool isPerforming_ProgramInfoItem;
- (void)programInfoItem
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_ProgramInfoItem)
    {
        isPerforming_ProgramInfoItem = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_ProgramInfoItem = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:programInfoItem];
    InterfaceModel      *model = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if (![commonProgramID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"programID"] = commonProgramID;
    }
    if (![commonAssertID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"assetID"] = commonAssertID;
    }
    if (![commonProviderID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"providerID"] = commonProviderID;
    }
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_ProgramInfoItem_WithProgramID:paramsDic[@"programID"]
                                                                    assetID:paramsDic[@"assetID"]
                                                                 providerID:paramsDic[@"providerID"]
                                                                  episodeID:paramsDic[@"episodeID"]
                                                                    success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCProgramItemInfo *> *arr) {
                                                                        [self successWithSession:sessionTask indexPath:indexPath];
                                                                    }
                                                                    failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                        [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                    }];
    NSLog(@"%@", task);
}

// 2.13
static bool isPerforming_ProgramInfo;
- (void)programInfo
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_ProgramInfo)
    {
        isPerforming_ProgramInfo = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_ProgramInfo = false;

    NSIndexPath         *indexPath = [self prepareForSection:vod Row:programInfo];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if (![commonProgramID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"programID"] = commonProgramID;
    }
    if (![commonAssertID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"assetID"] = commonAssertID;
    }
    if (![commonProviderID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"providerID"] = commonProviderID;
    }
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_ProgramInfo_WithProgramID:paramsDic[@"programID"]
                                                                assetID:paramsDic[@"assetID"]
                                                             providerID:paramsDic[@"providerID"]
                                                                success:^(NSURLSessionDataTask *sessionTask, OMCProgramInfo *programItemInfo) {
                                                                    [self successWithSession:sessionTask indexPath:indexPath];
                                                                }
                                                                failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                    [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                }];
    NSLog(@"%@", task);
}

// 2.14
static bool isPerforming_ProgramCount;
- (void)programCount
{
    if ([commonCategoryID isEqualToString:INIT_DEFAULT] && !isPerforming_ProgramCount)
    {
        isPerforming_ProgramCount = true;
        [self categoryInfoList]; // 依赖2.9
        return;
    }
    
    isPerforming_ProgramCount = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:programCount];
    InterfaceModel      *model = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if (![commonColumnID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"columnID"] = commonColumnID;
    }
    if (![commonCategoryID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"categoryID"] = commonCategoryID;
    }
    if ([paramsDic[@"location"] isEqualToString:@""])
    {
        paramsDic[@"location"] = @"0";
    }
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_ProgramCount_WithColumnID:paramsDic[@"columnID"]
                                                             categoryID:paramsDic[@"categoryID"]
                                                                   year:paramsDic[@"year"]
                                                               location:paramsDic[@"location"]
                                                                success:^(NSURLSessionDataTask *sessionTask, NSString *count) {
                                                                    [self successWithSession:sessionTask indexPath:indexPath];
                                                                }
                                                                failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                    [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                }];
    NSLog(@"%@", task);
}

// 2.15, 暂未开发
- (void)allProgramLabel {
#if 0
    NSIndexPath    *indexPath = [self prepareForSection:vod Row:allProgramLabel];
    InterfaceModel *model = vodArr[indexPath.row];
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_AllProgramLabel_WithSuccess:^(NSURLSessionDataTask *sessionTask, NSArray *arr) {
        model.responseArr = arr;
        if (arr.count > 0)
        {
            
        }
        
        [self successWithSession:sessionTask indexPath:indexPath];
        
        // 2.16
        if (isPerforming_ProgramInfosByLabel)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self programInfosByLabel];
            });
        }

    } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
        [self failureWithSession:sessionTask error:error indexPath:indexPath];
        
        // 2.16
        isPerforming_ProgramInfosByLabel = false;
    }];
    NSLog(@"%@", task);
#endif
}

// 2.16, 暂未开发
static bool isPerforming_ProgramInfosByLabel;
- (void)programInfosByLabel {
#if 0
    if (([commonLabelName isEqualToString:INIT_DEFAULT] || [commonCategoryID isEqualToString:INIT_DEFAULT]) && !isPerforming_ProgramInfosByLabel) {
        isPerforming_ProgramInfosByLabel = true;
        [self categoryInfoList]; // 依赖2.9
        [self allProgramLabel];  // 依赖2.15
        return;
    }
    
    isPerforming_ProgramInfosByLabel = false;
    
    NSIndexPath *indexPath = [self prepareForSection:vod Row:programInfosByLabel];
    InterfaceModel *model = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if (![commonColumnID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"columnID"] = commonColumnID;
    }
    if (![commonCategoryID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"categoryID"] = commonCategoryID;
    }
    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"end"] isEqualToString:@""])
    {
        paramsDic[@"end"] = @"0";
    }

    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_ProgramInfosByLabel_WithColumnID:paramsDic[@"columnID"]
                                                                    categoryID:paramsDic[@"categoryID"]
                                                                         start:[paramsDic[@"start"] integerValue]
                                                                           end:[paramsDic[@"end"] integerValue]
                                                                     labelName:paramsDic[@"labelName"]
                                                                       success:^(NSURLSessionDataTask *sessionTask, NSArray *arr) {
                                                                           [self successWithSession:sessionTask indexPath:indexPath];
                                                                       }
                                                                       failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                           [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                       }];
    NSLog(@"%@", task);
#endif
}

// 2.17
- (void)queryHistory
{
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:queryHistory];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"end"] isEqualToString:@""])
    {
        paramsDic[@"end"] = @"5";
    }
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_QueryHistory_WithLastQueryTime:paramsDic[@"queryTime"]
                                                                       start:[paramsDic[@"start"] integerValue]
                                                                         end:[paramsDic[@"end"] integerValue]
                                                                     success:^(NSURLSessionDataTask *sessionTask, NSArray *arr) {
                                                                         [self successWithSession:sessionTask indexPath:indexPath];
                                                                     }
                                                                     failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                         [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                     }];
    NSLog(@"%@", task);
}

// 2.18
static bool isPerforming_UpdateHistory;
- (void)updateHistory {
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_UpdateHistory)
    {
        isPerforming_UpdateHistory = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_UpdateHistory = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:updateHistory];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    paramsDic[@"programID"] = commonProgramID;
    
    OMCVodHistoryInfo *history = [[OMCVodHistoryInfo alloc] init];
    history.programID       = paramsDic[@"programID"];
    history.episodeID       = paramsDic[@"episodeID"];
    history.compeleted      = paramsDic[@"completed"];
    history.lastPosition    = paramsDic[@"lastPosition"];
    history.localModifyTime = paramsDic[@"localModifyTime"];
    
    NSArray *paramsArr = [NSArray arrayWithObject:history];
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_UpdateHistory_WithVodHistoryInfos:paramsArr
                                                                        success:^(NSURLSessionDataTask *sessionTask, NSArray *arr) {
                                                                            [self successWithSession:sessionTask indexPath:indexPath];
                                                                        }
                                                                        failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                            [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                        }];
    NSLog(@"%@", task);
}

// 2.19
static bool isPerforming_DeleteHistory;
- (void)deleteHistory {
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_DeleteHistory)
    {
        isPerforming_DeleteHistory = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_DeleteHistory = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:deleteHistory];
    InterfaceModel      *model = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    paramsDic[@"programID"] = commonProgramID;
    
    OMCVodHistoryInfo *history = [[OMCVodHistoryInfo alloc] init];
    history.programID       = paramsDic[@"programID"];
    history.localModifyTime = paramsDic[@"localModifyTime"];
    
    NSArray *paramsArr = [NSArray arrayWithObject:history];
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_DeleteHistory_WithVodHistoryInfos:paramsArr
                                                                        success:^(NSURLSessionDataTask *sessionTask, NSArray *arr) {
                                                                            [self successWithSession:sessionTask indexPath:indexPath];
                                                                        }
                                                                        failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                            [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                        }];
    NSLog(@"%@", task);
}

// 2.20
- (void)queryVodFavorite
{
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:queryVodFavorite];
    InterfaceModel      *model = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;

    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"end"] isEqualToString:@""])
    {
        paramsDic[@"end"] = @"5";
    }
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_QueryVodFavorite_WithLastQueryTime:paramsDic[@"lastQueryTime"]
                                                                           start:[paramsDic[@"end"] integerValue]
                                                                             end:[paramsDic[@"end"] integerValue]
                                                                         success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCVodFavoriteInfo *> *arr) {
                                                                             [self successWithSession:sessionTask indexPath:indexPath];
                                                                         }
                                                                         failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                             [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                         }];
    NSLog(@"%@", task);
}

// 2.21
static bool isPerforming_UpdateVodFavorite;
- (void)updateVodFavorite
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_UpdateVodFavorite)
    {
        isPerforming_UpdateVodFavorite = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_UpdateVodFavorite = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:updateVodFavorite];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    paramsDic[@"programID"] = commonProgramID;
    paramsDic[@"columnID"]  = commonColumnID;
    
    OMCVodFavoriteInfo *favorite = [[OMCVodFavoriteInfo alloc] init];
    favorite.programID       = paramsDic[@"programID"];
    favorite.columnID        = paramsDic[@"columnID"];
    favorite.columnName      = paramsDic[@"columnName"];
    favorite.localModifyTime = paramsDic[@"localModifyTime"];
    
    NSArray *paramsArr = [NSArray arrayWithObject:favorite];
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_UpdateVodFavorite_WithVodFavoriteInfos:paramsArr
                                                                             success:^(NSURLSessionDataTask *sessionTask, NSArray *arr) {
                                                                                 [self successWithSession:sessionTask indexPath:indexPath];
                                                                             }
                                                                             failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                                 [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                             }];
    NSLog(@"%@", task);
}

// 2.22
static bool isPerforming_DeleteVodFavorite;
- (void)deleteVodFavorite
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_DeleteVodFavorite)
    {
        isPerforming_DeleteVodFavorite = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_DeleteVodFavorite = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:deleteVodFavorite];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    paramsDic[@"programID"] = commonProgramID;
    paramsDic[@"columnID"]  = commonColumnID;
    
    OMCVodFavoriteInfo *favorite = [[OMCVodFavoriteInfo alloc] init];
    favorite.programID       = paramsDic[@"programID"];
    favorite.columnID        = paramsDic[@"columnID"];
    favorite.localModifyTime = paramsDic[@"localModifyTime"];
    
    NSArray *paramsArr = [NSArray arrayWithObject:favorite];
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_DeleteVodFavorite_WithVodFavoriteInfos:paramsArr
                                                                             success:^(NSURLSessionDataTask *sessionTask, NSArray *arr) {
                                                                                 [self successWithSession:sessionTask indexPath:indexPath];
                                                                             }
                                                                             failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                                 [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                             }];
    NSLog(@"%@", task);
}

// 暂未开发
// 2.23
- (void)vodVoiceKeyWords
{
#if 0
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:vodVoiceKeyWords];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_VodVoiceKeyWords_WithVersions:paramsDic[@"version"]
                                                                    success:^(NSURLSessionDataTask *sessionTask, NSDictionary *dic) {
                                                                        [self successWithSession:sessionTask indexPath:indexPath];
                                                                    }
                                                                    failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                        [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                    }];
    NSLog(@"%@", task);
#endif
}

// 2.24
bool isPerforming_ProgramStatus;
- (void)programStatus
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_ProgramStatus)
    {
        isPerforming_ProgramStatus = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_ProgramStatus = false;

    NSIndexPath         *indexPath = [self prepareForSection:vod Row:programStatus];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    paramsDic[@"programID"] = commonProgramID;
    paramsDic[@"columnID"]  = commonColumnID;
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_ProgramStatus_WithProgramID:paramsDic[@"programID"]
                                                                 columnID:paramsDic[@"columnID"]
                                                                  success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCProgramStatus *> *arr)
                                                                  {
                                                                      [self successWithSession:sessionTask indexPath:indexPath];
                                                                  }
                                                                  failure:^(NSURLSessionDataTask *sessionTask, NSError *error)
                                                                  {
                                                                      [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                  }];
    NSLog(@"%@", task);
}

// 2.25
bool isPerforming_Danmaku;
- (void)danmaku
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_Danmaku)
    {
        isPerforming_Danmaku = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_Danmaku = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:danmaku];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    paramsDic[@"assetID"]    = commonAssertID;
    paramsDic[@"providerId"] = commonProviderID;
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_Danmaku_WithAssetID:paramsDic[@"assetID"]
                                                       providerId:paramsDic[@"providerId"]
                                                        episodeId:paramsDic[@"episodeId"]
                                                        startTime:[paramsDic[@"startTime"] doubleValue]
                                                          endTime:[paramsDic[@"endTime"] doubleValue]
                                                          success:^(NSURLSessionDataTask *sessionTask, OMCDanmakuModel *danmakuModel) {
                                                              [self successWithSession:sessionTask indexPath:indexPath];
                                                          }
                                                          failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                              [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                          }];
    NSLog(@"%@", task);
}

// 2.26
bool isPerforming_SendDanmaku;
- (void)sendDanmaku
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_SendDanmaku)
    {
        isPerforming_SendDanmaku = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_SendDanmaku = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:sendDanmaku];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    paramsDic[@"assetID"]    = commonAssertID;
    paramsDic[@"providerId"] = commonProviderID;
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_SendDanmaku_WithAssetId:paramsDic[@"assetID"]
                                                           providerId:paramsDic[@"providerId"]
                                                            episodeId:paramsDic[@"episodeID"]
                                                                  pos:paramsDic[@"pos"]
                                                                 size:paramsDic[@"size"]
                                                                color:paramsDic[@"color"]
                                                                speed:paramsDic[@"speed"]
                                                              content:paramsDic[@"content"]
                                                                 type:paramsDic[@"type"]
                                                             playTime:paramsDic[@"playTime"]
                                                              success:^(NSURLSessionDataTask *sessionTask, BOOL result) {
                                                                  [self successWithSession:sessionTask indexPath:indexPath];
                                                              }
                                                              failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                  [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                              }];
    NSLog(@"%@", task);
}

// 2.27
static bool isPerforming_ProgramPrevue;
- (void)programPrevue
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_ProgramPrevue)
    {
        isPerforming_ProgramPrevue = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_ProgramPrevue = false;
    
    NSIndexPath         *indexPath  = [self prepareForSection:vod Row:programPrevue];
    InterfaceModel      *model      = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic  = model.paramsDic;
    
    paramsDic[@"programID"]  = commonProgramID;
    paramsDic[@"assetID"]    = commonAssertID;
    paramsDic[@"providerId"] = commonProviderID;
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_ProgramPrevue_WithProgramID:paramsDic[@"programID"]
                                                                  assetID:paramsDic[@"assetID"]
                                                               providerID:paramsDic[@"providerId"]
                                                                episodeID:paramsDic[@"episodeID"]
                                                                  success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCProgramItemInfo *> *prevuesArr, NSArray<OMCProgramItemInfo *> *videoClipsArr) {
                                                                      [self successWithSession:sessionTask indexPath:indexPath];
                                                                  }
                                                                  failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                      [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                  }];
    NSLog(@"%@", task);
}

// 2.28
- (void)queryVodPlayHistory
{
    NSIndexPath           *indexPath  = [self prepareForSection:vod Row:queryVodPlayHistory];
    InterfaceModel        *model      = vodArr[indexPath.row];
    NSMutableDictionary   *paramsDic  = model.paramsDic;
    
    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"end"] isEqualToString:@""])
    {
        paramsDic[@"end"] = @"5";
    }
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_QueryVodPlayHistory_WithStart:[paramsDic[@"start"] integerValue]
                                                                        end:[paramsDic[@"end"] integerValue]
                                                                    success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCCloudVodHistoryInfo *> *arr) {
                                                                        [self successWithSession:sessionTask indexPath:indexPath];
                                                                    }
                                                                    failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                        [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                    }];
    NSLog(@"%@", task);
}

// 2.29
static bool isPerforming_UpdateVodPlayHistory;
- (void)updateVodPlayHistory {
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_UpdateVodPlayHistory)
    {
        isPerforming_UpdateVodPlayHistory = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_UpdateVodPlayHistory = false;
    
    NSIndexPath           *indexPath  = [self prepareForSection:vod Row:updateVodPlayHistory];
    InterfaceModel        *model      = vodArr[indexPath.row];
    NSMutableDictionary   *paramsDic  = model.paramsDic;
    
    paramsDic[@"programID"]  = commonProgramID;
    paramsDic[@"assertID"]   = commonAssertID;
    paramsDic[@"providerID"] = commonProviderID;
    
    OMCVodHistoryInfo *history = [[OMCVodHistoryInfo alloc] init];
    history.programID    = paramsDic[@"programID"];
    history.assertID     = paramsDic[@"assertID"];
    history.providerID   = paramsDic[@"providerID"];
    history.compeleted   = paramsDic[@"completed"];
    history.lastPosition = paramsDic[@"lastPosition"];
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_UpdateVodPlayHistory_WithVodHistoryInfo:history
                                                                              success:^(NSURLSessionDataTask *sessionTask, BOOL result) {
                                                                                  [self successWithSession:sessionTask indexPath:indexPath];
                                                                              }
                                                                              failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                                  [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                              }];
    NSLog(@"%@", task);
}

// 2.30
static bool isPerforming_DeleteVodPlayHistory;
- (void)deleteVodPlayHistory
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_DeleteVodPlayHistory)
    {
        isPerforming_DeleteVodPlayHistory = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_DeleteVodPlayHistory = false;
    
    NSIndexPath           *indexPath  = [self prepareForSection:vod Row:deleteVodPlayHistory];
    InterfaceModel        *model      = vodArr[indexPath.row];
    NSMutableDictionary   *paramsDic  = model.paramsDic;
    
    paramsDic[@"assertID"]   = commonAssertID;
    paramsDic[@"providerID"] = commonProviderID;
    
    OMCVodHistoryInfo *history = [[OMCVodHistoryInfo alloc] init];
    history.assertID   = paramsDic[@"assertID"];
    history.providerID = paramsDic[@"providerID"];
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_DeleteVodPlayHistory_WithVodHistoryInfo:history
                                                                              success:^(NSURLSessionDataTask *sessionTask, BOOL result) {
                                                                                  [self successWithSession:sessionTask indexPath:indexPath];
                                                                              }
                                                                              failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                                  [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                              }];
    NSLog(@"%@", task);
}

// 2.31
- (void)queryVodFavoriteHistory
{
    NSIndexPath         *indexPath  = [self prepareForSection:vod Row:queryVodFavoriteHistory];
    InterfaceModel      *model      = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic  = model.paramsDic;

    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"end"] isEqualToString:@""])
    {
        paramsDic[@"end"] = @"5";
    }
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_QueryVodFavoriteHistory_WithStart:[paramsDic[@"start"] integerValue]
                                                                            end:[paramsDic[@"end"] integerValue]
                                                                        success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCCloudVodFavoriteInfo *> *arr) {
                                                                            [self successWithSession:sessionTask indexPath:indexPath];
                                                                        }
                                                                        failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                            [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                        }];
    NSLog(@"%@", task);
}

// 2.32
static bool isPerforming_UpdateVodFavoriteHistory;
- (void)updateVodFavoriteHistory
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_UpdateVodFavoriteHistory)
    {
        isPerforming_UpdateVodFavoriteHistory = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_UpdateVodFavoriteHistory = false;
    
    NSIndexPath         *indexPath  = [self prepareForSection:vod Row:updateVodFavoriteHistory];
    InterfaceModel      *model      = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic  = model.paramsDic;
    
    paramsDic[@"programID"] = commonProgramID;
    paramsDic[@"columnID"]  = commonColumnID;
    
    OMCVodFavoriteInfo *favorite = [[OMCVodFavoriteInfo alloc] init];
    favorite.programID  = paramsDic[@"programID"];
    favorite.columnID   = paramsDic[@"columnID"];
    favorite.columnName = paramsDic[@"columnName"];

    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_UpdateVodFavoriteHistory_WithVodFavoriteInfo:favorite
                                                                                   success:^(NSURLSessionDataTask *sessionTask, BOOL result) {
                                                                                       [self successWithSession:sessionTask indexPath:indexPath];
                                                                                   }
                                                                                   failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                                       [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                                   }];
    NSLog(@"%@", task);
}

// 2.33
static bool isPerforming_DeleteVodFavoriteHistory;
- (void)deleteVodFavoriteHistory
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_DeleteVodFavoriteHistory)
    {
        isPerforming_DeleteVodFavoriteHistory = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_DeleteVodFavoriteHistory = false;
    
    NSIndexPath         *indexPath  = [self prepareForSection:vod Row:deleteVodFavoriteHistory];
    InterfaceModel      *model      = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic  = model.paramsDic;
    
    paramsDic[@"programID"] = commonProgramID;
    paramsDic[@"columnID"]  = commonColumnID;
    
    OMCVodFavoriteInfo *favorite = [[OMCVodFavoriteInfo alloc] init];
    favorite.programID = paramsDic[@"programID"];
    favorite.columnID  = paramsDic[@"columnID"];
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_DeleteVodFavoriteHistory_WithVodFavoriteInfo:favorite
                                                                                   success:^(NSURLSessionDataTask *sessionTask, BOOL result) {
                                                                                       [self successWithSession:sessionTask indexPath:indexPath];
                                                                                   }
                                                                                   failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                                       [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                                   }];
    NSLog(@"%@", task);
}

// 2.34
bool isPerforming_VodSearchByProgramName;
- (void)vodSearchByProgramName
{
    if ([commonSubID isEqualToString:INIT_DEFAULT] && !isPerforming_VodSearchByProgramName)
    {
        isPerforming_VodSearchByProgramName = true;
        [self vodSearch]; // 依赖2.2
        return;
    }
    
    isPerforming_VodSearchByProgramName = false;
    
    NSIndexPath         *indexPath  = [self prepareForSection:vod Row:vodSearchByProgramName];
    InterfaceModel      *model      = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic  = model.paramsDic;
    
    paramsDic[@"subID"] = commonSubID;
    
    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"end"] isEqualToString:@""])
    {
        paramsDic[@"end"]   = @"5";
    }
    if ([paramsDic[@"type"] isEqualToString:@""])
    {
        paramsDic[@"type"]  = @"0";
    }
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_VodSearchByProgramName_WithKeyword:paramsDic[@"keyword"]
                                                                           start:[paramsDic[@"start"] integerValue]
                                                                             end:[paramsDic[@"end"] integerValue]
                                                                           subID:paramsDic[@"subID"]
                                                                      columnSize:paramsDic[@"columnSize"]
                                                                            type:[paramsDic[@"type"] integerValue]
                                                                         success:^(NSURLSessionDataTask *sessionTask, NSString *columnSize, NSString *count, NSArray<OMCProgramInfo *> *programsArr, NSArray<OMCSearchCategory *> *categoryArr) {
                                                                             [self successWithSession:sessionTask indexPath:indexPath];
                                                                         }
                                                                         failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                             [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                         }];
    NSLog(@"%@", task);
}

// 2.35
bool isPerforming_VodFavoriteExists;
- (void)vodFavoriteExists
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_VodFavoriteExists)
    {
        isPerforming_VodFavoriteExists = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_VodFavoriteExists = false;
    
    NSIndexPath         *indexPath  = [self prepareForSection:vod Row:vodFavoriteExists];
    InterfaceModel      *model      = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic  = model.paramsDic;
    
    paramsDic[@"programID"] = commonProgramID;

    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_VodFavoriteExists_WithProgramID:paramsDic[@"programID"]
                                                                      success:^(NSURLSessionDataTask *sessionTask, BOOL result) {
                                                                          [self successWithSession:sessionTask indexPath:indexPath];
                                                                      }
                                                                      failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                          [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                      }];
    NSLog(@"%@", task);
}

// 2.36
bool isPerforming_VodPlayHistory;
- (void)vodPlayHistory
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_VodPlayHistory)
    {
        isPerforming_VodPlayHistory = true;
        [self programInfoList]; // 依赖2.11
        return;
    }
    
    isPerforming_VodPlayHistory = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:vod Row:vodPlayHistory];
    InterfaceModel      *model     = vodArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    paramsDic[@"programID"] = commonProgramID;
    
    NSURLSessionDataTask *task =
    [[OMCVodApiManager sharedManager] request_VodPlayHistory_WithProgramID:paramsDic[@"programID"]
                                                                   success:^(NSURLSessionDataTask *sessionTask, NSArray *arr) {
                                                                       [self successWithSession:sessionTask indexPath:indexPath];
                                                                   }
                                                                   failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                       [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                   }];
    NSLog(@"%@", task);
}

#pragma mark -- 直播接口

// 3.3
- (void)liveCategoryList
{
    NSIndexPath *indexPath = [self prepareForSection:live Row:liveCategoryList];
    InterfaceModel *model  = liveArr[indexPath.row];
    
    NSURLSessionTask *task =
    [[OMCLiveApiManager sharedManager] request_LiveCategoryList_WithSuccess:^(NSURLSessionDataTask *task, NSArray<OMCLiveCategory *> *arrayData) {
        model.responseArr = arrayData;
        if (arrayData.count > 0)
        {
            commonLiveCategoryID = arrayData[0].categoryID;
        }
        
        [self successWithSession:task indexPath:indexPath];
        
        // 3.5
        if (isPerforming_ChannelList)
        {
            [self channelList];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self failureWithSession:task error:error indexPath:indexPath];
        
        // 3.5
        isPerforming_ChannelList = false;
    }];
    NSLog(@"%@", task);
}

// 3.5
bool isPerforming_ChannelList;
- (void)channelList
{
    if ([commonLiveCategoryID isEqualToString:INIT_DEFAULT] && !isPerforming_ChannelList)
    {
        isPerforming_ChannelList = true;
        [self liveCategoryList]; // 依赖3.3
        return;
    }
    
    isPerforming_ChannelList = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:live Row:channelList];
    InterfaceModel      *model     = liveArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if (![commonLiveCategoryID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"categoryID"] = commonLiveCategoryID;
    }
    
    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"end"] isEqualToString:@""])
    {
        paramsDic[@"end"] = @"5";
    }
    if ([paramsDic[@"sortType"] isEqualToString:@""])
    {
        paramsDic[@"sortType"] = @"2";
    }
    
    NSURLSessionTask *task =
    [[OMCLiveApiManager sharedManager] request_ChannelList_WithCategoryId:paramsDic[@"categoryID"]
                                                                    start:[paramsDic[@"start"] integerValue]
                                                                      end:[paramsDic[@"end"] integerValue]
                                                              channelName:paramsDic[@"channelName"]
                                                              programName:paramsDic[@"programName"]
                                                                 sortType:[paramsDic[@"sortType"] integerValue]
                                                                  success:^(NSURLSessionDataTask *task, NSArray<OMCChannelInfo *> *arrayData) {
                                                                      model.responseArr = arrayData;
                                                                      if (arrayData.count > 0)
                                                                      {
                                                                          commonChannelID = arrayData[0].channelID;
                                                                          commonChannelName = arrayData[0].channelName;
                                                                      }
                                                                      
                                                                      [self successWithSession:task indexPath:indexPath];
                                                                      
                                                                      // 3.7
                                                                      if (isPerforming_CurrentEPG)
                                                                      {
                                                                          [self currentEPG];
                                                                      }
                                                                      // 3.8
                                                                      if (isPerforming_EPGInfoList)
                                                                      {
                                                                          [self EPGInfoList];
                                                                      }
                                                                      // 3.19
                                                                      if (isPerforming_ChannelStatus)
                                                                      {
                                                                          [self channelStatus];
                                                                      }
                                                                  }
                                                                  failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                                      [self failureWithSession:task error:error indexPath:indexPath];
                                                                      
                                                                      // 3.7
                                                                      isPerforming_CurrentEPG = false;
                                                                      // 3.8
                                                                      isPerforming_EPGInfoList = false;
                                                                      //3.19
                                                                      isPerforming_ChannelStatus = false;
                                                                  }];
    NSLog(@"%@", task);
}

// 3.7
bool isPerforming_CurrentEPG;
- (void)currentEPG
{
    if ([commonChannelID isEqualToString:INIT_DEFAULT] && !isPerforming_CurrentEPG)
    {
        isPerforming_CurrentEPG = true;
        [self channelList]; // 依赖3.5
        return;
    }
    
    isPerforming_CurrentEPG = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:live Row:currentEPG];
    InterfaceModel      *model     = liveArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    paramsDic[@"channelID"]  = commonChannelID;
    paramsDic[@"categoryID"] = commonLiveCategoryID;
    
    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"end"] isEqualToString:@""])
    {
        paramsDic[@"end"] = @"5";
    }
    if ([paramsDic[@"sortType"] isEqualToString:@""])
    {
        paramsDic[@"sortType"] = @"0";
    }

    NSURLSessionTask *task =
    [[OMCLiveApiManager sharedManager] request_CurrentEPG_WithStart:[paramsDic[@"start"] integerValue]
                                                                end:[paramsDic[@"end"] integerValue]
                                                           sortType:[paramsDic[@"sortType"] integerValue]
                                                         categoryID:paramsDic[@"categoryID"]
                                                          channelID:paramsDic[@"channelID"]
                                                         resolution:paramsDic[@"resolution"]
                                                            success:^(NSURLSessionDataTask *task, NSArray<OMCCurrentEPG *> *arrayData) {
                                                                [self successWithSession:task indexPath:indexPath];
                                                            }
                                                            failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                                [self failureWithSession:task error:error indexPath:indexPath];
                                                            }];
    NSLog(@"%@", task);
}

// 3.8
bool isPerforming_EPGInfoList;
- (void)EPGInfoList
{
    if ([commonChannelID isEqualToString:INIT_DEFAULT] && !isPerforming_EPGInfoList)
    {
        isPerforming_EPGInfoList = true;
        [self channelList]; // 依赖3.5
        return;
    }
    
    isPerforming_EPGInfoList = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:live Row:EPGInfoList];
    InterfaceModel      *model     = liveArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    paramsDic[@"channelID"] = commonChannelID;
    
    NSURLSessionTask *task =
    [[OMCLiveApiManager sharedManager] request_EPGInfoList_WithChanneID:paramsDic[@"channelID"]
                                                              startTime:paramsDic[@"startTime"]
                                                                endTime:paramsDic[@"endTime"]
                                                                success:^(NSURLSessionDataTask *task, NSArray<OMCChannelEPG *> *arrayData) {
                                                                    model.responseArr = arrayData;
                                                                    if (arrayData.count > 0)
                                                                    {
                                                                        commonepgID = arrayData[0].epgID;
                                                                        commonepgName = arrayData[0].epgName;
                                                                    }
                                                                    [self successWithSession:task indexPath:indexPath];
                                                                    
                                                                    // 5.5
                                                                    if (isPerforming_ProgramByEpg)
                                                                    {
                                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                                            [self programByEpg];
                                                                        });
                                                                    }
                                                                    // 5.6
                                                                    if (isPerforming_LiveSeriesInfoList)
                                                                    {
                                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                                            [self liveSeriesInfoList];
                                                                        });
                                                                    }
                                                                    // 5.7
                                                                    if (isPerforming_liveEpgSeriesDesInfo)
                                                                    {
                                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                                            [self liveEpgSeriesDesInfo];
                                                                        });
                                                                    }
                                                                }
                                                                failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                                    [self failureWithSession:task error:error indexPath:indexPath];
                                                                    
                                                                    // 5.5
                                                                    isPerforming_ProgramByEpg = false;
                                                                    // 5.6
                                                                    isPerforming_LiveSeriesInfoList = false;
                                                                    // 5.7
                                                                    isPerforming_liveEpgSeriesDesInfo = false;
                                                                }];
    NSLog(@"%@", task);
}

// 3.19
bool isPerforming_ChannelStatus;
- (void)channelStatus
{
    if ([commonChannelID isEqualToString:INIT_DEFAULT] && !isPerforming_ChannelStatus)
    {
        isPerforming_ChannelStatus = true;
        [self channelList]; // 依赖3.5
        return;
    }
    
    isPerforming_ChannelStatus = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:live Row:channelStatus];
    InterfaceModel      *model     = liveArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    paramsDic[@"channelID"] = commonChannelID;
    
    NSURLSessionTask *task =
    [[OMCLiveApiManager sharedManager] request_ChannelStatus_WithArrayChannelID:@[paramsDic[@"channelID"]] success:^(NSURLSessionDataTask *task, NSArray<OMCChannelStatus *> *arrayData) {
        [self successWithSession:task indexPath:indexPath];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self failureWithSession:task error:error indexPath:indexPath];
    }];
    NSLog(@"%@", task);
}

// 3.20
- (void)channelRecommend
{
    NSIndexPath *indexPath  = [self prepareForSection:live Row:channelRecommend];
    
    NSURLSessionTask *task =
    [[OMCLiveApiManager sharedManager] request_ChannelRecommend_WithSuccess:^(NSURLSessionDataTask *task, NSArray<OMCWelcomeChannelInfo *> *arrayData) {
        [self successWithSession:task indexPath:indexPath];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self failureWithSession:task error:error indexPath:indexPath];
    }];
    NSLog(@"%@", task);
}

#pragma mark - 用户接口

// 4.1
- (void)login
{
    NSIndexPath         *indexPath = [self prepareForSection:user Row:login];
    InterfaceModel      *model     = userArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;

    NSURLSessionDataTask *task =
    [[OMCUserApiManager sharedManager] request_Login_WithUser:paramsDic[@"user"]
                                                     password:paramsDic[@"password"]
                                                    protected:paramsDic[@"protected"]
                                                      drmInfo:paramsDic[@"drmInfo"]
                                                      success:^(NSURLSessionDataTask *sessionTask, NSString *AAAToken, NSString *DRMToken) {
                                                          [self successWithSession:task indexPath:indexPath];
                                                      }
                                                      failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                          [self failureWithSession:task error:error indexPath:indexPath];
                                                      }];
    NSLog(@"%@", task);
}

// 4.2
- (void)regist
{
    NSIndexPath         *indexPath = [self prepareForSection:user Row:regist];
    InterfaceModel      *model     = userArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    NSURLSessionDataTask *task =
    [[OMCUserApiManager sharedManager] request_Regist_WithUser:paramsDic[@"user"]
                                                   phoneNumber:paramsDic[@"phoneNumber"]
                                                         email:paramsDic[@"email"]
                                                      password:paramsDic[@"password"]
                                                     protected:paramsDic[@"protected"]
                                                    cardNumber:paramsDic[@"cardNumber"]
                                                  cardPassword:paramsDic[@"cardPassword"]
                                                     accountID:paramsDic[@"accountID"]
                                                         token:paramsDic[@"token"]
                                                       success:^(NSURLSessionDataTask *sessionTask, BOOL result) {
                                                           [self successWithSession:sessionTask indexPath:indexPath];
                                                       }
                                                       failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                           [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                       }];
    NSLog(@"%@", task);
}

// 4.3
- (void)logout
{
    NSIndexPath *indexPath  = [self prepareForSection:user Row:logout];
    
    NSURLSessionDataTask *task =
    [[OMCUserApiManager sharedManager] request_Logout_WithSuccess:^(NSURLSessionDataTask *sessionTask, BOOL result) {
        [self successWithSession:sessionTask indexPath:indexPath];
    } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
        [self failureWithSession:sessionTask error:error indexPath:indexPath];
    }];
    NSLog(@"%@", task);
}

// 4.4
- (void)modifyPassword
{
    NSIndexPath         *indexPath = [self prepareForSection:user Row:modifyPassword];
    InterfaceModel      *model     = userArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;

    NSURLSessionDataTask *task =
    [[OMCUserApiManager sharedManager] request_ModifyPassword_WithUser:paramsDic[@"user"]
                                                           oldPassword:paramsDic[@"oldPassword"]
                                                           newPassword:paramsDic[@"newPassword"]
                                                           phoneNumber:paramsDic[@"phoneNumber"]
                                                               success:^(NSURLSessionDataTask *sessionTask, BOOL result) {
                                                                   [self successWithSession:sessionTask indexPath:indexPath];
                                                               }
                                                               failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                   [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                               }];
    NSLog(@"%@", task);
}

// 4.5
- (void)getToken
{
    NSIndexPath         *indexPath = [self prepareForSection:user Row:getToken];
    InterfaceModel      *model     = userArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if ([paramsDic[@"type"] isEqualToString:@""])
    {
        paramsDic[@"type"] = @"1";
    }
    
    NSURLSessionDataTask *task =
    [[OMCUserApiManager sharedManager] request_GetToken_WithPhoneNumber:paramsDic[@"phoneNumber"]
                                                                   type:[paramsDic[@"type"] integerValue]
                                                                success:^(NSURLSessionDataTask *sessionTask, NSString *timeout) {
                                                                    [self successWithSession:sessionTask indexPath:indexPath];
                                                                }
                                                                failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                    [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                }];
    NSLog(@"%@", task);
}

// 4.6
- (void)checkToken
{
    NSIndexPath         *indexPath = [self prepareForSection:user Row:checkToken];
    InterfaceModel      *model     = userArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if ([paramsDic[@"type"] isEqualToString:@""])
    {
        paramsDic[@"type"] = @"1";
    }
    
    NSURLSessionDataTask *task =
    [[OMCUserApiManager sharedManager] request_CheckToken_WithPhoneNumber:paramsDic[@"phoneNumber"]
                                                                    token:paramsDic[@"token"]
                                                                     type:[paramsDic[@"type"] integerValue]
                                                                  success:^(NSURLSessionDataTask *sessionTask, BOOL result) {
                                                                      [self successWithSession:sessionTask indexPath:indexPath];
                                                                  } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                      [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                  }];
    NSLog(@"%@", task);
}

// 4.7
- (void)resetPassword
{
    NSIndexPath         *indexPath = [self prepareForSection:user Row:resetPassword];
    InterfaceModel      *model     = userArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    NSURLSessionDataTask *task =
    [[OMCUserApiManager sharedManager] request_ResetPassword_WithPhoneNumber:paramsDic[@"phoneNumber"]
                                                                       token:paramsDic[@"token"]
                                                                   protected:paramsDic[@"protected"]
                                                                 newPassword:paramsDic[@"newPassword"]
                                                                     success:^(NSURLSessionDataTask *sessionTask, BOOL result) {
                                                                         [self successWithSession:sessionTask indexPath:indexPath];
                                                                     }
                                                                     failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                         [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                     }];
    NSLog(@"%@", task);
}

// 4.8
bool isPerforming_Auth;
- (void)auth
{
    // 默认使用点播鉴权
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_Auth)
    {
        isPerforming_Auth = true;
        [self programInfoList]; // 2.11
        return;
    }
    
    isPerforming_Auth = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:user Row:auth];
    InterfaceModel      *model     = userArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if (![commonProgramID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"programID"] = commonProgramID;
    }
    if (![commonChannelID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"channelID"] = commonChannelID;
    }
    if (![commonepgID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"epgID"] = commonepgID;
    }
    if ([paramsDic[@"pType"] isEqualToString:@""])
    {
        paramsDic[@"pType"] = @"0";
    }
    
    NSURLSessionDataTask *task =
    [[OMCUserApiManager sharedManager] request_Auth_WithprogramID:paramsDic[@"programID"]
                                                         DRMtoken:paramsDic[@"DRMtoken"]
                                                        channelID:paramsDic[@"channelID"]
                                                            epgID:paramsDic[@"epgID"]
                                                         authType:paramsDic[@"authType"]
                                                     secondAuthid:paramsDic[@"secondAuthid"]
                                                          success:^(NSURLSessionDataTask *sessionTask, NSString *authResult) {
                                                              [self successWithSession:sessionTask indexPath:indexPath];
                                                          }
                                                          failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                              [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                          }];
    NSLog(@"%@", task);
}

// 4.9
bool isPerforming_AuthV1;
- (void)authV1
{
    // 默认使用点播鉴权
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_AuthV1)
    {
        isPerforming_AuthV1 = true;
        [self programInfoList]; // 2.11
        return;
    }
    
    isPerforming_AuthV1 = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:user Row:authV1];
    InterfaceModel      *model     = userArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if (![commonProgramID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"pid"] = commonProgramID;
    }
    if (![commonChannelID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"cid"] = commonChannelID;
    }
    if (![commonepgID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"epgID"] = commonepgID;
    }
    if (![commonLocationID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"locationID"] = commonLocationID;
    }
    if ([paramsDic[@"authType"] isEqualToString:@""])
    {
        paramsDic[@"authType"] = @"0";
    }
    if ([paramsDic[@"pType"] isEqualToString:@""])
    {
        paramsDic[@"pType"] = @"3";
    }
    
    NSURLSessionDataTask *task =
    [[OMCUserApiManager sharedManager] request_Auth_V1_WithDRMtoken:paramsDic[@"DRMtoken"]
                                                              epgID:paramsDic[@"epgID"]
                                                           authType:paramsDic[@"authType"]
                                                       secondAuthid:paramsDic[@"secondAuthid"]
                                                              token:paramsDic[@"token"]
                                                                pid:paramsDic[@"pid"]
                                                                cid:paramsDic[@"cid"]
                                                             userID:paramsDic[@"userID"]
                                                       platformType:[paramsDic[@"pType"] integerValue]
                                                         locationID:paramsDic[@"locationID"]
                                                           deviceID:paramsDic[@"deviceID"]
                                                               name:paramsDic[@"name"]
                                                            version:paramsDic[@"version"]
                                                            success:^(NSURLSessionDataTask *sessionTask, NSString *authResult) {
                                                                [self successWithSession:sessionTask indexPath:indexPath];
                                                            }
                                                            failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                            }];
    NSLog(@"%@", task);
}

#pragma - mark UBA接口

// 5.1
- (void)recommendList
{
    NSIndexPath         *indexPath = [self prepareForSection:uba Row:recommendList];
    InterfaceModel      *model     = ubaArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;

    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"numberOfResults"] isEqualToString:@""])
    {
        paramsDic[@"numberOfResults"] = @"5";
    }
    if ([paramsDic[@"type"] isEqualToString:@""])
    {
        paramsDic[@"type"] = @"0";
    }
    
    [[OMCUBAApiManager sharedManager] request_RecommendList_WithStart:[paramsDic[@"start"] integerValue]
                                                      numberOfResults:[paramsDic[@"numberOfResults"] integerValue]
                                                      recommendTypeID:paramsDic[@"recommendTypeID"]
                                                                 type:[paramsDic[@"type"] integerValue]
                                                           resolution:paramsDic[@"resolution"]
                                                              success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCRecommendItem *> *arr) {
                                                                  [self successWithSession:sessionTask indexPath:indexPath];
                                                              }
                                                              failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                  [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                              }];
    
}

// 5.2
bool isPerforming_VodRankList;
- (void)vodRankList
{
    if ([commonColumnID isEqualToString:INIT_DEFAULT] && !isPerforming_VodRankList)
    {
        isPerforming_VodRankList = true;
        [self columnInfoList]; //2.8
        return;
    }
    
    isPerforming_VodRankList = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:uba Row:vodRankList];
    InterfaceModel      *model     = ubaArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if (![commonColumnID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"columnID"] = commonColumnID;
    }
    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"numberOfResults"] isEqualToString:@""])
    {
        paramsDic[@"numberOfResults"] = @"5";
    }
    
    [[OMCUBAApiManager sharedManager] request_VodRankList_WithStart:[paramsDic[@"start"] integerValue]
                                                    numberOfResults:[paramsDic[@"numberOfResults"] integerValue]
                                                           columnID:paramsDic[@"columnID"]
                                                    secondaryTypeID:paramsDic[@"secondaryTypeID"]
                                                           sortType:paramsDic[@"sortType"]
                                                           location:paramsDic[@"location"]
                                                          timeRange:paramsDic[@"timeRange"]
                                                            success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCVodRecommendItem *> *arr) {
                                                                [self successWithSession:sessionTask indexPath:indexPath];
                                                            }
                                                            failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                            }];
}

// 5.3
bool isPerforming_RelevantRecommendList;
- (void)relevantRecommendList
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_RelevantRecommendList)
    {
        isPerforming_RelevantRecommendList = true;
        [self programInfoList]; // 2.11
        return;
    }
    
    isPerforming_RelevantRecommendList = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:uba Row:relevantRecommendList];
    InterfaceModel      *model     = ubaArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;

    if (![commonProgramID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"programID"] = commonProgramID;
    }
    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"numberOfResults"] isEqualToString:@""])
    {
        paramsDic[@"numberOfResults"] = @"5";
    }
    if ([paramsDic[@"type"] isEqualToString:@""])
    {
        paramsDic[@"type"] = @"0";
    }
    
    [[OMCUBAApiManager sharedManager] request_RelevantRecommendList_WithStart:[paramsDic[@"start"] integerValue]
                                                              numberOfResults:[paramsDic[@"numberOfResults"] integerValue]
                                                                        epgID:paramsDic[@"epgID"]
                                                                  epgSeriesID:paramsDic[@"epgSeriesID"]
                                                                    programID:paramsDic[@"programID"]
                                                                         type:[paramsDic[@"type"] integerValue]
                                                                   resolution:paramsDic[@"resolution"]
                                                                      success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCRecommendItem *> *arr) {
                                                                          [self successWithSession:sessionTask indexPath:indexPath];
                                                                      }
                                                                      failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                          [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                      }];
}

// 5.4
bool isPerforming_ProgramByArtist;
- (void)programByArtist
{
    if ([commonAssertID isEqualToString:INIT_DEFAULT] && !isPerforming_ProgramByArtist)
    {
        isPerforming_ProgramByArtist = true;
        [self programInfoList]; // 2.11
        return;
    }
    
    isPerforming_ProgramByArtist = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:uba Row:programByArtist];
    InterfaceModel      *model     = ubaArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if (![commonAssertID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"assertID"] = commonAssertID;
    }
    
    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"numberOfResults"] isEqualToString:@""])
    {
        paramsDic[@"numberOfResults"] = @"5";
    }
    
    [[OMCUBAApiManager sharedManager] request_ProgramByArtist_WithName:paramsDic[@"name"]
                                                              assertID:paramsDic[@"assertID"]
                                                                 start:[paramsDic[@"start"] integerValue]
                                                       numberOfResults:[paramsDic[@"numberOfResults"] integerValue]
                                                               success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCRecommendItem *> *arr) {
                                                                   [self successWithSession:sessionTask indexPath:indexPath];
                                                               }
                                                               failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                   [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                               }];
}

// 5.5
bool isPerforming_ProgramByEpg;
- (void)programByEpg
{
    if ([commonepgName isEqualToString:INIT_DEFAULT] && !isPerforming_ProgramByEpg)
    {
        isPerforming_ProgramByEpg = true;
        [self EPGInfoList]; // 3.8
        return;
    }
    
    isPerforming_ProgramByEpg = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:uba Row:programByEpg];
    InterfaceModel      *model     = ubaArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if (![commonepgName isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"epgName"] = commonepgName;
    }
    
    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"numberOfResults"] isEqualToString:@""])
    {
        paramsDic[@"numberOfResults"] = @"5";
    }
    
    [[OMCUBAApiManager sharedManager] request_ProgramByEpg_WithEpgName:paramsDic[@"epgName"]
                                                                 start:[paramsDic[@"start"] integerValue]
                                                       numberOfResults:[paramsDic[@"numberOfResults"] integerValue]
                                                               success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCProgramInfo *> *arr) {
                                                                   [self successWithSession:sessionTask indexPath:indexPath];
                                                               }
                                                               failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                   [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                               }];
}

// 5.6
bool isPerforming_LiveSeriesInfoList;
- (void)liveSeriesInfoList
{
    if ([commonepgID isEqualToString:INIT_DEFAULT] && !isPerforming_LiveSeriesInfoList)
    {
        isPerforming_LiveSeriesInfoList = true;
        [self EPGInfoList]; // 3.8
        return;
    }
    
    isPerforming_LiveSeriesInfoList = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:uba Row:liveSeriesInfoList];
    InterfaceModel      *model     = ubaArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if (![commonChannelID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"channelID"] = commonChannelID;
    }
    if (![commonepgID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"epgID"] = commonepgID;
    }
    
    
    [[OMCUBAApiManager sharedManager] request_LiveSeriesInfoList_WithEpgID:paramsDic[@"epgID"]
                                                                 channelID:paramsDic[@"channelID"]
                                                                   success:^(NSURLSessionDataTask *sessionTask, OMCEpgSeriesDesInfo *epgSeriesDesInfo) {
                                                                       [self successWithSession:sessionTask indexPath:indexPath];
                                                                   }
                                                                   failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                       [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                   }];
}

// 5.7
bool isPerforming_liveEpgSeriesDesInfo;
- (void)liveEpgSeriesDesInfo
{
    if ([commonepgID isEqualToString:INIT_DEFAULT] && !isPerforming_liveEpgSeriesDesInfo)
    {
        isPerforming_liveEpgSeriesDesInfo = true;
        [self EPGInfoList]; // 3.8
        return;
    }
    
    isPerforming_liveEpgSeriesDesInfo = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:uba Row:liveEpgSeriesDesInfo];
    InterfaceModel      *model     = ubaArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    paramsDic[@"epgID"] = commonepgID;
    
    [[OMCUBAApiManager sharedManager] request_LiveEpgSeriesDesInfo_WithEpgID:paramsDic[@"epgID"]
                                                                     success:^(NSURLSessionDataTask *sessionTask, OMCLiveEpgSeriesDesInfo *liveEpgSeriesDesInfo) {
                                                                         [self successWithSession:sessionTask indexPath:indexPath];
                                                                     }
                                                                     failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                         [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                     }];
}

// 5.8
- (void)liveChannelTop
{
    NSIndexPath         *indexPath = [self prepareForSection:uba Row:liveChannelTop];
    InterfaceModel      *model     = ubaArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    [[OMCUBAApiManager sharedManager] request_LiveChannelTop_WithResolution:paramsDic[@"resolution"]
                                                                    success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCRecommendItem *> *arr) {
                                                                        [self successWithSession:sessionTask indexPath:indexPath];
                                                                    }
                                                                    failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                        [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                    }];
}

// 5.9
- (void)reseedTop
{
    NSIndexPath *indexPath = [self prepareForSection:uba Row:reseedTop];
    
    [[OMCUBAApiManager sharedManager] request_ReseedTop_WithSuccess:^(NSURLSessionDataTask *sessionTask, NSArray<OMCPlayBackTopItem *> *arr) {
        [self successWithSession:sessionTask indexPath:indexPath];
    } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
        [self failureWithSession:sessionTask error:error indexPath:indexPath];
    }];
}

// 5.10
bool isPerforming_LookRelevantRecommendList;
- (void)lookRelevantRecommendList
{
    if ([commonProgramID isEqualToString:INIT_DEFAULT] && !isPerforming_LookRelevantRecommendList)
    {
        isPerforming_LookRelevantRecommendList = true;
        [self programInfoList]; // 2.11
        return;
    }
    
    isPerforming_LookRelevantRecommendList = false;
    
    NSIndexPath         *indexPath = [self prepareForSection:uba Row:lookRelevantRecommendList];
    InterfaceModel      *model     = ubaArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    paramsDic[@"programID"] = commonProgramID;
    
    if ([paramsDic[@"start"] isEqualToString:@""])
    {
        paramsDic[@"start"] = @"0";
    }
    if ([paramsDic[@"numberOfResults"] isEqualToString:@""])
    {
        paramsDic[@"numberOfResults"] = @"5";
    }
    
    [[OMCUBAApiManager sharedManager] request_LookRelevantRecommendList_WithStart:[paramsDic[@"start"] integerValue]
                                                                  numberOfResults:[paramsDic[@"numberOfResults"] integerValue]
                                                                        programID:paramsDic[@"programID"]
                                                                          success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCRecommendItem *> *arr) {
                                                                              [self successWithSession:sessionTask indexPath:indexPath];
                                                                          }
                                                                          failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                              [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                          }];
}

// 5.11
- (void)hotSearchInfo
{
    NSIndexPath *indexPath = [self prepareForSection:uba Row:hotSearchInfo];

    [[OMCUBAApiManager sharedManager] request_HotSearchInfo_WithSuccess:^(NSURLSessionDataTask *sessionTask, NSArray<OMCHotSearchInfo *> *arr) {
        [self successWithSession:sessionTask indexPath:indexPath];
    } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
        [self failureWithSession:sessionTask error:error indexPath:indexPath];
    }];
}

// 5.12
- (void)firstWordSearch
{
    NSIndexPath         *indexPath = [self prepareForSection:uba Row:firstWordSearch];
    InterfaceModel      *model     = ubaArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;

    [[OMCUBAApiManager sharedManager] request_FirstWordSearch_WithFirstLetter:paramsDic[@"firstLetter"]
                                                                      success:^(NSURLSessionDataTask *sessionTask, NSArray *arr) {
                                                                          [self successWithSession:sessionTask indexPath:indexPath];
                                                                      }
                                                                      failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                          [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                      }];
}

// 5.13
- (void)searchByMix
{
    NSIndexPath         *indexPath = [self prepareForSection:uba Row:searchByMix];
    InterfaceModel      *model     = ubaArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if (![commonChannelID isEqualToString:INIT_DEFAULT])
    {
        paramsDic[@"channelID"] = commonChannelID;
    }
    
    [[OMCUBAApiManager sharedManager] request_SearchByMix_WithKeyword:paramsDic[@"keyWord"]
                                                                start:[paramsDic[@"start"] integerValue]
                                                           resultSize:[paramsDic[@"resultSize"] integerValue]
                                                         categoryType:paramsDic[@"categoryType"]
                                                            channelId:paramsDic[@"channelID"]
                                                         categoryName:paramsDic[@"categoryName"]
                                                             rootType:paramsDic[@"rootType"]
                                                           resolution:paramsDic[@"resolution"]
                                                              success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCSearchResultInfo *> *arr) {
                                                                  [self successWithSession:sessionTask indexPath:indexPath];
                                                              }
                                                              failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                  [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                              }];
}

#pragma - mark 支付接口

// 6.1
- (void)accountBalance
{
    NSIndexPath         *indexPath = [self prepareForSection:pay Row:accountBalance];
    InterfaceModel      *model     = payArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;

    [[OMCPayApiManager sharedManager] request_AccountBalance_WithUserName:paramsDic[@"userName"]
                                                                  success:^(NSURLSessionDataTask *sessionTask, OMCAccountBalanceInfo *accountBalanceInfo) {
                                                                      [self successWithSession:sessionTask indexPath:indexPath];
                                                                  }
                                                                  failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                      [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                  }];
}

// 6.2
- (void)goodsList
{
    NSIndexPath         *indexPath = [self prepareForSection:pay Row:goodsList];
    InterfaceModel      *model     = payArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if ([paramsDic[@"productType"] isEqualToString:@""])
    {
        paramsDic[@"productType"] = @"0";
    }
    if ([paramsDic[@"type"] isEqualToString:@""])
    {
        paramsDic[@"type"] = @"1";
    }
    
    [[OMCPayApiManager sharedManager] request_GoodsList_WithBusinessID:paramsDic[@"businessID"]
                                                           productType:[paramsDic[@"productType"] integerValue]
                                                                  type:[paramsDic[@"type"] integerValue]
                                                               success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCGoodsInfo *> *arr) {
                                                                   [self successWithSession:sessionTask indexPath:indexPath];
                                                               }
                                                               failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                   [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                               }];
}

// 6.3
- (void)orderList
{
    NSIndexPath         *indexPath = [self prepareForSection:pay Row:orderList];
    InterfaceModel      *model     = payArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    if ([paramsDic[@"orderStatus"] isEqualToString:@""])
    {
        paramsDic[@"orderStatus"] = @"5";
    }
    
    [[OMCPayApiManager sharedManager] request_OrderList_WithUserName:paramsDic[@"userName"]
                                                           startTime:paramsDic[@"startTime"]
                                                             endTime:paramsDic[@"endTime"]
                                                         orderStatus:[paramsDic[@"orderStatus"] integerValue]
                                                             success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCOrderInfo *> *arr) {
                                                                 [self successWithSession:sessionTask indexPath:indexPath];
                                                             }
                                                             failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                 [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                             }];
}

// 6.4
- (void)payBalance
{
    NSIndexPath         *indexPath = [self prepareForSection:pay Row:payBalance];
    InterfaceModel      *model     = payArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    [[OMCPayApiManager sharedManager] request_PayBalance_WithPassword:paramsDic[@"password"]
                                                              orderID:paramsDic[@"orderID"]
                                                              success:^(NSURLSessionDataTask *sessionTask, OMCPayBalanceInfo *payBalanceInfo) {
                                                                  [self successWithSession:sessionTask indexPath:indexPath];
                                                              }
                                                              failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                  [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                              }];
}

// 6.5
- (void)orderFromBoss
{
    NSIndexPath         *indexPath = [self prepareForSection:pay Row:orderFromBoss];
    InterfaceModel      *model     = payArr[indexPath.row];
    NSMutableDictionary *paramsDic = model.paramsDic;
    
    [[OMCPayApiManager sharedManager] request_OrderFromBoss_WithGoodsID:paramsDic[@"goodsID"]
                                                               userName:paramsDic[@"userName"]
                                                                  count:[paramsDic[@"count"] integerValue]
                                                                success:^(NSURLSessionDataTask *sessionTask, OMCOrderInfo *orderInfo) {
                                                                    [self successWithSession:sessionTask indexPath:indexPath];
                                                                }
                                                                failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
                                                                    [self failureWithSession:sessionTask error:error indexPath:indexPath];
                                                                }];
}

@end

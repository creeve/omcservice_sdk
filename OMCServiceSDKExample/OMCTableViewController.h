//
//  OMCTableViewController.h
//  OMCServiceDemoApp
//
//  Created by GuochengLiu on 2017/9/24.
//  Copyright © 2017年 sumavision. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "InterfaceMethods.h"
#import "InterfaceController.h"
#import "Ivideo_TableViewCell.h"

NSArray <InterfaceModel *> *commonArr;
NSArray <InterfaceModel *> *vodArr;
NSArray <InterfaceModel *> *liveArr;
NSArray <InterfaceModel *> *userArr;
NSArray <InterfaceModel *> *ubaArr;
NSArray <InterfaceModel *> *payArr;


@interface OMCTableViewController : UITableViewController <UpdateModelDelegate, SendMethodDelegate, UpdateCellDelegate>

@end

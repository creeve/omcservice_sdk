//
//  CustomURLController.h
//  IvideoApiClient
//
//  Created by fudo on 16/11/15.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomURLController : UIViewController <UITextViewDelegate, UIAlertViewDelegate>

@end

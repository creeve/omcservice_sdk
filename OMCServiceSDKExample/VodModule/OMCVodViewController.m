//
//  OMCVodViewController.m
//  OMCServiceDemoApp
//
//  Created by GuochengLiu on 2017/9/26.
//  Copyright © 2017年 sumavision. All rights reserved.
//

#import "OMCVodViewController.h"
#import <OMCService/OMCService.h>

@interface OMCVodViewController ()

@property (weak, nonatomic) IBOutlet UIButton *getLocationButton;
@property (weak, nonatomic) IBOutlet UILabel *getLocationLabel;
@property (weak, nonatomic) IBOutlet UIButton *getVodColumnButton;
@property (weak, nonatomic) IBOutlet UILabel *getVodColumnLabel;
@property (weak, nonatomic) IBOutlet UIButton *getVodListButton;
@property (weak, nonatomic) IBOutlet UILabel *getVodListLabel;
@property (weak, nonatomic) IBOutlet UIButton *getEpisodeListButton;
@property (weak, nonatomic) IBOutlet UILabel *getEpisodeListLabel;
@property (weak, nonatomic) IBOutlet UIButton *authButton;
@property (weak, nonatomic) IBOutlet UILabel *authLabel;


@end

@implementation OMCVodViewController
{
    OMCLocationInfo *_selectedLocation;
    
    OMCVodColumn *_selectedVodColumn;
    
    OMCProgramInfo *_selectedVodInfo;

    OMCProgramItemInfo *_selectedEpisode;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"直播实现";
    self.getVodColumnButton.enabled = NO;
    self.getVodListButton.enabled = NO;
    self.getEpisodeListButton.enabled = NO;
    self.authButton.enabled = NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onHitLocationButton:(UIButton *)sender {
    [[OMCCommonApiManager sharedManager] request_LocationInfo_WithLocationID:nil success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCLocationInfo *> *arrLocations) {
        NSLog(@"地区列表 %@", arrLocations);
        
        _selectedLocation = arrLocations[0];
        [OMCApiBaseManager sharedManager].omc_pLocation = _selectedLocation.locationID;
        
        self.getLocationLabel.text = [NSString stringWithFormat:@"选择了地区 id:%@, name:%@", _selectedLocation.locationID, _selectedLocation.locationName];
        self.getVodColumnButton.enabled = YES;
        
    } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
        NSLog(@"Get location error %@", error.localizedDescription);
    }];
}

- (IBAction)onHitGetVodColumnButton:(id)sender {
    [[OMCVodApiManager sharedManager] request_ColumnInfoList_WithSuccess:^(NSURLSessionDataTask *sessionTask, NSArray<OMCVodColumn *> *arr) {
        NSLog(@"点播栏目列表 %@", arr);
        _selectedVodColumn = arr[0];
        
        self.getVodColumnLabel.text = [NSString stringWithFormat:@"选择了栏目 id:%@, name:%@", _selectedVodColumn.columnID, _selectedVodColumn.columnName];
        self.getVodListButton.enabled = YES;
    } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
        NSLog(@"Get column error %@", error.localizedDescription);

    }];
}

- (IBAction)onHitGetVodListButton:(id)sender {
    [[OMCVodApiManager sharedManager] request_ProgramInfoList_WithColumnID:_selectedVodColumn.columnID categoryID:nil start:0 end:10 programName:nil queryValue:nil sortType:0 year:nil location:nil success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCProgramInfo *> *arr) {
        _selectedVodInfo =arr[0];
        self.getVodListLabel.text = [NSString stringWithFormat:@"选择了节目 id:%@, name:%@",
                                     _selectedVodInfo.programID, _selectedVodInfo.programName];
        self.getEpisodeListButton.enabled = YES;
    } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
        NSLog(@"Get ProgramInfoList error %@", error.localizedDescription);
    }];
}

- (IBAction)onHitGetEpisodesButton:(id)sender {
    [[OMCVodApiManager sharedManager] request_ProgramInfoItem_WithProgramID:_selectedVodInfo.programID assetID:nil providerID:nil episodeID:nil success:^(NSURLSessionDataTask *sessionTask, NSArray<OMCProgramItemInfo *> *arr) {
        _selectedEpisode = arr[0];
        _getEpisodeListLabel.text = [NSString stringWithFormat:@"选择了单集 %@",
                                     _selectedEpisode.episodeName];
        _authButton.enabled = YES;
    } failure:^(NSURLSessionDataTask *sessionTask, NSError *error) {
        NSLog(@"Get Episodes error %@", error.localizedDescription);

    }];
}

- (IBAction)onHitAuthButton:(id)sender {
    
    OMCMovieUrlInfo *media = _selectedEpisode.movieUrlArr[0];

    [[OMCAuthApiManager sharedManager] authWithAuthType:OMCAuthTypeDefault secondAuthid:nil pid:_selectedEpisode.movieAssertID cid:nil epgID:nil drmToken:nil mediaURL:media.movieUrl programType:OMCAuthProgramTypeVod completion:^(OMCAuthResultModel *resultModel, NSError *error) {
        
        if (error != nil) {
            self.authLabel.text = [NSString stringWithFormat:@"鉴权失败: [%li]%@",
                                   (long)error.code, error.localizedDescription];
        } else {
            if (resultModel.hasRight == YES) {
                self.authLabel.text = [NSString stringWithFormat:@"鉴权通过，播放地址 %@",
                                       resultModel.finalMediaURL];
            } else {
                self.authLabel.text = [NSString stringWithFormat:@"鉴权没通过: [%@]%@",
                                       resultModel.errorCode, resultModel.authError.localizedDescription];
            }
        }
        
    }];
}

@end

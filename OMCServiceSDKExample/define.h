//
//  define.h
//  IvideoApiClient
//
//  Created by fudo on 16/11/22.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#ifndef define_h
#define define_h

#define INIT_DEFAULT @"init_default"

extern NSArray <InterfaceModel *> *commonArr;
extern NSArray <InterfaceModel *> *vodArr;
extern NSArray <InterfaceModel *> *liveArr;
extern NSArray <InterfaceModel *> *userArr;
extern NSArray <InterfaceModel *> *ubaArr;
extern NSArray <InterfaceModel *> *payArr;

/*枚举，用于确定使用哪个模型数组*/
typedef NS_ENUM(NSInteger, Section)
{
    baseData = 0,
    vod,
    live,
    user,
    uba,
    pay
};
/*枚举，用于在模型数组中定位每个接口方法所用到的模型*/
// 基础数据
typedef NS_ENUM(NSInteger, BaseDataRow)
{
    terminalShow = 0,
    updateInfo,
    feedback,
    configInfo,
    terminalConfigInfo,
    locationInfo,
    serverTime,
    portalServerIP,
    resolution,
    recommendTypeList,
    bootPicture,
    updatePackageUrl,
    advertisement,
    likeOrNot,
    portalKey,
    configInfoByKeys,
    configV1,
    likeOrNotV1
};
// 点播
typedef NS_ENUM(NSInteger, VodRow)
{
    vodHotSearchInfo = 0,
    vodSearch,
    findProgramByName,
    searchBySTB,
    vodWelcomeList,
    horizontalWelcome,
    verticalWelcome,
    columnInfoList,
    categoryInfoList,
    filterList,
    programInfoList,
    programInfoItem,
    programInfo,
    programCount,
    allProgramLabel,
    programInfosByLabel,
    queryHistory,
    updateHistory,
    deleteHistory,
    queryVodFavorite,
    updateVodFavorite,
    deleteVodFavorite,
    vodVoiceKeyWords,
    programStatus,
    danmaku,
    sendDanmaku,
    programPrevue,
    queryVodPlayHistory,
    updateVodPlayHistory,
    deleteVodPlayHistory,
    queryVodFavoriteHistory,
    updateVodFavoriteHistory,
    deleteVodFavoriteHistory,
    vodSearchByProgramName,
    vodFavoriteExists,
    vodPlayHistory
};
// 直播
typedef NS_ENUM(NSInteger, LiveRow)
{
    liveCategoryList = 0,
    channelList,
    currentEPG,
    EPGInfoList,
    channelStatus,
    channelRecommend
};
// 用户
typedef NS_ENUM(NSInteger, UserRow)
{
    login = 0,
    regist,
    logout,
    modifyPassword,
    getToken,
    checkToken,
    resetPassword,
    auth,
    authV1
};
// UBA
typedef NS_ENUM(NSInteger, UbaRow)
{
    recommendList = 0,
    vodRankList,
    relevantRecommendList,
    programByArtist,
    programByEpg,
    liveSeriesInfoList,
    liveEpgSeriesDesInfo,
    liveChannelTop,
    reseedTop,
    lookRelevantRecommendList,
    hotSearchInfo,
    firstWordSearch,
    searchByMix
};
// 支付
typedef NS_ENUM(NSInteger, PayRow)
{
    accountBalance = 0,
    goodsList,
    orderList,
    payBalance,
    orderFromBoss
};

// 依赖参数
NSString *commonLocationID = INIT_DEFAULT;
NSString *commonColumnID   = INIT_DEFAULT;
NSString *commonCategoryID = INIT_DEFAULT;
NSString *commonProgramID  = INIT_DEFAULT;
NSString *commonAssertID   = INIT_DEFAULT;
NSString *commonProviderID = INIT_DEFAULT;
NSString *commonLabelID    = INIT_DEFAULT;
NSString *commonLabelName  = INIT_DEFAULT;
NSString *commonSubID      = INIT_DEFAULT;
NSString *commonLiveCategoryID = INIT_DEFAULT;
NSString *commonChannelID      = INIT_DEFAULT;
NSString *commonChannelName    = INIT_DEFAULT;
NSString *commonepgID       = INIT_DEFAULT;
NSString *commonepgName     = INIT_DEFAULT;
NSString *commonepgSeriesID = INIT_DEFAULT;

#endif /* define_h */

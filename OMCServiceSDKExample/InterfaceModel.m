//
//  Interfaces.m
//  IvideoApiClient
//
//  Created by fudo on 16/11/18.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import "InterfaceModel.h"

@implementation InterfaceModel

+ (instancetype)interfaceWithDic:(NSDictionary *)dic {
    return [[self alloc] initWithDic:dic];
}

- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
        self.status = 0;
        
        self.ID           = dic[@"id"];
        self.path         = dic[@"path"];
        self.paramsArr    = dic[@"paramsArr"];
        self.interfaceDes = dic[@"interfaceDes"];
        self.sendMethod   = NSSelectorFromString(dic[@"sendMethod"]);
        self.paramsDic    = [NSMutableDictionary dictionary];
        
        for (NSString *param in self.paramsArr)
        {
            [self.paramsDic setObject:@"" forKey:param];
        }
        
        if (dic[@"rely"]) {
            self.rely  = dic[@"rely"];
        } else {
            self.rely = @"";
        }
        
        if (dic[@"relied"]) {
            self.relied = [dic[@"relied"] boolValue];
        } else {
            self.relied = NO;
        }
    }
    return self;
}

@end

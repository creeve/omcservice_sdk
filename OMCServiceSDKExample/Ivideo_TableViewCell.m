//
//  Ivideo_TableViewCell.m
//  IvideoApiClient
//
//  Created by fudo on 16/11/4.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import "Ivideo_TableViewCell.h"
#import "InterfaceModel.h"

@implementation Ivideo_TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)sendRequest:(UIButton *)sender {
    [self.delegate sendRequestWith:self.model.sendMethod];
}

- (UIButton *)sendButton
{
    [_sendButton.layer setCornerRadius:4];
    return _sendButton;
}
- (UIView *)statusView
{
    [_statusView.layer setCornerRadius:8];
    return _statusView;
}

@end

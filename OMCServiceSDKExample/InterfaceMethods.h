//
//  InterfaceMethods.h
//  IvideoApiClient
//
//  Created by fudo on 16/11/22.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InterfaceModel.h"

@class InterfaceModel;

@protocol UpdateCellDelegate <NSObject>

- (void)updateCellAtIndexPath: (NSIndexPath *)indexPath;

@end

@interface InterfaceMethods : NSObject

@property (nonatomic, assign) id <UpdateCellDelegate> delegate;

+ (instancetype)sharedMethod;

//- (void)locationInfo;
//- (void)columnInfoList;
//- (void)categoryInfoList;

@end

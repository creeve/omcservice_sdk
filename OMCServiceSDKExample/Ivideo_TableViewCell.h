//
//  Ivideo_TableViewCell.h
//  IvideoApiClient
//
//  Created by fudo on 16/11/4.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SendMethodDelegate <NSObject>

- (void)sendRequestWith: (SEL)selector;

@end

@class InterfaceModel;

@interface Ivideo_TableViewCell : UITableViewCell

@property (nonatomic, assign) id <SendMethodDelegate> delegate;

@property (strong, nonatomic) InterfaceModel *model;

@property (weak, nonatomic) IBOutlet UILabel *relyLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIView *statusView;

- (IBAction)sendRequest:(UIButton *)sender;

@end

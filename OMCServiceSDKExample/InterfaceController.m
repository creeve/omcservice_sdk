//
//  InterfaceController.m
//  IvideoApiClient
//
//  Created by fudo on 16/11/21.
//  Copyright © 2016年 sumavision. All rights reserved.
//


#import "InterfaceController.h"
#import "InterfaceModel.h"
#import <OMCService/OMCService.h>

extern NSString *commonLocationID;
extern NSString *commonColumnID;
extern NSString *commonCategoryID;
extern NSString *commonProgramID;
extern NSString *commonProviderID;
extern NSString *commonAssertID;
extern NSString *commonSubID;
extern NSString *commonLiveCategoryID;
extern NSString *commonChannelID;
extern NSString *commonChannelName;
extern NSString *commonLabelName;
extern NSString *commonepgID;
extern NSString *commonepgName;

@interface InterfaceController ()

@end

@implementation InterfaceController

- (instancetype)initWithModel:(InterfaceModel *)model {
    
    self.responseData = model.responseData;
    self.errorData = model.errorData;
    self.path = model.path;
    self.responseArr = model.responseArr;
    
    XLFormDescriptor *form = [XLFormDescriptor formDescriptorWithTitle:model.interfaceDes];
    XLFormSectionDescriptor *section = [XLFormSectionDescriptor formSection];
    XLFormRowDescriptor *row;
    [form addFormSection:section];
    
    // 判断该接口是否需要参数
    if ([model.paramsArr count] == 0) {
        section = [XLFormSectionDescriptor formSectionWithTitle:@"无需参数"];
    }
    else {
        section = [XLFormSectionDescriptor formSection];
        
        for (int i=0; i<model.paramsArr.count; i++) {
            row = [XLFormRowDescriptor formRowDescriptorWithTag:model.paramsArr[i] rowType:XLFormRowDescriptorTypeText title:model.paramsArr[i]];
            row.value = model.paramsDic[model.paramsArr[i]];
            [section addFormRow:row];
        }
    }
    
    [form addFormSection:section];
    
    // 请求url
    section = [XLFormSectionDescriptor formSectionWithTitle:@"请求的URL"];
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"requestURL" rowType:XLFormRowDescriptorTypeTextView];
    [row.cellConfig setObject:[UIFont systemFontOfSize:12] forKey:@"textView.font"];
    [row.cellConfig setObject:@(NO) forKey:@"textView.editable"];
    row.height = 200;
    row.value = model.requestURL;
    [section addFormRow:row];
    [form addFormSection:section];
    
    return [self initWithForm:form];
    
}

- (UIView *)responseView {
    if (_responseView == nil) {
        _responseView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        _responseView.backgroundColor = [UIColor colorWithRed:239.0f/255 green:239.0f/255 blue:244.0f/255 alpha:1];
        [_responseView addSubview:self.text];
        _responseView.hidden = YES;
    }
    return _responseView;
}

- (UITextView *)text {
    if (_text == nil) {
        _text = [[UITextView alloc] initWithFrame:CGRectMake(0, 133, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-233)];
        _text.editable = NO;
    }
    return _text;
}

#pragma mark - segmentedControl
- (UISegmentedControl *)segmentedControl {
    if (_segmentedControl == nil) {
        _segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"Request", @"Response", @"error"]];
        _segmentedControl.frame = CGRectMake(20, 84, [UIScreen mainScreen].bounds.size.width-40, 29);
        _segmentedControl.selectedSegmentIndex = 0;
        [_segmentedControl addTarget:self action:@selector(segmentChanged) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentedControl;
}

- (void)segmentChanged {
    
    switch (self.segmentedControl.selectedSegmentIndex) {
        case 0:
            self.responseView.hidden = YES;
            break;
        case 1:
            self.responseView.hidden = NO;
            self.responsePicker.hidden = NO;
            self.text.text = self.responseData;
            self.text.textColor = [UIColor blackColor];
            break;
        case 2:
            self.responseView.hidden = NO;
            self.responsePicker.hidden = YES;
            self.text.text = self.errorData;
            self.text.textColor = [UIColor redColor];
            break;
        default:
            break;
    }
    
    [self.view endEditing:YES];
}

#pragma mark - segmentedControl
- (UIPickerView *)responsePicker {
    if (_responsePicker == nil) {
        _responsePicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        _responsePicker.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _responsePicker.frame = CGRectMake(0, CGRectGetMaxY(self.text.frame), [UIScreen mainScreen].bounds.size.width, 100);
        _responsePicker.dataSource = self;
        _responsePicker.delegate = self;
        _responsePicker.showsSelectionIndicator = YES;
    }
    return _responsePicker;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return ([self.responseArr count]+1);
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30.0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title = @"";
    NSArray *paths = @[@"ptl_ipvp_cmn_cmn006", // 地区locationID
                       @"ptl_ipvp_vod_vod008", // 栏目columnID
                       @"ptl_ipvp_vod_vod009", // 栏目categoryID
                       @"ptl_ipvp_vod_vod011", // 点播节目列表programID
                       @"ptl_ipvp_vod_vod015", // 标签labelName
                       @"ptl_ipvp_vod_vod002", // subID
                       @"ptl_ipvp_live_live003", // liveCategoryID
                       @"ptl_ipvp_live_live005", // channelID
                       @"ptl_ipvp_live_live008", // epgID
                       ];
    NSInteger index = [paths indexOfObject:self.path];
    
    NSArray *tempArr = [self.responseArr arrayByAddingObject:@""];
    
    if (row != tempArr.count-1)
    {
        switch (index) {
            case 0: { // 地区locationID
                OMCLocationInfo *location = (OMCLocationInfo *)tempArr[row];
                title = location.locationName;
            }
                break;
            case 1: { // 栏目columnID
                OMCVodColumn *column = (OMCVodColumn *)tempArr[row];
                title = column.columnName;
            }
                break;
            case 2: { // 栏目categoryID
                OMCVodCategory *column = (OMCVodCategory *)tempArr[row];
                title = column.categoryName;
            }
                break;
            case 3: { // 点播节目列表programID
                OMCProgramInfo *column = (OMCProgramInfo *)tempArr[row];
                title = column.programName;
            }
                break;
            case 4: {
                // 标签信息，暂未开发
            }
                break;
            case 5: {
                OMCSearchCategory *category = (OMCSearchCategory *)tempArr[row];
                title = category.subName;
            }
                break;
            case 6: {
                OMCLiveCategory *category = (OMCLiveCategory *)tempArr[row];
                title = category.categoryName;
            }
                break;
            case 7: {
                OMCChannelInfo *channel = (OMCChannelInfo *)tempArr[row];
                title = channel.channelName;
            }
                break;
            case 8: {
                OMCChannelEPG *epg = (OMCChannelEPG *)tempArr[row];
                title = epg.epgName;
            }
                break;
            default:
                break;
        }
    }
    else
    {
        title = @"空";
    }
    
    return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    [self.delegate setPickerRowWith:row atIndexPath:self.indexPath];
    
    NSArray *paths = @[@"ptl_ipvp_cmn_cmn006", // 地区locationID
                       @"ptl_ipvp_vod_vod008", // 栏目columnID
                       @"ptl_ipvp_vod_vod009", // 栏目categoryID
                       @"ptl_ipvp_vod_vod011", // 点播节目列表programID
                       @"ptl_ipvp_vod_vod015", // 标签labelName
                       @"ptl_ipvp_vod_vod002", // subID
                       @"ptl_ipvp_live_live003", // liveCategoryID
                       @"ptl_ipvp_live_live005", // channelID
                       @"ptl_ipvp_live_live008", // epgID
                       ];
    NSInteger index = [paths indexOfObject:self.path];
    
    NSArray *tempArr = [self.responseArr arrayByAddingObject:@""];
    
    switch (index) {
        case 0: {
            if (row != tempArr.count-1)
            {
                OMCLocationInfo *location = (OMCLocationInfo *)tempArr[row];
                commonLocationID = location.locationID;
            }
            else
            {
                commonLocationID = @"";
            }
            
        }
            break;
        case 1: {
            if (row != tempArr.count-1)
            {
                OMCVodColumn *column = (OMCVodColumn *)tempArr[row];
                commonColumnID = column.columnID;
            }
            else
            {
                commonColumnID = @"";
            }
        }
            break;
        case 2: { // 栏目categoryID
            if (row != tempArr.count-1)
            {
                OMCVodCategory *column = (OMCVodCategory *)tempArr[row];
                commonCategoryID = column.categoryID;
            }
            else
            {
                commonCategoryID = @"";
            }
        }
            break;
        case 3: { // 点播节目列表programID
            if (row != tempArr.count-1)
            {
                OMCProgramInfo *column = (OMCProgramInfo *)tempArr[row];
                commonProgramID  = column.programID;
                commonAssertID   = column.assertID;
                commonProviderID = column.providerID;
            }
            else
            {
                commonProgramID  = @"";
                commonAssertID   = @"";
                commonProviderID = @"";
            }
        }
            break;
        case 4: {
            // 标签信息,暂未开发
        }
            break;
        case 5: {
            if (row != tempArr.count-1)
            {
                OMCSearchCategory *category = (OMCSearchCategory *)tempArr[row];
                commonSubID = category.subId;
            }
            else
            {
                commonSubID = @"";
            }
        }
            break;
        case 6: {
            if (row != tempArr.count-1)
            {
                OMCLiveCategory *category = (OMCLiveCategory *)tempArr[row];
                commonLiveCategoryID = category.categoryID;
            }
            else
            {
                commonLiveCategoryID = @"";
            }
        }
            break;
        case 7: {
            if (row != tempArr.count-1)
            {
                OMCChannelInfo *channel = (OMCChannelInfo *)tempArr[row];
                commonChannelID = channel.channelID;
                commonChannelName = channel.channelName;
            }
            else
            {
                commonChannelID = @"";
                commonChannelName = @"";
            }
        }
            break;
        case 8: {
            if (row != tempArr.count-1)
            {
                OMCChannelEPG *epg = (OMCChannelEPG *)tempArr[row];
                commonepgID = epg.epgID;
                commonepgName = epg.epgName;
            }
            else
            {
                commonChannelID = @"";
                commonChannelName = @"";
            }
        }
            break;
        default:
            break;
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.responseView];
    [self.view addSubview:self.segmentedControl];
    
    if (self.responseArr)
    {
        [self.responseView addSubview:self.responsePicker];
    }
    
    [self.segmentedControl sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //更新参数
    NSMutableDictionary *dicM = [NSMutableDictionary dictionaryWithDictionary:self.formValues];
    
    for (NSString *key in [dicM allKeys])
    {
        if ([dicM[key] isKindOfClass:[NSNull class]])
        {
            dicM[key] = @"";
        }
    }
    
    NSDictionary *paramsDic = dicM;
    
    if ([self.delegate respondsToSelector:@selector(updateParamsDicWith:atIndexPath:)])
    {
        [self.delegate updateParamsDicWith:paramsDic atIndexPath:self.indexPath];
    }
}


@end

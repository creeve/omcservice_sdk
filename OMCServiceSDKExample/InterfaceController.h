//
//  InterfaceController.h
//  IvideoApiClient
//
//  Created by fudo on 16/11/21.
//  Copyright © 2016年 sumavision. All rights reserved.
//

#import <XLForm/XLForm.h>

@class InterfaceModel;

@protocol UpdateModelDelegate <NSObject>

- (void)updateParamsDicWith: (NSDictionary *)paramsDic atIndexPath: (NSIndexPath *)indexPath;
- (void)setPickerRowWith: (NSInteger)row atIndexPath: (NSIndexPath *)indexPath;

@end

@interface InterfaceController : XLFormViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, assign) id <UpdateModelDelegate> delegate;

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) NSString *path;

@property (nonatomic, copy) NSString *errorData;
@property (nonatomic, copy) NSString *responseData;
@property (nonatomic, strong) NSArray *responseArr;

@property (nonatomic, strong) UIView *responseView;
@property (nonatomic, strong) UITextView *text;
@property (nonatomic, strong) UIPickerView *responsePicker;
@property (nonatomic, strong) UISegmentedControl *segmentedControl;

- (instancetype)initWithModel: (InterfaceModel *)model;

@end

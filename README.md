# OMCService SDK 使用说明

## 前提条件

已经申请得到库文件（OMCService.framework），AppID以及APIKey。

## 集成方法

将OMCService.framework库文件，拖入工程中，勾选Copy items if needed。

![01](https://ws1.sinaimg.cn/large/006tNc79gy1fgcgyc4024j314k0nwgnf.jpg)

点击finish之后，定位到Tagerts - Buid Settings - Other Linker Flags。

为Other Linker Flags后面添加：`ObjC`。

![02](https://ws3.sinaimg.cn/large/006tNc79gy1fgch46y81dj31kw0hl0w9.jpg)

点击编译运行，此时应该能正常编译运行。

> 如果用户已经集成了CocoaPods，默认已经添加了`ObjC`，无需再添加，如果您的项目已经添加了OMCService.framework，对Other Linker Flags做了修改，将要使用CocoaPods，则请先将Other Linker Flags改为$(inherited)

此外，由于iOS9之后，默认不允许HTTP访问，我们需要在自己的工程的Info.plist里面设置允许访问HTTP，才能调用OMCService获取网络数据。

![03](https://ws2.sinaimg.cn/large/006tNc79gy1fgchbzgr7dj318c03o3zk.jpg)

## 调用流程

### 初始化OMCService

在AppDelegate的中，添加头文件引用：

```
#import <OMCService/OMCService.h>
```

以及在`didFinishLaunchingWithOptions`中添加初始化方法：

```
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // 初始化Portal地址
    [OMCApiBaseManager setServerAddress:@"http://portal.rifestone.com:8080/PortalServer-App/new/"];
    // 使用获取到的AppID和APIKey初始化OMCApiBaseManager（必须）
    [OMCApiBaseManager setAppId:appID appKey:appKey completion:nil];

    return YES;
}
```

其他模块使用教程见[Wiki](http://git.oschina.net/creeve/omcservice_sdk/wikis)文档

## 示例App使用方法

由于本示例App使用了CocoaPods，请确保已经安装了[CocoaPods](https://guides.cocoapods.org/using/getting-started.html)

```
git clone https://git.oschina.net/creeve/omcservice_sdk.git
```

下载完成后

```
cd omcservice_sdk
```

进入工程路径

```
pod install
```

安装第三方库依赖，完成后打开OMCServiceSDKExample.xcworkspace即可。

示例App包含了各个模块的接口功能，点击后即可根据日志及网络请求查看实际的接口数据。

> 推荐使用[Charles](https://www.charlesproxy.com/)来抓包查看实际请求数据，这里有[教程](http://www.jianshu.com/p/9822e3f28f0a)

示例里面还包含了一个典型的直播播放实现流程：在Wiki中对应：[典型实现流程](https://gitee.com/creeve/omcservice_sdk/wikis/%E5%85%B8%E5%9E%8B%E5%AE%9E%E7%8E%B0%E6%B5%81%E7%A8%8B)

